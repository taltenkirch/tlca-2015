


Coinductive data types are used in functional programming to represent infinite data structures.
Examples include the ubiquitous data type of streams over a given base type, but also more sophisticated types; 
as an example we present an alternative definition of equivalence of types (Example \ref{ex:equivalence}).

From a categorical perspective, coinductive types are characterized by a \emph{universal property},
which specifies the object with that property \emph{uniquely} in a suitable sense.
More precisely, a coinductive type is specified as the \emph{terminal coalgebra} of a suitable endofunctor.
In this category-theoretic viewpoint, coinductive types are dual to \emph{inductive} types, which are 
defined as initial algebras.

Inductive, resp.\ coinductive, types are usually considered in the principled form of the family of $\W$-types, resp.\ $\M$-types, 
parametrized by a type $A$ and a dependent type family $B$ over $A$, that is, a family of types $(B(a))_{a:A}$. 
Intuitively, the elements of the coinductive type $\M(A,B)$ are trees with nodes labeled by elements of $A$ 
such that a node labeled by $a:A$ has $B(a)$-many subtrees, given by a map $B(a) \to \M(A,B)$; see Figure \ref{fig:tree} for an example.
The \emph{inductive} type $\W(A,B)$  contains only trees where any path within that tree
eventually leads to a \emph{leaf}, that is, to a node $a:A$ such that $B(a)$ is empty.

\begin{figure}[htb]
\begin{minipage}{0.3\textwidth}
 \begin{align*}
 a,b,c &: A\\
  B(a) &= 0\\
  B(b) &= 2\\
  B(c) &= 3
\end{align*}
\end{minipage}
\begin{minipage}{0.6\textwidth}
 \[
 \begin{xy}
  \xymatrix@C=1pc {
               & \vdots &  & a & \vdots & \vdots & \vdots    \\
            a  & & b \ar@{-}[lu]^{1} \ar@{-}[ru]_{2}& & c\ar@{-}[u]^{1}\ar@{-}[ru]^{2}\ar@{-}[rru]_{3} & &    \\
               & & c \ar@{-}[llu]^{1} \ar@{-}[u]^{2} \ar@{-}[rru]_{3} & & & &    
   }
 \end{xy}
\]
\end{minipage}
 \caption{Example of a tree (adapted from \cite{DBLP:journals/apal/BergM07})} \label{fig:tree}
\end{figure}

In this work, we study coinductive types in Homotopy Type Theory (HoTT), 
an extension of intensional Martin-Löf type theory \cite{martin_lof}; we give a brief overview in Section \ref{sec:hott}.


The universal properties defining inductive and coinductive types, respectively, 
can be expressed internally to intensional Martin-Löf type theory (and thus internally to HoTT).
Awodey, Gambino, and Sojakova \cite{DBLP:conf/lics/AwodeyGS12} use this facility when proving, within a subtheory $\mathcal{H}$ of HoTT, a logical equivalence between
\begin{enumerate}
 \item the existence of $\W$-types (a.k.a.\ the existence of a universal object) and
 \item the addition of a set of type-theoretic rules to their \enquote{base theory} $\mathcal{H}$. \label{enum:rules_ind_types}
\end{enumerate}
We might call the $\W$-types defined internally \enquote{internal $\W$-types}, and those specified via type-theoretic rules \enquote{external} ones.
In that sense, Awodey, Gambino, and Sojakova \cite{DBLP:conf/lics/AwodeyGS12} prove a logical equivalence between the existence of internal and external $\W$-types.

The universal property defining (internal) coinductive types in HoTT is dual to the one defining (internal) inductive types. 
One might hence assume that their existence is equivalent to a set of type-theoretic rules dual (in a suitable sense) to those given for external $\W$-types as in Item \ref{enum:rules_ind_types} above.
However, the rules for external $\W$-types cannot be dualized in a na\"ive way, due to some asymmetry of HoTT related to dependent types as maps into a \enquote{type of types} (a \emph{universe}),
see the discussion in \cite{hott-mailing-coind}.


In this work, we show instead that coinductive types in the form of $\M$-types can be derived from certain inductive types.
(More precisely, only one specific $\W$-type is needed: the type of natural numbers, which is readily specified as a $\W$-type \cite{DBLP:conf/lics/AwodeyGS12}.)




The result presented in this work is not surprising; indeed, the constructibility of coinductive types from inductive types 
has been shown in extensional type theory (see Section \ref{sec:rel-work}) and
was conjectured to work in HoTT during a discussion on the HoTT mailing list \cite{hott-mailing-coind}.
In this work, we give a formal proof of the constructibility of a class of coinductive types from inductive types, with a proof of correctness of the construction.

The theorem we prove here is actually more general than described above: instead of plain $\M$-types as described above, we construct \emph{indexed} $\M$-types, which 
can be considered as a form of \enquote{(simply-)typed} trees, typed over a type of indices $I$. Plain $\M$-types then correspond to the mono-typed indexed $\M$-types,
that is, to those for which $I = 1$.
Since all the ideas are already contained in the case of plain $\M$-types, we describe the construction of those extensively, and only briefly state the definitions
and the main result for the indexed case. The formalisation in \Agda, however, is done for the more general, indexed, case.
An example illustrates the need for these more general \emph{indexed} $\M$-types.

\subsection{Related work}\label{sec:rel-work}
 Inductive types in the form of $\W$-types in HoTT have been studied by Awodey, Gambino, and Sojakova \cite{DBLP:conf/lics/AwodeyGS12}. The content of that work is described above.
 
 
 Van den Berg and De Marchi \cite{DBLP:journals/apal/BergM07} study the existence of \emph{plain} $\M$-types in models of \emph{extensional} type theory, that is, of type
 theory with a reflection rule identifying propositional and judgmental equality.
 They prove the derivability of $\M$-types from $\W$-types in such models, see Corollary 2.5 of the arXiv version of that article.
 A construction in extensional type theory of $\M$-types from $\W$-types is given by Abbott, Altenkirch, and Ghani \cite{DBLP:journals/tcs/AbbottAG05}.
 
 Martin-Löf type theory without identity reflection, but with the principle of \emph{Uniqueness of Identity Proofs} (Axiom K) can be identified with 
 the 0-truncated fragment of HoTT (modulo the assumption of univalence and HITs). For such a type theory,
 a construction of (indexed) $\M$-types from $\W$-types is described by Altenkirch et al.~\cite{indexed_containers},
 internalizing a standard result in 1-category theory \cite{DBLP:journals/tcs/Barr93}.
 The present work thus generalizes the construction described in \cite{indexed_containers} by extending it from the 0-truncated fragment to the whole of HoTT.
 More specifically, the main work in this generalization is to develop higher-categorical variants of the 1-categorical constructions used in 
 \cite{indexed_containers} that are compatible with the higher-categorical structure (the coherence data) of types.
\subsection{Synopsis}


The paper is organized as follows:
In Section \ref{sec:hott} we present the type theory we are working in---a \enquote{subsystem} of HoTT as presented in \cite{hottbook}.
In Section \ref{sec:m-in-hott} we define signatures for \emph{plain} $\M$-types and, via a universal property, the $\M$-type associated to a given signature.
In Section \ref{sec:derive-m} we construct the $\M$-type of a given signature.
In Section \ref{sec:indexed} we state the main result for the case of \emph{indexed} $\M$-types.
Finally, in Section \ref{sec:formal} we give an overview of the formalisation of our result in the proof assistant \Agda.


\subsection*{Acknowledgments}
We are grateful to many people for helpful discussions about coinductive types, online as well as offline: 
 Thorsten Altenkirch, 
 Steve Awodey, 
 Mart\'{i}n Escard\'{o}, 
 Nicolai Kraus,
 Peter LeFanu Lumsdaine,
 Ralph Matthes, 
 Paige North,
 Mike Shulman, and
 Vladimir Voevodsky.
We thank Nicolai Kraus, Peter LeFanu Lumsdaine and Paige North for suggesting improvements and clarifications for this paper.
 