
In Section \ref{sec:m-in-hott} we defined the type $\Final_S$ of final coalgebras of a signature $S$, and showed that this 
type is a proposition (Lemma \ref{lem:is-prop-final}).
In this section, we construct an element of $\Final_S$, which, combined with Lemma \ref{lem:is-prop-final}, proves the following theorem per the remark at the end of Section \ref{sec:hott}:

\begin{theorem}\label{thm:is-contr-final}
 The type 
   \[\Final_S = \sm{(X,\rho)}\prd{(C,\gamma)}\iscontr\big(\Coalgmor{(C,\gamma)}{(X,\rho)}\big)\] 
 is contractible.
\end{theorem}

The construction of the final coalgebra is done in several steps, inspired by a construction of $\M$-types from $\W$-types 
in a type theory satisfying Axiom K by Altenkirch et al.~\cite{indexed_containers}.
Its carrier is defined as the limit of a \emph{chain}:

\begin{definition}
 A \fat{chain} is a pair $(X,\pi)$ of a family of types $X:\N \to \type$ and a family of functions $\pi_n : X_{n+1} \to X_n$.
 Here and below we write $X_n:=X(n)$ for the $n$th component of the family $X$.
\end{definition}

The (homotopy) limit of such a chain is given by the type of \enquote{compatible tuples}:
\begin{definition}\label{defn:limit}
 The \fat{limit} of the chain $(X,\pi)$ is given by the type
 \[ L := \sm{x:\prd{n:\N}X_n}\prd{n:\N}\pi_{n}x_{n+1} = x_{n} \enspace . \] 
The limit is equipped with projections $p_n : L \to X_n$, and $\beta_n : \comp{p_{n+1}}{\pi_n} = p_n$.  Sometimes, we simply write
\[ L = \lim X,\]
when the maps $\pi$ are clear.
\end{definition}

Note that this limit (we drop the adjective \enquote{homotopy}) is an instance of the general construction of homotopy limits 
by Avigad, Kapulkin, and Lumsdaine \cite{homotopy_limits}.


\begin{figure}[hbt]
\centering
  \[
   \begin{xy}
    \xymatrix@C=30pt{
             & A \ar@{.>}[ddl]|{f_0}\ar@{.>}[dd]|{f_1} \ar@{.>}[ddr]|{f_2} \ar@{.>}[ddrr]|{f_3} \ar@{.>}[ddrrr]|{f_4} \ar@{-->}[drrrrr]|{f}& \\
       & & & &  & &  L \ar[lllllld]|{p_0} \ar[llllld]|{p_1} \ar[lllld]|{p_2} \ar[dlll]|{p_3}&   \\
         X_0 & X_1 \ar[l]^{\pi_0} & X_2 \ar[l]^{\pi_1}& X_3 \ar[l]^{\pi_2} & \ldots \ar[l]^{\pi_3}  
     }
   \end{xy}
  \]
\caption{Universal property of $L$} \label{fig:universal}
\end{figure}



\begin{lemma}\label{lem:univ_prop_limit}
 The type $L$ satisfies the following universal property: for all types $A$, we have an equivalence of types between maps into $L$ and 
 \enquote{cones} over $X$:
 \[
      A \to L \enspace \simeq  \enspace \sm{f : \prd{n:\N} A \to X_n} \prd{n:\N} \comp{f_{n+1}}{\pi_n} = f_n  \enspace =: \Cone(A) \enspace . 
  \]
\end{lemma}
The equivalence, from left to right, maps a function $f : A \to L$ to
its projections $\comp{f}{p_n}$, as shown in Figure \ref{fig:universal}.

The next lemma is about tuples in \fat{co}chains, that is, tuples in chains with inverted arrows. Those tuples are determined by their first element:
\begin{lemma}\label{lem:first_elem_deter_cochain_tuple}
  Let $X : \N \to \type$ be a family of types, and $l : \prd{n:\N} X_n \to X_{n+1}$ a family of functions.
  Let 
  \[ Z := \sm{x:\prd{n:\N} X_n} \prd{n:\N} x_{n+1} = l_n(x_n) \enspace . \]
  Then the projection $Z \to X_0$ is an equivalence.
\end{lemma}

\begin{proof}
 Let $G$ be the functor defined by $G Y = 1 + Y$. Fix an element $z : Z$. 
 Then $z$ and $l$ together define a $G$-algebra structure on $X$, regarded as
a fibration over $\N$.  
Since $\N$ is the homotopy initial algebra of $G$, the
type $S z$ of algebra sections of $X$ is contractible.  But $Z$ is 
equivalent to

\[ \sm{z:X_0} \sm{x : \prd{n:\N}X_n} (x_0 = z) \times \left(\prd{n:\N} x_{n+1} = l_n(x_n)\right) \enspace , \]
% 
which is exactly $\sm{z : X_0} S z \simeq X_0$.
\end{proof}

\begin{lemma}\label{lem:limit_of_shifted_chain}
Let $(X, \pi)$ be a chain, and let $(X', \pi')$ be the \emph{shifted} chain,
defined by $X'_n := X_{n+1}$ and $\pi'_n := \pi_{n+1}$.  Then the two chains have
equivalent limits.
\end{lemma}
\begin{proof}
 Let $L$ and $L'$ be the limits of $(X,\pi)$ and $(X',\pi')$, respectively. 
 We have
 \begin{align*}
 L' &\overset{\definemarker{1}}{\simeq} \sm{y:\prd{n:\N} X_{n+1}} \prd{n:\N} \pi_{n+1}y_{n+1} = y_n \\
    &\overset{\definemarker{2}}{\simeq} \sm{x_0 : X_0} \sm{y : \prd{n:\N} X_{n+1}} (\pi_0 y_0 = x_0) \times \left(\prd{n:\N}\pi_{n+1}y_{n+1} = y_n\right) \\
    &\overset{\definemarker{3}}{\simeq} \sm{x:\prd{n:\N} X_n} (\pi_0 x_0 = x_0) \times \left(\prd{n:\N}\pi_{n+1}x_{n+2} = x_{n+1} \right)\\
    &\overset{\definemarker{4}}{\simeq} \sm{x:\prd{n:\N} X_n}  \prd{n:\N}\pi_{n}x_{n+1} = x_{n} \\
    &\overset{\definemarker{5}}{\simeq} L \enspace ,
 \end{align*}
where \refermarker{1} and \refermarker{5} are by definition. Equivalence \refermarker{2} is given by multiplying with the contractible 
type $\sm{x_0:X_0} \pi_0 y_0 = x_0$ \cite[Lem.\ 3.11.8]{hottbook} and subsequent swapping of components in a direct product.
Equivalence \refermarker{3} is given by joining the first two components, and similarly in \refermarker{4} the last two components are joined.
\end{proof}

The next lemma says that  polynomial functors (see Definition \ref{def:poly_functor}) commute with limits of chains.
Let $(A,B)$ be a container with associated polynomial functor $P = P_{A,B}$.
Let $(X,\pi)$ be a chain with limit $(L,p)$. 
Define the chain $(PX,P\pi)$ with $PX_n := P(X_n)$ and likewise for $P\pi$, and let $L^P$ be its limit.
The family of maps $Pp_n : PL \to PX_n$ determines a function $\alpha : PL \to L^P$.
\begin{lemma}\label{lem:poly_continuous}
The function $\alpha$ is an isomorphism.
\end{lemma}
\begin{proof}
By \enquote{equational} reasoning we have
\begin{align*} L^P &\overset{\definemarker{6}}{\simeq} \sm{w : \prd{n:\N} \sm{a:A} B(a)\to X_n} \prd{n:\N} (P\pi_n) w_{n+1} = w_n  \\
                   &\overset{\definemarker{7}}{\simeq} \sm{a: \prd{n:\N}A} \sm{u:\prd{n:\N}B(a_n) \to X_n} \prd{n:\N} (a_{n+1},\comp{u_{n+1}}{\pi_n}) = (a_n,u_n) \\
                   &\overset{\definemarker{8}}{\simeq} \sm{a: \prd{n:\N}A} \sm{p:\prd{n:\N} a_{n+1}=a_n}\sm{u:\prd{n:\N} B(a_n) \to X_n} \prd{n:\N} \trans{(p_n)}{\comp{u_{n+1}}{\pi_n}} = u_n \\
                   &\overset{\definemarker{9}}{\simeq} \sm{a:A} \sm{u : \prd{n:\N} B(a) \to X_n} \prd{n:\N} \comp{u_{n+1}}{\pi_n} = u_n \\
                   &\overset{\definemarker{10}}{\simeq} \sm{a:A} B(a)\to L \\
                   &\overset{\definemarker{11}}{\simeq} PL
\end{align*}
where \refermarker{6} and \refermarker{11} are by definition, \refermarker{7} is by swapping $\Pi$ and $\Sigma$, \refermarker{8} by expanding equality of pairs,
\refermarker{9} by applying Lemma \ref{lem:first_elem_deter_cochain_tuple} and \refermarker{10} by universal property of $L$.
Verifying that the composition of these isomorphisms is
$\alpha$ is straightforward.
\end{proof}


\begin{proof}[Proof of Theorem \ref{thm:is-contr-final}]
We now construct a terminal coalgebra for a container $(A,B)$. Let $P = P_{A,B}$ the polynomial functor associated to the container.
By recursion on $\N$ we define the chain
\[
 \begin{xy}
  \xymatrix{
       1 & P1 \ar[l]_{!} & P^2 1 \ar[l]_{P!} & P^3 \ar[l]_{P^2 !} & \ldots \ar[l]_{P^3 !} 
   }
 \end{xy}
\]
which for brevity we call $(W,\pi)$, that is, $W_n:=P^n1$ and $\pi_n:=P^n!$.
 
Let $(L,p)$ be the limit of $(W, \pi)$.  If $L'$ is the limit of the shifted chain, we have a sequence of equivalences
\[  PL \overset{\definemarker{12}}{\simeq} L' \overset{\definemarker{13}}{\simeq} L \]
where \refermarker{12} is given by Lemma \ref{lem:poly_continuous} and \refermarker{13} by Lemma \ref{lem:limit_of_shifted_chain}.
 We denote this equivalence by $\in : P L \to L$, and its inverse by $\out : L \to P L$.

 It is worth noting that the construction of $L$ ``does not raise the universe
 level'', i.e., if $A$ and $B$ are contained in some universe $\mathcal{U}$,
 then $L$ is contained in $\mathcal{U}$ as well.  In other words, we only need
 one universe to carry out our construction of the final coalgebra.

 We will now show that $(L,\out)$ is a final $(A,B)$-coalgebra.  For this, let
$(C,\gamma)$ be any coalgebra, i.e., $\gamma : C \to PC$. The type of coalgebra
morphisms $\Coalgmor{(C,\gamma)}{(L,\out)}$ is given by
 \[ U:= \sm{f : C \to L} \comp{f}{\out} = \comp{\gamma}{Pf} \enspace .  \] We
need to show that $U$ is contractible.
We compute as follows (see below the math display for intermediate definitions):

  \begin{align*}
    U &\overset{\definemarker{14}}{\simeq} \sm{f : C \to L} \comp{f}{\out} = \comp{\gamma}{Pf} \\
      &\overset{\definemarker{15}}{\simeq} \sm{f : C \to L} \comp{f}{\out} = \step(f)\\
      &\overset{\definemarker{16}}{\simeq} \sm{f : C \to L} \comp{\comp{f}{\out}}{\in} = \comp{\step(f)}{\in}\\
      &\overset{\definemarker{17}}{\simeq} \sm{f : C \to L} f = \Psi(f)\\
      &\overset{\definemarker{18}}{\simeq} \sm{c : \Cone} e(c) = \Psi(e(c))\\
      &\overset{\definemarker{19}}{\simeq} \sm{c : \Cone} e(c) = e (\Phi(c))\\
      &\overset{\definemarker{20}}{\simeq} \sm{c : \Cone} c = \Phi(c)\\
      &\overset{\definemarker{21}}{\simeq} \sm{(u,q) : \Cone} \sm {p : u = \Phi_0 (u)} \trans{p}{q} = \Phi_1 (u)(q)\\
      &\overset{\definemarker{22}}{\simeq} \sm{u : \Cone_0}\sm{p : u = \Phi_0 u} \sm{q : \Cone_1 u} \trans{p}{q} = \Phi_1 (u)(q)\\
      &\overset{\definemarker{23}}{\simeq} \sm{t : 1} 1\\
      &\simeq 1
  \end{align*}
% 
  where we use the following definitions:
  The function 
   $\step_Y : (C \to Y) \to (C \to PY)$ is defined as $\step_Y(f) := \comp{\gamma}{Pf}$ and
   $\Psi : (C \to L) \to (C \to L)$ is defined as $\Psi(f) := \comp{\step_L(f)}{\in}$.
  The map $\Phi : \Cone \to \Cone$ is the counterpart of $\Psi$ on the side of cones. We define
  $\Phi (u,g) = (\Phi_0 u, \Phi_1u(g)) : \Cone \to \Cone$ with 
  \begin{align*} (\Phi_0 u)_0 &:= x \mapsto tt : C \to 1 = W_0\\
                 (\Phi_0 u)_{n+1} &:= \step_{W_n}(u_n) : C \to W_{n+1} = PW_n
  \end{align*}
  and analogously for $\Phi_1$ on paths.
  By $e$ we denote the equivalence of Lemma \ref{lem:univ_prop_limit} from right to left, and $\Cone = \sm{u:\Cone_0} \Cone_1(u)$ is short for $\Cone(C)$.
  The equivalence \refermarker{16} follows from $\in$ being an equivalence, and \refermarker{17} follows from $\in$ and $\out$ being inverse to each other.
  We pass from maps into $L$ to cones in \refermarker{18}, using the equivalence of Lemma \ref{lem:univ_prop_limit}, while \refermarker{19} uses the commutativity of the following square:
\[
\xymatrix{
\Cone \ar[r]^-e \ar[d]_{\Phi} & (C \to L) \ar[d]^{\Psi}\\
\Cone \ar[r]^-e & (C \to L).
}
\]
  In \refermarker{21}, identity in a sigma type is reduced to identity of the components, and in \refermarker{22} the components are rearranged.
  Finally, step \refermarker{23} consists of two applications of Lemma \ref{lem:first_elem_deter_cochain_tuple}.
  
  Altogether, this shows that for any coalgebra $(C,\gamma)$, the type of coalgebra morphisms $\Coalgmor{(C,\gamma)}{(L,\out)}$ is contractible.
  This concludes the construction of a final coalgebra for the (polynomial functor of the) signature $(A,B)$ and thus,
  combined with Lemma \ref{lem:is-prop-final}, the proof of Theorem \ref{thm:is-contr-final}. 
\end{proof}

  
  
  
From the construction of $L$ we get the following corollary about the homotopy level of $\M$-types:

\begin{lemma}
 The homotopy level of the (carrier of the) $\M$-type associated to the signature $(A,B)$ is bounded by that of the type of nodes $A$, that is,
 \[ \isofhlevel{n}{A} \to \isofhlevel{n}{\M(A,B)} \enspace . \]
\end{lemma}


\begin{example} We continue the example of streams of Example \ref{example:streams}, with $A = A_0$ the type of nodes.
 In that case, the chain considered in the proof of Theorem \ref{thm:is-contr-final} is given by $W_n=P^n(1) = A^n$, and the map $\pi_n : A^{n+1} \to A^n$
 chops of the $(n+1)$th element of any $(n+1)$-tuple. The limit $L$ is hence given by $A^\N = (\N \to A)$.
 The type of streams over $A$ has the same homotopy level as the type $A$ of nodes.
\end{example}


\input{bisim}


