The proofs contained in this paper have been formalised in the proof assistant
\Agda in a self-contained development. 
The proof has been type-checked by version 2.4.2.2 of \Agda. 
The source code as well as HTML documentation can be found on
\url{https://github.com/HoTT/m-types/}. 
The source code is also archived with the arXiv version of this article \cite{ahrens_capriotti_spadotti}.

The formalised proofs deal with the indexed case (Section \ref{sec:indexed}) directly,
but apart from that, they correspond closely to the informal proofs presented
here. In particular, they make heavy use of the ``equational reasoning''
technique to prove equivalences between types.

In fact, it is often the case that proving an equivalence between types $A$ and
$B$ ``directly'', i.e.\ by defining functions $A \to B$ and $B \to A$, and then
proving that they compose to identities in both directions, is unfeasibly hard,
due to the complexity of the terms involved.

However, in most cases, we can construct an equivalence between $A$ and $B$ by
composition of several simple equivalences.  Those simple building blocks range
from certain ad-hoc equivalences that make specific use of the features of the
two types involved, to very general and widely applicable ``rewriting rules'',
like the fact that we can swap a $\Sigma$-type with a $\Pi$-type (sometimes
called the \emph{constructive axiom of choice} \cite{hottbook}).

By assembling elementary equivalences, then, we automatically get both a
function $A \to B$ \emph{and} a proof that it is an equivalence.  However,
sometimes care is needed to ensure that the resulting function has the desired
computational properties.

An important consideration during the formalisation of the proof of 
Theorem \ref{thm:is-contr-final} was keeping the size of the terms reasonably short.  For
example, in one early attempt, the innocent-looking term $\in$ was being
normalised into a term spanning more than 12000 lines.

The explosion in term size was clearly causing performance issues during
type-checking, which resulted in \Agda running out of memory while checking
apparently trivial proofs.

We solved this problem by moving certain definitions (like that of $\in$ itself)
into an \emph{abstract} block, thereby preventing \Agda from expanding it at all.
Of course, this means that we lost all the computational properties of certain
functions, so we had to abstract out their computational behaviour in the form
of propositional equalities, and manually use them in the proof of
Theorem \ref{thm:is-contr-final}. This work-around is the source of most of the
complications in the formal proof.
