

The present work takes place within a type theory that is a subsystem of the type theory presented in the HoTT book \cite{hottbook}.
The latter is often referred to as Homotopy Type Theory (HoTT); it is an extension of intensional Martin-L\"of type theory (IMLTT) \cite{martin_lof}.
The extension is given by two data: firstly, the \emph{Univalence Axiom}, introduced by Vladimir Voevodsky and proven consistent
with IMLTT in the simplicial set model \cite{simp_set_model}.
The second extension is given by \emph{Higher Inductive Types} (HITs), the precise theory of which is still subject to active research.
Preliminary results on HITs have been worked out by Sojakova \cite{DBLP:conf/popl/Sojakova15} and 
Lumsdaine and Shulman---see \cite[Chap.\ 6]{hottbook} for an introduction.
In the present work, we use the Univalence Axiom, but do not make use of HITs.

The syntax of HoTT is extensively described in a book \cite{hottbook}; 
we only give a brief summary of the type constructors used in the present work, thus fixing notation.
The fundamental objects are \emph{types}, which have \emph{elements} (\enquote{inhabitants}), written $a:A$.
Types can be dependent on terms, which we write as $x:A\entails B(x) : \type$. In the preceding judgment, we use a special type $\type$, 
the \enquote{universe} or \enquote{type of types}. In this work we  assume any type being an element of $\type$ 
in the sense of \enquote{typical ambiguity} \cite[Chap.\ 1.3]{hottbook}, without worrying about 
universe levels. The formalisation in \Agda ensures that everything works fine in that respect: 
as we will see later, the universe $\type$ is closed under the construction of $\M$-types.

We use the following type constructors: dependent products $\prd{x:A}B(x)$, with non-dependent variant written $A \to B$, 
dependent sums $\sm{x:A}B(x)$ with non-dependent variant written $A \times B$, the identity type $\id[A]{x}{y}$ and
the coproduct type $A + B$.
In particular, we assume the empty type $0$ and the singleton type $1$.
Furthermore, we assume the existence of a type of natural numbers, given as an inductive type according to the rules given in \cite[Chap.\ 1.9]{hottbook}.
Finally, we assume the univalence axiom for the universe $\type$ as presented in \cite[Chap.\ 2.10]{hottbook}.

Concerning terms, function application is denoted by parentheses as in $f(x)$ or, occasionally, simply by juxtaposition. 
We write dependent pairs as $(a,b)$ for $b : B(a)$. Projections are indicated by a subscript, that is,
for $x : \sm{a:A}B(a)$ we have $x_0 : A$ and $x_1 : B(x_0)$.
Indices are also used occasionally to specify earlier arguments of a function of several arguments; 
e.g., we write $B_i(a)$ instead of $B(i)(a)$.

We conclude this brief introduction by recalling two important internally definable properties of types:
  we call the type $X$ \fat{contractible}, if $X$ is inhabited by a unique element, that is, if the following type is inhabited:
  \[ \iscontr(X) := \sm{x : X} \prd{x' : X} x' = x \enspace . \]
  We call the type $Y$ \fat{a proposition} if for all $y,y':Y$, we have $y = y'$.
  Note that a type $X$ is contractible iff $X$ is a proposition and there is an element $x:X$ (see also \cite[Lemma 3.11.3]{hottbook}).


