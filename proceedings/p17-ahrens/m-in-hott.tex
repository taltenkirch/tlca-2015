

\emph{Coinductive} types represent \emph{potentially infinite} data structures, 
such as streams or infinite lists.
As such, they have to be contrasted to \emph{inductive} datatypes, which represent structures that are \emph{necessarily finite},
such as unary natural numbers or finite lists.


\subsection{Signatures, a.k.a.\ containers}

In order to analyze inductive and coinductive types systematically, one usually fixes a notion of \enquote{signature}: 
a signature specifies, in an abstract way, the rules according to which the instances of a data structure are built.
In the following, we consider signatures to be given by \enquote{containers} \cite{indexed_containers}:
\begin{definition}
A \fat{container} (or \fat{signature}) is a pair $(A,B)$ of a type $A$ and a dependent type $x:A\entails B(x):\type$ over $A$.
\end{definition}
The container $(A,B)$ then determines a type of \enquote{trees} built as follows:
such a tree consists of a \emph{root node}, labeled by an element $a:A$, and a family of trees---\enquote{subtrees} of the original tree---indexed by the type $B(a)$.
A tree is \emph{well-founded} if it does not have an infinite chain of subtrees.

To the container $(A,B)$ one associates two types of trees built according to those rules: the type $\W(A,B)$ of well-founded trees, 
and the type $\M(A,B)$ of all trees, i.e., not necessarily well-founded.


The description of the inhabitants of $\W(A,B)$ and $\M(A,B)$ in terms of trees gives a suitable intuition; formally, those types are defined in terms of a
\emph{universal property}.
Indeed, $\M(A,B)$ will be defined as (the carrier of) a terminal object in a suitable sense. 


% In this section we define a class of coinductive types known as $\M$-types.
% An $\M$-type is specified by a type $A$ and a dependent type $x:A\entails B(x):\type$.
% Given $(A,B)$ as before, the inhabitants of the type $\M(A,B):\type$ can be visualized as \fat{infinite} trees with nodes labeled by inhabitants of $A$ such that
% a node labeled by $a:A$ has subtrees indexed by $B(a)$.
% 
% The same data $(A,B)$ also specifies a type $\W(A,B)$ of \fat{finite} trees.
% As explained in \Cref{todo}, \fat{in set-theoretic semantics} the theory of $\M$-types is dual to that of $\W$-types. However, \fat{this is not the case in dependent type theory}.
% If, on the other hand, \fat{finite}




\subsection{Coalgebras for a signature}


Any container $(A,B)$ specifies an endomorphism on types as follows:

\begin{definition}\label{def:poly_functor}
 Given a container $(A,B)$, define the \fat{polynomial functor} $P : \type \to \type$ associated to $(A,B)$ as
 \[P(X):=P_{A,B}(X):= \sm{a:A}(B(a) \to X) \enspace .\] 
 Given a map $f : X \to Y$, define $Pf: PX \to PY$ as the map
 \[ Pf(a,g) := (a, f \circ g) \enspace . \]
\end{definition}

Note that Definition \ref{def:poly_functor} does not really define a functor, and, more fundamentally, the universe $\type$ is not a (pre-)category
in the sense of \cite{rezk_completion}.
Instead, the appropriate notion for $P$ would be an $\infty$-(endo)functor on the $(\infty,1)$-category $\type$ \cite{james_cranch}.
However, we do not attempt to make any of these notions precise, and do not make use of any \enquote{functorial} properties of the defined maps.
Our use of the word \enquote{functor} merely indicates an analogy to the 1-categorical case.


To any signature $S=(A,B)$ we associate a type of 
\emph{coalgebras} $\Coalg_{S}$, and a family of types of morphisms between them:

\begin{definition}\label{def:coalgebra}
 Given a signature $S = (A,B)$ as in Definition \ref{def:poly_functor}, an \fat{$S$-coalgebra} is defined to be a pair $(C,\gamma)$ consisting of 
 a type $C:\type$ and a map $\gamma:C \to P_{S}C$.
 A map of coalgebras from $(C,\gamma)$ to $(D,\delta)$ is defined to be a pair $(f,p)$ of a map $f : C\to D$ and a path $p : \comp{f}{\delta} = \comp{\gamma}{P_Sf} $.
 Put differently, we set
  \[ \Coalg_S := \sm{C:\type} C\to PC \] and
  \[ \Coalg_S\Bigl( (C,\gamma),(D,\delta) \Bigr) := \sm{f : C\to D} \comp{f}{\delta} = \comp{\gamma}{Pf}  \enspace . \]
\end{definition}

\noindent
There is an obvious composition of coalgebra morphisms, and the identity map $C \to C$ is the carrier of a coalgebra endomorphism on $(\C,\gamma)$.
We also write $\Coalgmor{(C,\gamma)}{(D,\delta)}$ for the type of coalgebra morphisms from $(C,\gamma)$ to $(D,\delta)$.

\subsection{What is an \texorpdfstring{$\M$-type}{M-type}?}

In this section we define (internal) $\M$-types in HoTT via a universal property.

\begin{definition}\label{def:m-type}
 Given a container $(A,B)$, \fat{the (internal) $\M$-type $\M_{A,B}$ associated to $(A,B)$} 
 is defined to be the pair $(M,\out : M \to P_{A,B}M)$ with the following universal 
 property: for any coalgebra $(C,\gamma) : \Coalg_{(A,B)}$ of $(A,B)$, the type of coalgebra morphisms $\Coalgmor{(C,\gamma)}{(M,\out)}$ from $(C,\gamma)$ to $(M,\out)$ is contractible.
\end{definition}

The use of the definite article in Definition \ref{def:m-type} is justified by the following lemma:
\begin{lemma}\label{lem:is-prop-final}
 The type 
  \[\Final_S:= \sm{(X,\rho):\Coalg_S}\prd{(C,\gamma):\Coalg_S}\iscontr\big(\Coalgmor{(C,\gamma)}{(X,\rho)}\big)\] 
 is a proposition.
\end{lemma}
\begin{proof}
 The proof that any two final coalgebras $(L, \out)$ and $(L', \out')$ have equivalent carriers is standard. 
 The Univalence Axiom then implies that the carriers are (pro\-po\-sitionally) equal, $L = L'$. 
 It then remains to show that the coalgebra structure $\out$, when transported along this identity, is equal to $\out'$. 
 We refer to the formalized proof for details. 
\end{proof}

That is, any inhabitant of $\Final_S$ is necessarily unique up to propositional equality. 
We refer to this inhabitant as the \enquote{final coalgebra}.
In Section \ref{sec:derive-m} we construct the final coalgebra.

In the introduction, we use the adjectives \enquote{internal} and \enquote{external} to distinguish between types specified via
universal properties and type-theoretic rules, respectively. Since we do not consider rules for $\M$-types (that is, external $\M$-types) in this work,
we drop the adjective \enquote{internal} in what follows.

In the following example, we anticipate the result of the next section, namely the existence of a final coalgebra for any signature $(A,B)$:
\begin{example}\label{example:streams}
The coinductive type $\stream(A_0)$ of streams over a base type $A_0$ is given
by $\M(A,B)$ with $A = A_0$ and $B(a):= 1$ for any $a:A_0$.  The corresponding
polynomial functor $P$ satisfies $P(X) = A_0 \times X$.

Using finality of $\M(A,B)$ we can define maps into streams and prove that they
have the expected computational behaviour.
For example, the $\zip$ function
\[
\zip : \stream(A) \times \stream(B) \to \stream(A \times B)
\]
can be obtained from the universal property applied to the coalgebra
\[
\begin{array}{l}
\theta : \stream(A) \times \stream(B) \to (A \times B) \times (\stream(A) \times \stream(B)) \\
\theta (xs, ys) := ((\head(xs), \head(ys)), (\tail(xs), \tail(ys))
\end{array}
\]
where $\head : \stream(X) \to X$ and $\tail : \stream(X) \to \stream(X)$ are the
two components of the final coalgebra $\out$.
The computational behaviour of $\zip$ is expressed by the fact that $\zip$ is a
coalgebra morphism
\[
\zip(xs, ys) = \cons((\head(xs), \head(ys)), (\zip (\tail(xs), \tail(ys)))),
\]
where $\cons = \out^{-1}$.
\end{example}


