%In proof theory, the want for a fine-grained understanding of the dynamics at work in the \cut-elimination
%procedure led to the introduction of linear logic~\cite{Girard1987}. In this logic, the structural rules
%(contraction, the rule that allows to copy information, in particular) are only available in certain situations,
%namely on formulas of the form $\oc A$, the modality $\oc$ being called the \emph{exponential} modality of 
%linear logic.

%The last decades have witnessed a shift in the focus of proof theory towards a more operational point of view,
%triggered by the \cut-elimination theorem by Gentzen.
%This results and its interpretation computational interpretation allow to view
%the \cut-elimination theorem can be regarded as a computation system.

From the perspective of the Curry-Howard (or propositions-as-types) correspondence~\cite{Gallier1995}, 
a proof of $A\Rightarrow B$ in a logic enjoying a 
\cut-elimination procedure can be seen as a program that inputs (through the \cut rule) a proof of $A$ 
and outputs a \cut-free proof of $B$. 

Coming from this dynamic point of view, linear logic~\cite{Girard1987}
makes apparent the distinction between data that can or cannot be copied/erased \via 
its exponential modalities and retains the symmetry of classical logic:
the linear negation $\lneg{(\cdot)}$ is an involutive operation.
%
The study of \cut-elimination is easier in this setting thanks to the linearity constraint. However,
in its sequent calculus presentation, the \cut-elimination procedure of linear logic still
suffers from the common flaw of these type of calculi: commutative conversions.

\begin{center}
\begin{prooftree}
	\Theproof{\pi}{\lneg A , \lneg B, C,D,\Gamma}
	\Infer{1}[$\parr$]{\lneg A\parr \lneg B,C,D,\Gamma}
	\Infer{1}[$\parr$]{\lneg A\parr \lneg B,C\parr D,\Gamma}
	\Theproof{\mu}{A}
	\Theproof{\nu}{B}
	\Infer{2}[$\otimes$]{A\otimes B}
	\Infer{2}[\tt cut]{C\parr D,\Gamma}
\end{prooftree}
\quad$\rightarrow$\qquad
\begin{prooftree}
	\Theproof{\pi}{\lneg A , \lneg B, C,D,\Gamma}
	\Infer{1}[$\parr$]{\lneg A\parr \lneg B,C, D,\Gamma}
	\Theproof{\mu}{A}
	\Theproof{\nu}{B}
	\Infer{2}[$\otimes$]{A\otimes B}
	\Infer{2}[\tt cut]{C,D,\Gamma}
	\Infer{1}[$\parr$]{C\parr D,\Gamma}
\end{prooftree}
\end{center}

In the above reduction, one of the two formulas related by the \cut rule is introduced deeper in the proof,
making it impossible to perform an actual elimination step right away: one needs first to \emph{permute}
the rules in order to be able to go on.

This type of step is called a \emph{commutative conversion} and their presence complexify a lot the study of the 
\cut-elimination procedure,
as one needs to work \modulo an equivalence relation on proofs that is not {orientable} into a rewriting 
procedure in an obvious way: there are for instance situations of the form
%
\begin{center}
{
\begin{prooftree}
	\Theproof{\pi_1}{\lneg A,\lneg B, \Gamma}
	\Theproof{\pi_2}{A}
	\Infer{2}[\tt cut]{\vdash\lneg B,\Gamma}
	\Theproof{\pi_3}{B}
	\Infer{2}[\seqrule{cut}]{\vdash\Gamma}
\end{prooftree}
}
\quad~$\leftrightarrow$~\qquad
%\scalebox{0.83}
{
\begin{prooftree}
	\Theproof{\pi_1}{\lneg A,\lneg B, \Gamma}
	\Theproof{\pi_3}{B}
	\Infer{2}[\tt cut]{\vdash\lneg A,\Gamma}
	\Theproof{\pi_2}{A}
	\Infer{2}[\tt cut]{\vdash\Gamma}
	
\end{prooftree}
}
\end{center}
%
where it is not possible to favor one side of the equivalence without further non-local knowledge of the proof.
%
The point here is that, as a language for describing proofs, sequent calculus is somewhat \emph{too explicit}.
For instance, the fact that the two proofs
\begin{center}
\begin{prooftree}
	\Theproof{\pi}{A , B, C,D, \Gamma}
	\Infer{1}[$\parr$]{A\parr B,C,D, \Gamma}
	\Infer{1}[$\parr$]{A\parr B,C\parr D, \Gamma}
\end{prooftree}
\ \quad and \qquad
\begin{prooftree}
	\Theproof{\pi}{A , B, C,D, \Gamma}
	\Infer{1}[$\parr$]{A, B,C\parr D, \Gamma}
	\Infer{1}[$\parr$]{A\parr B,C\parr D, \Gamma}
\end{prooftree}
\end{center}
are different objects from the point of view of sequent calculus generates the first commutative conversion
we saw above.

A possible solution to this issue is to look for more intrinsic description of proofs, to find a language
that is more \emph{synthetic}; if possible to the point where we have no commutative conversions to perform
anymore.

Introduced at the same time as linear logic, the theory of \emph{proofnets}~\cite{Girard1987,Girard1996}
partially addresses this issue. The basic idea is to describe proofs as graphs rather than trees,
where application of logical rules become local graph construction, thus erasing some inessential sequential
informations. Indeed, the two proofs above would translate into the same proofnet:
%
\begin{proofnet}
\pnsomenet[R]{\large $\mathcal R_\pi$}{3cm}{1.2cm}
\pnoutfrom{R.-27}{$\Gamma$}[3.6]
\pnoutfrom{R.-50}[D]{$D$}
\pnoutfrom{R.-97}[C]{$C$}
\pnoutfrom{R.-140}[B]{$B$}
\pnoutfrom{R.-155}[A]{$A$}
\pnpar{A,B}{$A\parr B$}
\pnpar{C,D}{$C\parr D$}
\end{proofnet}
%
(where $\mathcal R_\pi$ is the proofnet translation of the rest of the proof) and the corresponding
commutative conversion disappears.

For the multiplicative without units fragment of linear logic (\mllm), proofnets yield an entirely satisfactory
solution to the problem, and constitute a low-complexity canonical representation of proofs based on local
operations on graphs.

By canonical, we mean here that two proofs are equivalent \modulo the permutations of rules induced by the commutative
conversions
if and only if they have the same proofnet translation.
%
From a categorical perspective, this means that proofnets constitute a syntactical 
presentation of the free semi-$\ast$-autonomous
category and a solution to the associated word problem~\cite{Heijltjes2014}.

Contrastingly, the linear logic community
has struggled to extend the notion of proofnets to wider fragment: even the question of 
\mll (that is, \mllm plus the multiplicative units) could not find a satisfactory answer.
%
A recent result~\cite{Heijltjes2014a} helps to understand this situation: proof equivalence of \mll is
actually a \Pspace-complete problem. Hence,
there is no hope for a satisfactory notion of low-complexity proofnet for this fragment%
\footnote{Of course, this applies only to the standard formulation of units: the equivalence problem
for any notion of multiplicative units enjoying less
permutations of rules could potentially still be tractable \via proofnets: see for instance the work 
of S.~Guerrini and A.~Masini~\cite{Guerrini2001} and D.~Hughes~\cite{Hughes2005a}}. 

In this article, we consider the same question, but in the case of \mallm: the multiplicative-additive
without units fragment of linear logic. Indeed, this fragment has so far
also resisted the attempts to build a notion of proofnet that at the same time characterizes proof equivalence
and has basic operations of tractable complexity: we have either canonical nets of exponential size~\cite{Hughes2005}
or tractable nets that are not canonical~\cite{Girard1996}. Therefore, it would have not been too surprising
to have a similar result of completeness for some untractable complexity class. An obvious candidate in that
respect would be \coNP: as we will see, one of these two approaches to proofnets for \mallm is related to Boolean formulas,
which equivalence problem is \coNP-complete.

It turns out in the end that this is not the case: our investigation concludes that the equivalence problem in \mallm
is \Logspace-complete under \aco reductions. But maybe more importantly, we uncover 
in the course of the proof an unexpected connexion
of this theoretical problem with a very practical issue: indeed we show that \mallm proofs are closely related
to binary decision diagrams.%, and vice versa.





