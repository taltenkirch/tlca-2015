
\newcommand{\Axiom}[1]{\Hypo{}\Infer{1}[\texttt{ax}]{#1}}

\begin{notation}
	The formulas of \mallm are built inductively from atoms which we write $\alpha,\beta,\gamma,\dots$
	their duals $\nalpha,\nbeta,\ngamma,\dots$ and the binary connectives $\parr,\otimes,\labwith x,\plus$
	\emph{(we consider that the $\twith$ connectives carry a label $x$ to simplify some reasonings, but we will
	omit it when it is not relevant)}.
	%
	We write formulas as uppercase letters $A,B,C,\dots$ unless we want to specify they are atoms.
	%
	\emph{Sequents} are sequences of formulas, written as greek uppercase letters $\Gamma,\Delta,\Lambda,\dots$
	such that all
	occurrences of the connective $\twith$ in a sequent carry a different label. The concatenation
	of two sequents $\Gamma$ and $\Delta$ is simply written $\Gamma,\Delta$.
\end{notation}

Let us recall the rules of \mallm\footnote{We consider a $\eta$-expanded version of \mallm,
which simplifies proofs and definitions, but the extension of our results to a version with non-atomic axioms
would be straightforward. Also, we work \modulo the exchange rule.}. We do not include the \cut rule in our study, since in a static situation
(we are not looking at the \cut-elimination procedure of \mallm) it can always be encoded \wloss
using the $\otimes$ rule.
\begin{center}
	\begin{prooftree}
		\Hypo{\alpha,\nalpha}
	\end{prooftree}
	%
	\hfill
	%
\begin{prooftree}
		\Hypo{\Gamma,A,B}
		\Infer{1}[$\rparr$]{\Gamma,A\parr B}
	\end{prooftree}
	%
	\hfill
	%
	\begin{prooftree}
		\Hypo{\Gamma,A}
		\Hypo{\Delta,B}
		\Infer{2}[$\otimes$]{\Gamma,\Delta,A\otimes B}
	\end{prooftree}
	%
	\hfill
	%
	\begin{prooftree}
		\Hypo{\Gamma,A}
		\Infer{1}[$\lplus$]{\Gamma,A\plus B}
	\end{prooftree}
	%
	\hfill
	%
	\begin{prooftree}
		\Hypo{\Gamma,B}
		\Infer{1}[$\rplus$]{\Gamma,A\plus B}
	\end{prooftree}
	%
	\hfill
	%
	\begin{prooftree}
		\Hypo{\Gamma,A}
		\Hypo{\Gamma,B}
		\Infer{2}[$\rlabwith x$]{\Gamma,A\labwith x B}
	\end{prooftree}
\end{center}
\textit{(by convention, we leave the axiom rule implicit to lighten notations.
Also, we will use the notation \begin{prooftree}\Theproof\pi\Gamma\end{prooftree}
for \enquote{the proof $\pi$ of conclusion $\Gamma$}.
)}

\begin{remark}\label{rem_proofs}
	Any time we will look at a \mallm proof from a complexity perspective, we will consider they are represented as
	trees with nodes corresponding to rules, labeled by the connective introduced and the sequent that is 
	the conclusion of the rule. 
%	The cases of \texttt{If}\:$\zero$, \texttt{DontCare}\:$\one,$\dots{} will be useful to obtain
%	\aco reductions in \cref{sec_bool} and \cref{sec_logspace}, since erasing a whole subpart of a graph is not something that is possible
%	in this complexity
%	class.
\end{remark}

%\smallskip
Two \mallm proofs $\pi$ and $\nu$ are said to be \emph{equivalent} (notation $\pi\malleq\nu$) if one can pass
from one to the other \via permutations of rules~\cite{Hughes2015}. We have an associated decision problem.

\newcommand{\mallmeq}{\mallm\problem{equ}\xspace}
\begin{definition}[\mallmeq]\label{def_mallmeq}
	\mallmeq is the decision problem: 
	\begin{center}
	{\it\enquote{Given two \mallm proofs $\pi$ and $\nu$ with the same conclusion, 
	do we have $\pi\malleq\nu$?}}
	\end{center}
\end{definition}

We will not go through all the details about this syntactic way to define proof equivalence in \mallm.
The reason
for this is that we already have an available equivalent characterization in terms of \emph{slicing}~\cite{Hughes2015}
which we review in \cref{sec_slicing}. Instead, let us focus only on the most significant case.

%The proof
\begin{center}
\begin{prooftree}
	\Theproof\pi{\Gamma,A,C}
	\Theproof\mu{\Gamma,B,C}
	\Infer{2}[$\rwith$]{\Gamma,A\with B,C}
	\Theproof\nu{\Delta,D}
	\Infer{2}[$\otimes$]{\Gamma,\Delta,A\with B,C\otimes D}
\end{prooftree}
%is equivalent to the proof
\quad{\large$\malleq$}\qquad
\begin{prooftree}
	\Theproof\pi{\Gamma,A,C}
	\Theproof\nu{\Delta,D}
	\Infer{2}[$\otimes$]{\Gamma,\Delta,A,C\otimes D}
	\Theproof\mu{\Gamma,B,C}
	\Theproof\nu{\Delta,D}
	\Infer{2}[$\otimes$]{\Gamma,\Delta,B,C\otimes D}
	\Infer{2}[$\rwith$]{\Gamma,\Delta,A\with B,C\otimes D}
\end{prooftree}
\end{center}
%
In the above equivalence, the $\otimes$ rule gets lifted above the $\with$ rule. But doing so, notice that
we created two copies
of $\nu$ instead of one, therefore the size of the prooftree has grown. Iterating on this observation, it is not hard to
build pairs of proofs that are equivalent, but one of which is exponentially bigger than
the other.
%
This is indeed where the difficulty of proof equivalence in \mallm lies. As a matter of fact, this
permutation of rules \emph{alone} would be enough to build the encoding of the equivalence problem 
of binary decision diagrams presented in \cref{sec_reduce}.

A way to attack proof equivalence in a logic, as we exposed in \cref{sec_twoface}, is to try to setup a notion of
proofnet for this logic. In the following, we will review the main two approaches to this idea in the case of \mallm:
\emph{monomial proofnets} by J.-Y.~Girard~\cite{Girard1996,Laurent2008a} and 
\emph{slicing proofnets} by \Hughes and \VGlab~\cite{Hughes2005,Hughes2015}.
We will then design an intermediate notion of \emph{\bdd slicing} that will be more suited to our needs.

