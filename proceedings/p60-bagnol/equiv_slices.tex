The idea of slicing dates back to J.-Y.~Girard's original article on proofnets for \mallm~\cite{Girard1996},
and was even present in the original article on linear logic~\cite{Girard1987}.
It amounts to the
natural point of view already evoked in \cref{sec_mono}, seeing the $\twith$ rule as introducing
superposed variants of the proof,
which are eventually to be selected from in the course of \cut-elimination.
If we have two alternative \emph{slices} for each $\twith$ connective of a sequent $\Gamma$ and all
combinations of slices can be selected independently, we readily see that the global number of slices will be
exponential in the number of $\twith$ connectives in $\Gamma$.

This is indeed the major drawback of the representation of proofs as set of slices: the size of objects
representing proofs may grow exponentially in the size of the original proofs. This of course impairs any fine-grained
analysis in terms of complexity.

\begin{definition}[slicing]
	Given a \mallm sequent $\Gamma$, a \emph{linking} of $\Gamma$ is a subset
	$$\extset[\big]{[\alpha_1,\nalpha_1]}{[\alpha_n,\nalpha_n]}$$
	of %$\dual(\Gamma)$,
	the set of (unordered) pairs of occurrences of dual atoms in $\Gamma$.
	
	Then, a \emph{slicing} of $\Gamma$ is a finite set of linkings of $\Gamma$.
	
	To any \mallm proof $\pi$, we associate a slicing $\slicing\pi$ by induction:

	\begin{itemize}
		\item If $\pi=\begin{prooftree}\Hypo{\alpha,\nalpha}\end{prooftree}$ then 
		      $\slicing\pi$ is the set containing only the linking $\big\{[\alpha,\nalpha]\big\}$
		
		\medskip
		\item If $\pi=\,
		      \begin{prooftree}
		      \Theproof{\mu}{\Gamma,A,B}
		      \Infer{1}[$\rparr$]{\Gamma,A\parr B}
		      \end{prooftree}$
		      \ \begin{minipage}[t]{10cm}
		      then $\slicing\pi=\slicing\mu$, where we see atoms 
		      of $A\parr B$ \\as the corresponding atoms of $A$ and $B$
		      \end{minipage}
		
		\medskip
		\item If $\pi=\,
		      \begin{prooftree}
		      \Theproof{\mu}{\Gamma,A}
		      \Theproof{\nu}{\Delta,B}
		      \Infer{2}[$\otimes$]{\Gamma,\Delta,A\otimes B}
		      \end{prooftree}$
		      then 
		      $\slicing\pi=\set{\lambda\cup\lambda'}{\lambda\in \slicing\mu\,,\,\lambda'\in\slicing\nu}$
		
		\medskip
		\item If $\pi=\,
		      \begin{prooftree}
		      \Theproof{\mu}{\Gamma,A}
		      \Infer{1}[$\lplus$]{\Gamma,A\plus B}
		      \end{prooftree}$
		      then 
		      $\slicing\pi=\slicing\mu$,
		and likewise for $\rplus$
		
		\medskip
		\item If $\pi=\,
		      \begin{prooftree}
		      \Theproof{\mu}{\Gamma,A}
		      \Theproof{\nu}{\Gamma,B}
		      \Infer{2}[$\rwith$]{\Gamma,\Delta,A\with B}
		      \end{prooftree}$
		      then 
		      $\slicing\pi=\slicing\mu \cup \slicing\nu$
	\end{itemize}
\end{definition}

%\TODOEXEMPLE

\begin{remark}
	In the $\otimes$ rule, it is clearly seen that the number of slices are multiplied. This is just what is
	needed in order to have a combinatorial explosion: for any $n$, a proof $\pi_n$ of
	$$\underbrace{{\nalpha}\otimes\cdots\otimes{\nalpha}}_{n\text{ times}},
	\underbrace{\extlist{\alpha\with \alpha}{\alpha\with \alpha}}_{n\text{ times}}$$
	obtained by combining with the $\otimes$ rule $n$ copies of the proof
	\begin{prooftree*}
		\Hypo{\nalpha,\alpha}
		\Hypo{\nalpha,\alpha}
		\Infer{2}[$\rwith$]{\nalpha,\alpha\with\alpha}
	\end{prooftree*}
	will be of linear size in $n$, but with a slicing $\slicing n$ containing $2^n$ linkings.
\end{remark}

Slicings (associated to a proof) correspond exactly to the notion of proofnets elaborated by
\Hughes and \VGlab~\cite{Hughes2005}. 
While their study was mainly focused on the problems of finding a correctness criterion and
designing a \cut-elimination procedure for these, it also covers the proof equivalence problem.
The proof that their notion of proofnet characterizes \mallm proof equivalence
can be found in an independent note~\cite{Hughes2015}.

\begin{theorem}[slicing equivalence~\protect{\cite[Theorem~1]{Hughes2015}}]\label{th_equiv}
	Let $\pi$ and $\nu$ be two \mallm proofs.
	%
	We have that $\pi\malleq\nu$ if and only if $\slicing\pi=\slicing\nu$.
\end{theorem}

%We will use this characterization in the reductions between proof equivalence in \mallm and equivalence of
%\BDD. 
Let us also end this section
with a graphical representation of an example of proofnet from the article of Hughes and van Glabbeek,
encoding the proof on the left
with three linkings $\lambda_1, \lambda_2, \lambda_3$:

\medskip
\begin{center}
	\scalebox{0.75}{
	\begin{prooftree}[center=false]
		\Hypo{\lneg P,P}
		\Infer{1}[$\lplus$]{\lneg P\oplus \lneg Q,P}
		\Infer{1}[$\lplus$]{(\lneg P\oplus \lneg Q)\oplus \lneg R,P}
		\Hypo{\lneg Q,Q}
		\Infer{1}[$\rplus$]{\lneg P\oplus \lneg Q,Q}
		\Infer{1}[$\lplus$]{(\lneg P\oplus \lneg Q)\oplus \lneg R,P}
		\Infer{2}[$\with$]{(\lneg P\oplus \lneg Q)\oplus \lneg R,P\with Q}
		\Hypo{\lneg R,R}
		\Infer{1}[$\rplus$]{(\lneg P\oplus \lneg Q)\oplus \lneg R,P}
		\Infer{2}[$\with$]{(\lneg P\oplus \lneg Q)\oplus \lneg R,(P\with Q)\with R}
	\end{prooftree}
	}
	\hfill
	\scalebox{0.8}{
	\includegraphics[width=5cm]{hvg_mall}
	}
\end{center}

