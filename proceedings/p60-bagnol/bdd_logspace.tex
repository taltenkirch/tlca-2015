We prove in this section that all these equivalence problems are \Logspace-complete. We begin by listing
a few useful properties of \bdd that will allow to design a \Logspace decision procedure for their equivalence.
Then, we prove the \Logspace-hardness by reducing to \obddequ a \Logspace-complete problem on line graph orderings.

The starting point is the good behavior of \bdd with respect to negation. In the following lemma, we consider
the negation of a \bdd which is not strictly speaking a \bdd itself:
we think of it as the equivalent Boolean formula, the point
being precisely to show that this Boolean formula can be easily expressed as a \bdd.

\begin{lemma}[negation]\label{lem_neg}
	If $\phi$ and $\psi$ are \bdd and $X$ is either $\zero$, $\one$ or a variable, we have
	$$\neg{\itef X\phi\psi}\,\booleq\,\itef X{\neg\phi}{\neg\psi}$$
	%
	$$\neg{\dcare X\phi}\,\booleq\,\dcare X{\neg\phi}$$
\end{lemma}

\begin{proof}
	First we can transform our expression by
	$$\neg{\itef X\phi\psi}\,\booleq\, \neg{X\p\phi+\neg X\p\psi}\,\booleq\,(\neg X+\neg\phi)\p(X+\neg\psi)
	\,\booleq\, X\p\neg\psi+\neg X\p\neg\phi+\psi\p\phi$$
	then we can apply the so-called \enquote{consensus rule} of Boolean formulas
	$$X\p\neg\psi+\neg X\p\neg\phi+\psi\p\phi\,\booleq\,
	 X\p\neg\phi+\neg X\p\neg\psi\,\booleq\, \itef X{\neg \phi}{\neg\psi}$$
	The case of \texttt{DontCare} is obvious.
\end{proof}

\begin{corollary}
	If $\phi$ is a \ite, then there is a \bdd $\ineg \phi$ such that
	$\neg\phi\booleq\ineg\phi$.
	
	Moreover $\ineg \phi$ can be computed in logarithmic space.
\end{corollary}

\begin{proof}
	An induction on the previous lemma shows that we can obtain the negation of a \bdd simply by flipping
	the $\zero$ nodes to $\one$ nodes and conversely. Hence the transformation is even in \aco.
\end{proof}

Then, we show that a \bdd can be seen as a sum of monomials through a \Logspace transformation.

\begin{lemma}[\ite as sums of monomials]
	If $\phi$ is a \bdd, then there is a formula $\idnf\phi$ which is a sum of monomials
	and is such that $\phi\booleq\idnf\phi$.
	
	Moreover $\idnf \phi$ can be computed in logarithmic space.
\end{lemma}

\begin{proof}
	For each $\one$ node in $\phi$, go down to the root of $\phi$ and output one by one the variables of
	any $\itef x{\cdot}{\cdot}$ encountered: this produces the monomial associated to this $\one$ node. Then
	$\idnf \phi$ is the sum of all the monomials obtained this way and is clearly equivalent to $\phi$.
	%
	The procedure is in \Logspace because we only need to remember which $\one$-leave we are treating and where we are
	in the tree (when going down) at any point.
\end{proof}

Putting all this together, we finally obtain a space-efficient decision procedure. Note however that it is totally
sub-optimal in terms of time: to keep with the logarithmic space bound, we have to recompute a lot of things
rather than store them.

\begin{corollary}[\bddequ is in \Logspace]
	There is a logarithmic space algorithm that, given two \bdd $\phi$ and $\psi$, decides
	wether they are equivalent.
\end{corollary}

\begin{proof}
	The \bdd $\phi$ and $\psi$ are equivalent if and only if 
	$\phi\Leftrightarrow\psi=(\neg\phi+\psi)\p(\neg\psi+\phi)\booleq 1$ that is to say (by passing to the
	negation)
	$(\neg\psi\p\phi)+(\neg\phi\p\psi)\booleq \zero$, which holds if and only if
	both $\neg\psi\p\phi\booleq\zero$ and $\neg\phi\p\psi\booleq \zero$.

	But then, considering the first one (the other being similar) we can rewrite it in logarithmic space
	using the two above lemmas as $\idnf{(\ineg\psi)}\p\idnf{\phi}\booleq\zero$.
	%
	This holds if and only if for all pairs $(\monof m,\monof m')$ of one monomial in 
	$\idnf{(\ineg\psi)}$ and one monomial
	in $\idnf{\phi}$, $\monof m$ and $\monof m'$ are in conflict; which can be checked in logarithmic space
	using \cref{rem_compat}.
\end{proof}

Let us now introduce an extremely simple, yet \Logspace-complete problem~\cite{Etessami1997}, which will ease the
\Logspace-hardness part of our proof.

\newcommand{\ord}{\problem{ORD}\xspace}
\begin{definition}[order between vertices]
\emph{Order between vertices} (\ord) is the following decision problem:
%\shrinkspace
	\begin{center}
	{\it\enquote{%
	Given a directed graph $G=(V,E)$ that is a line%
	%
	\footnote{We use the standard definition of graph as a pair $(V,E)$ of sets of vertices and edges (oriented
	couples of vertices $x\rightarrow y$).
	A graph is a \emph{line} if it is connected and all the vertices have in-degree and out-degree $1$, except the
	\emph{begin} vertex which has in-degree $0$ and out-degree $1$ and the \emph{exit} vertex
	which has in-degree $1$ and out-degree $0$. A line induces a total order on vertices through its transitive
	closure}
	%
	and two vertices $f,s\in V$\\
	do we have 
	$f<s$ in the total order induced by $G$?}}
	\end{center}
\end{definition}

\begin{lemma}
	\ord reduces to \obddequ in \aco.
\end{lemma}

\begin{proof}
	Again we are going to build a local graph transformation that is in \aco.
	
	First, we assume \wloss that the begin $b$ and the exit $e$ vertices of $G$ are different from $f$ and $s$. We write
	$f^+$ and $s^+$ the vertices immediately after $f$ and $s$ in $G$.
	
	Then, we perform a first transformation by replacing the graph with three copies of itself
	(this can be done by locally scanning the graph and create labeled copies of the vertices and edges).
	We write $x_i$
	to refer to the copy of the vertex $x$ in the graph $i$.
%
	The second transformation is a rewiring of the graph as follows: erase the edges going out of the $f_i$
	and $s_i$ and replace them as pictured in the two first subgraphs:
%	$$f_1 \rightarrow f_2^+\qquad f_2 \rightarrow f_3^+\qquad f_3 \rightarrow f_1^+$$
%	$$s_1 \rightarrow s_2^+\qquad s_2 \rightarrow s_1^+\qquad s_3 \rightarrow s_3^+$$
\newcommand{\compress}{\vspace{-2pt}}
\compress
\begin{center}
\input{pic_fs}
\qquad\qquad\qquad
\input{pic_xy}
\end{center}
\compress
%
Let us call $G_r$ the rewired graph and $G_n$ the non-rewired graph. To each of them we add two binary nodes
$x$ and $y$ connected to the begin vertices $b_i$ as pictured in the third graph above.
%
%\compress
%\begin{center}
%
%\end{center}
%%
%\compress
Then we can produce two corresponding \obdd{} $\phi_r$ and $\phi_n$ by replacing the exit vertices $e_1$, $e_2$, $e_3$ by $\one$, $\zero$, $\zero$ 
respectively, $x$ and $y$ by a $\itef {x}{(\dcare y\cdot)}{(\itef y\cdot\cdot)}$ block of nodes; and
any other $v_i$ vertex by a $\dcare v{\cdot}$. 
It is then easy to see that if $f<s$ in the order induced by $G$ if and only if $\phi_r$ and $\phi_n$ are equivalent.

Let us illustrate graphically what happens in the case where $f<s$: we draw the resulting \obdd{} as a labeled
graph with the convention that a node labeled with $z$ with out-degree $1$ is a $\dcare z\cdot$ and a node labeled
with $z$ with out-degree $2$ is a $\itef z\cdot\cdot$ node with the upper edge corresponding to the \texttt{Then}
branch and the lower edge corresponding to the \texttt{Else} branch.
\begin{center}
\input{pic_ord_ex}
\end{center}
\vspace{-0.5cm}
\end{proof}

\begin{remark}
	The above construction relies on the fact that there are non-commuting permutations on the set of three
	elements: in a sense we are just attributing two non-commuting $\sigma$ and $\tau$ to $f$ and $s$ and make
	sure that the order in which they intervene affects the equivalence class of the resulting \obdd{}. An approach
	quite similar in spirit with the idea of \emph{permutation branching program}~\cite{Barrington1989}.
\end{remark}

We can now extend our chain of reductions with the two new elements from this section
\begin{center}%\footnotesize
	\ord\ {\footnotesize(\Logspace-hard)}\quad$\rightarrow$\quad 
	 \obddequ\quad $\rightarrow$\quad 
	 \mallmeq\quad $\rightarrow$\quad \bddequ\ {\footnotesize($\in$\,\Logspace)}
	 %
%	 \footnotesize(\Logspace-hard)\hspace{6.4cm}($\in$\,\Logspace)\hspace{0.6cm}~
\end{center}
so in the end we get our main result:

\begin{theorem}[\Logspace-completeness]
	The decision problems
	\obddequ, \mallmeq and \bddequ are
	\Logspace-complete under \aco reductions.
\end{theorem}


