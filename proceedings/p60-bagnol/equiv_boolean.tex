We finally introduce an intermediate notion of representation of proofs which will be a central tool
in the next section.
In a sense, it is a synthesis of monomial proofnets and slicings: acknowledging the fact that slicing makes
the size of the representation explode, we rely on \bdd to keep things more compact.

Of course, the canonicity property is lost. But this is exactly the point! Indeed, deciding whether two
\enquote{\bdd slicings} are equivalent is the reformulation of proof equivalence in \mallm we 
rely on
in the reductions between \mallmeq (\cref{def_mallmeq}) and \bddequ (\cref{def_bddequ}).
%to prove that this problem lies in \Logspace.


\begin{definition}[\bdd slicing]\label{def_boolslicing}
	Given a \mallm sequent $\Gamma$, a \emph{\bdd slicing} of $\Gamma$ is a function $\slfont B$ 
	that associates a \bdd to every element $[\gamma,\ngamma]$
	of %$\dual[\Gamma]$,
	the set of (unordered) pairs of occurrences of dual atoms in $\Gamma$.

	
	We say that two \bdd slicings $M,N$ of the same $\Gamma$ are \emph{equivalent}
	(notation $M\sliceeq N$) if for any pair $[\gamma,\ngamma]$,
	we have $M[\gamma,\ngamma]\booleq N[\gamma,\ngamma]$ in the sense of \cref{def_booleq}.
	
	To any \mallm proof $\pi$, we associate a \bdd slicing $\mslicing\pi$ by induction:
	\begin{itemize}
		\item If $\pi=\begin{prooftree}\Hypo{\alpha,\nalpha}\end{prooftree}$ then 
		      $\mslicing\pi[\alpha,\nalpha]=\unit$.
		
		\medskip
		\item If $\pi=\,
		      \begin{prooftree}
		      \Theproof{\mu}{\Gamma,A,B}
		      \Infer{1}[$\rparr$]{\Gamma,A\parr B}
		      \end{prooftree}$
		      \ \begin{minipage}[t]{10.2cm}
%		      then $\mslicing\pi[\gamma,\ngamma]=\dcare\one{(\mslicing\mu[\gamma,\ngamma])}$ where we see atoms 
		      then $\mslicing\pi[\gamma,\ngamma]=\mslicing\mu[\gamma,\ngamma]$ where we see atoms 
		      of $A\parr B$ as the corresponding atoms of $A$ and $B$
		      \end{minipage}
		
		\medskip
		\item If $\pi=\,
		      \begin{prooftree}
		      \Theproof{\mu}{\Gamma,A}
		      \Theproof{\nu}{\Delta,B}
		      \Infer{2}[$\otimes$]{\Gamma,\Delta,A\otimes B}
		      \end{prooftree}$
		      then 
%		      
		      $\mslicing\pi[\gamma,\ngamma]=
		      \begin{cases}
		      \mslicing\mu[\gamma,\ngamma] &\text{ if $\gamma,\ngamma$ are atoms of $\Gamma,A$}\\
		      \mslicing\nu[\gamma,\ngamma] &\text{ if $\gamma,\ngamma$ are atoms of $\Delta,B$}\\
		      \zero &\text{ otherwise\footnotemark}
		      \end{cases}
		      $
%		      \itef\zero{(\mslicing\mu[\gamma,\ngamma])}{(\mslicing\nu[\gamma,\ngamma])}
%		      		&\text{ if $\alpha,\nalpha$ are atoms of $\Gamma,A$}\\
%		      \itef\one{(\mslicing\mu[\gamma,\ngamma])}{(\mslicing\nu[\gamma,\ngamma])}
%		      		&\text{ if $\alpha,\nalpha$ are atoms of $\Delta,B$}\\

		
		\medskip
		\item If $\pi=\,
		      \begin{prooftree}
		      \Theproof{\mu}{\Gamma,A}
		      \Infer{1}[$\lplus$]{\Gamma,A\plus B}
		      \end{prooftree}$
\begin{minipage}[t]{10cm}
		      then
		      $\mslicing\pi[\gamma,\ngamma]=
		      \begin{cases}
		      \mslicing\mu[\gamma,\ngamma] &\text{ if $\gamma,\ngamma$ are atoms of $\Gamma,A$}\\
		      \zero &\text{ otherwise}
		      \end{cases}
		      $
		      
		      and likewise for $\rplus$.
\end{minipage}

%		      ~\hfill$\mslicing\pi[\gamma,\ngamma]=
%		      \begin{cases}
%		      \dcare\one{(\mslicing\mu[\gamma,\ngamma])} &\text{ if $\alpha,\nalpha$ are atoms of $\Gamma,A$}\\
%		      \zero &\text{ otherwise}
%		      \end{cases}


		
		
		\medskip
		\item If $\pi=\,
		      \begin{prooftree}
		      \Theproof{\mu}{\Gamma,A}
		      \Theproof{\nu}{\Gamma,B}
		      \Infer{2}[$\rlabwith x$]{\Gamma,\Delta,A\labwith x B}
		      \end{prooftree}$
%		      \ \begin{minipage}[t]{10cm}
		      then
		      
		      \hfill$\mslicing\pi[\gamma,\ngamma]=
		      \begin{cases}
		      \itef x{\mslicing\mu[\gamma,\ngamma]}\zero &\text{ if $\gamma$ or $\ngamma$ is an atom of $A$}\\
		      \itef x\zero{\mslicing\nu[\gamma,\ngamma]} &\text{ if $\gamma$ or $\ngamma$ is an atom of $B$}\\
		      \itef x{\mslicing\mu[\gamma,\ngamma]}{\mslicing\nu[\gamma,\ngamma]} &\text{ otherwise}
		      \end{cases}
		      $
%		      \end{minipage}
	\end{itemize}
\end{definition}
\footnotetext{Remember we consider \emph{occurrences} of atoms, and as the $\otimes$ rule splits the
context into two independent parts that and no axiom rule can cross this splitting.}

\newcommand{\tparr}{\!\parr\!}
%\begin{remark}
%	The choice of \bdd constructors in the above definition might look a bit contrived at first. Indeed, we
%	could have made obvious simplifying choices, as for instance having simply
%	$\mslicing\mu[\gamma,\ngamma]$ instead of $\dcare\one{(\mslicing\mu[\gamma,\ngamma])}$ in the case of
%	the $\tparr$ rule. But then, we might have been struggling a little to show that the translation is
%	doable in \aco, which would have complicated our completeness proof.
%\end{remark}

\begin{remark}
	The \bdd we obtain this way are actually of a specific type: they are usually called \emph{read-once}
	\bdd: from the root to any leave, one never
	crosses two \texttt{IfThenElse} nodes asking for the value of 
	the same variable.
\end{remark}

\begin{example}
	The weight of the pairs $[\alpha,\nalpha]$ and $[\delta,\ndelta]$ in the \bdd slicing of the proof
	\begin{center}
	$\pi=\ $
	\begin{prooftree}
	\Hypo{\alpha,\nalpha}
	\Infer{1}[$\lplus$]{\alpha\oplus\beta,\nalpha}
	\Hypo{\beta,\nbeta}
	\Infer{1}[$\rplus$]{\alpha\oplus\beta,\nbeta}
	\Infer{2}[$\rlabwith x$]{\alpha\oplus\beta,\nalpha\labwith x\nbeta}
	\Hypo{\delta,\ndelta}
	\Infer{2}[$\otimes$]{\alpha\oplus\beta,(\nalpha\labwith x\nbeta)\otimes\delta,\ndelta}
	\end{prooftree}
	\end{center}
	are $\mslicing\pi[\alpha,\nalpha]=\itef x{\one}{\zero}$ and 
	$\mslicing\pi[\delta,\ndelta]=\one$.
\end{example}

It is not hard to see that proof equivalence matches the equivalence of \bdd slicings by relating
them to slicings from the previous section.

\begin{theorem}[\bdd slicing equivalence]
	Let $\pi$ and $\nu$ be two \mallm proofs.
	%
	We have that $\pi\malleq\nu$ if and only if $\mslicing\pi\sliceeq\mslicing\nu$.
\end{theorem}

\begin{proof} We show in fact that $\slicing\pi=\slicing\nu$ if and only if $\bslicing\pi\booleq\bslicing\nu$,
	with \cref{th_equiv} in mind.
%	Consider the function $f$ from \bdd slicings to slicings:
	\newcommand{\mB}{\mathcal B}
	
	To a \bdd slicing $\mB$, we can associate a linking $v(\mB)$ for each valuation $v$ of the variables
	occurring in $\mB$ by setting
	$v(\mB)=\set{[\alpha,\nalpha]}{v(\mathcal B[\alpha,\nalpha])=\one}$ and then a slicing
	$f(\mB)=\set{v(\mB)}{v \text{ valuation}}$. By definition, it is clear that if $\mB$ and $\mB'$
	involve the same variables and $\mB\sliceeq\mB'$ then $f(\mB)=f(\mB')$.
	
	Conversely, suppose $\bslicing\pi\not\booleq\bslicing\nu$, so that there is a $v$ such that
	$v(\bslicing\pi)\neq v(\bslicing\nu)$. To conclude that $f(\bslicing\pi)\neq f(\bslicing\nu)$, we must show 
	that there is no other $v'$ such that $v'(\bslicing\nu)=v(\bslicing\pi)$.
	
	To do this, we can extend the notion of valuation to proofs: if $v$ is a valuation of
	the labels $x$ of the $\tlabwith x$ in $\pi$, $v(\pi)$ is defined by keeping only the left or right
	branch of $\tlabwith x$ according to the value of $x$.
	Now we can consider the set $v^{\tt ax}(\pi)$ of axiom rules in $v(\pi)$
	% and see that it corresponds exactly to a 
%	linking of $\slicing\pi$.
	and we can show by induction that $v^{\tt ax}(\pi)$ must contain at least one
	pair with one atom which is a subformula of the side of each $\tlabwith x$ that has been kept. Therefore for any
	$\pi$ and $\nu$ with the same conclusion, if $v\neq v'$ we have $v^{\tt ax}(\nu)\neq v'^{\tt ax}(\pi)$
	no matter what.
	Then we can remark that $v^{\tt ax}(\pi)$ is just another name for $v(\bslicing\pi)$
	so that in the end, there cannot be $v\neq v'$ such that 
	$v'(\bslicing\nu)=v(\bslicing\pi)$.
	
	Finally, an easy induction shows that $f(\bslicing\pi)=\slicing\pi$ and therefore we are done.
\end{proof}



%\begin{lemma}[\ite]
%	For any \mallm proof $\pi$ and any pair $[\gamma,\ngamma]$,
%	$\bslicing \pi[\gamma,\ngamma]$ is a \bdd.
%\end{lemma}

Also, a \bdd equivalent to the \bdd associated to a pair can be computed in \aco.

\begin{lemma}[computing \bdd slicings]\label{lem_bsl}
	For any \mallm proof $\pi$ and any pair $[\gamma,\ngamma]$,
	we can compute in \aco a \bdd $\phi$ such that $\phi\booleq\bslicing \pi[\gamma,\ngamma]$.
\end{lemma}

\begin{proof} As we see proofs as labeled trees (\cref{rem_proofs}), we will only locally replace
the rules of the proof the following way to obtain the corresponding \bdd $\phi$:
\begin{itemize}
	\item Replace axiom rules \begin{prooftree}\Hypo{\gamma,\ngamma}\end{prooftree} by $\one$ and other axiom
	rules by $\zero$
	\item Replace all $\tparr$ and $\oplus$ rules by $\dcare\one{\cdot}$ nodes
	\item In the $\otimes$ case, test which side the atoms $\gamma,\ngamma$ are attributed to 
	\emph{(this can be done locally by looking at the conclusions of the premise of the rule)}
	and replace it by
	a $\itef \zero{\cdot}{\cdot}$ or a $\itef \one{\cdot}{\cdot}$ node accordingly
	\item Replace $\tlabwith x$ rules by a $\itef x{\cdot}{\cdot}$ nodes
\end{itemize}
We can see by induction that the resulting \bdd is equivalent to $\bslicing\pi[\gamma,\ngamma]$.
All these operations can be performed by looking only at the rule under treatment (and its immediate neighbors
in the case of $\otimes$) and always replaces one rule by exactly one node. Therefore it is in \aco.
\end{proof}

%Hence, proof equivalence in \mallm reduces in \aco to equivalence of \ite.
\begin{corollary}[reduction]
	\mallmeq reduces to \bddequ in \aco.
\end{corollary}

In the next section we focus on the equivalence of \bdd and \obdd{}, proving first that the case of \obdd{}
can be reduced to proof
equivalence in \mallm.
%
%it to be in the class \Logspace.
Then, we will show the problem of equivalence of \BDD to be in \Logspace, and that of \obdd{} to be
\Logspace-hard,
thus characterizing the intrinsic complexity of proof equivalence in \mallm as \Logspace-complete.

Note that this contrasts with the classical result that equivalence of general Boolean formulas is \coNP-complete.
It turns out indeed that the classes of \BDD we consider enjoy a number of properties that allow to solve 
equivalence
more easily.

%the only thing that is really left to do is to find a way to build proofs that have boolean slicing
%expressing any boolean formula, and make sure that the equivalence of these proofs correspond exactly
%the the equivalence of the boolean formula they express. This is the objective of the next section.


