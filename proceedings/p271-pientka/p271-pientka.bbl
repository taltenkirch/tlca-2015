\begin{thebibliography}{10}

\bibitem{abel:tutchprimrec}
Andreas Abel.
\newblock {\em Tutch User's Guide}.
\newblock Carnegie-Mellon University, Pittsburgh, PA, 2002.
\newblock Section 7.1: Proof terms for structural recursion.

\bibitem{Abel:TLCA11}
Andreas Abel and Brigitte Pientka.
\newblock Higher-order dynamic pattern unification for dependent types and
  records.
\newblock In {\em \emph{TLCA'11}}, volume 6690 of {\em LNCS}, pages 10--26.
  Springer, 2011.

\bibitem{Belanger:CPP13}
Olivier~Savary Belanger, Stefan Monnier, and Brigitte Pientka.
\newblock Programming type-safe transformations using higher-order abstract
  syntax.
\newblock In {\em CPP'13}, volume 8307 of {\em LNCS}, pages 243--258. Springer,
  2013.

\bibitem{Cave:POPL12}
Andrew Cave and Brigitte Pientka.
\newblock Programming with binders and indexed data-types.
\newblock In {\em POPL'12}, pages 413--424. ACM, 2012.

\bibitem{dunfieldPientka:entcs09}
Joshua Dunfield and Brigitte Pientka.
\newblock Case analysis of higher-order data.
\newblock {\em ENTCS}, 228:69--84, 2009.

\bibitem{Ferreira:PPDP14}
Francisco Ferreira and Brigitte Pientka.
\newblock Bidirectional elaboration of dependently typed languages.
\newblock In {\em PPDP'14}. ACM, 2014.

\bibitem{gacekMillerNadathur:lics08}
Andrew Gacek, Dale Miller, and Gopalan Nadathur.
\newblock Combining generic judgments with recursive definitions.
\newblock In {\em LICS'08}, pages 33--44. IEEE CS Press, 2008.

\bibitem{harperHonsellPlotkin:LF}
Robert Harper, Furio Honsell, and Gordon Plotkin.
\newblock A framework for defining logics.
\newblock {\em JACM}, 40(1):143--184, 1993.

\bibitem{Nanevski:ICML05}
Aleksandar Nanevski, Frank Pfenning, and Brigitte Pientka.
\newblock Contextual modal type theory.
\newblock {\em ACM TOCL}, 9(3):1--49, 2008.

\bibitem{pientka:jar05}
Brigitte Pientka.
\newblock Verifying termination and reduction properties about higher-order
  logic programs.
\newblock {\em JAR}, 34(2):179--207, 2005.

\bibitem{Pientka:POPL08}
Brigitte Pientka.
\newblock A type-theoretic foundation for programming with higher-order
  abstract syntax and first-class substitutions.
\newblock In {\em POPL'08}, pages 371--382. ACM, 2008.

\bibitem{Pientka:JFP13}
Brigitte Pientka.
\newblock An insider's look at {LF} type reconstruction: {E}verything you
  (n)ever wanted to know.
\newblock {\em JFP}, 1(1--37), 2013.

\bibitem{PientkaDunfield:IJCAR10}
Brigitte Pientka and Joshua Dunfield.
\newblock Beluga: A framework for programming and reasoning with deductive
  systems (system description).
\newblock In {\em IJCAR'10}, volume 6173 of {\em LNCS}, pages 15--21. Springer,
  2010.

\bibitem{Pitts:infComp03}
Andrew Pitts.
\newblock Nominal logic, a first order theory of names and binding.
\newblock {\em Inf. Comput.}, 186(2):165--193, 2003.

\bibitem{Schurmann00phd}
Carsten Sch{\"u}rmann.
\newblock {\em Automating the Meta Theory of Deductive Systems.}
\newblock PhD thesis, Department of Computer Science, Carnegie Mellon
  University, 2000.
\newblock {CMU}-{CS}-00-146.

\bibitem{Schurmann:TCS01}
Carsten Sch{\"u}rmann, Jo{\"e}lle Despeyroux, and Frank Pfenning.
\newblock Primitive recursion for higher-order abstract syntax.
\newblock {\em TCS}, 266(1-2):1--57, 2001.

\bibitem{pfenningSchuermann:coverage}
Carsten Sch\"{u}rmann and Frank Pfenning.
\newblock A coverage checking algorithm for {LF}.
\newblock In {\em TPHOLS'03}, volume 2758 of {\em LNCS}, pages 120--135, Rome,
  Italy, 2003. Springer.

\bibitem{Tiu:JAL12}
Alwen Tiu and Alberto Momigliano.
\newblock Cut elimination for a logic with induction and co-induction.
\newblock {\em J. Applied Logic}, 10(4):330--367, 2012.

\bibitem{Virga99phd}
Roberto Virga.
\newblock {\em Higher-Order Rewriting with Dependent Types}.
\newblock PhD thesis, Department of Mathematical Sciences, Carnegie Mellon
  University, 1999.
\newblock {CMU}-{CS}-99-167.

\bibitem{watkins:concurrentLFTR}
Kevin Watkins, Iliano Cervesato, Frank Pfenning, and David Walker.
\newblock A concurrent logical framework {I}: Judgements and properties.
\newblock Technical report, School of Computer Science, Carnegie Mellon
  University, Pittsburgh, 2003.

\bibitem{xi:terminationHOSC}
Hongwei Xi.
\newblock Dependent types for program termination verification.
\newblock {\em HOSC}, 15(1):91--131, 2002.

\end{thebibliography}
