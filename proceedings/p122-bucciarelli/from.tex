\section{From canonicity to observability}
\label{sec:from}


We proved in the previous section that system $\Pu$ gives a complete
characterization of terms having canonical forms.  The next theorem proves that 
system $\Pu$ is complete with respect to observability.
\begin{theorem}
\label{th:completess}
Observability implies typability.
\end{theorem}
\begin{proof}
If $\s$ is observable, then 
there is a head context $\ccontext$ such that
$\ccontext[\s]$ reduces to $\pair{\uu}{\vv}$, for some $\uu$ and $\vv$. 
Since all pairs are typable, the term $\ccontext[\s]$ is typable by Lem.~\ref{lem:red:exp}.\ref{lem:subexp}. 
Remember that $\ccontext[\s] = 
(\l \p_1 \ldots \p_n . \s) \s_1 ...\s_m$  so that $\s$ is typable too, by easy inspection of the typing system.
\end{proof} 

Unfortunately, soundness does not hold, \ie\ the set of observable
terms is strictly included in the set of  terms  having canonical form, as shown below. 
\begin{example}\label{ex:counterex}
The term $\s_1= \l \x . \id [\pair{\y}{\z}/\x][\pair{\y'}{\z'}/\x \id] $ is canonical, hence typable 
(by Thm.~\ref{l:characterization-canonical}),
but not observable. In fact, it is easy to see that there is no term $\uu$ such that both $\uu$ and
$\uu \id$ reduce to  pairs. 
A less trivial example is the term $\s_2= \l
\x. \id[\pair{\y}{\z}/\x\pair{\id}{\id}][\pair{\y'}{\z'}/\x \id\id]$, 
which is canonical, hence typable,  but not observable,
as proved in the next lemma.
%(details in Appendix~\ref{appendix-from}). 
%Notice that the conclusion of every typing
%derivation for $\s_1$ has the shape $\x:\multiset{\sig_1, \multiset{\sig_2} \to \sig_3} \der \s_1:
%\sig$, where $\sig, \sig_2$ are types for $\id$ and $\sig_1, \sig_3$ are 
%product types. This suggests that product and functional types are {\it incompatible}.
%\end{enumerate}
\end{example}

\begin{lemma}
\label{l:para-contraejemplo}
%Let $\s_2= \l
%\x. \id[\pair{\y}{\z}/\x\pair{\id}{\id}][\pair{\y'}{\z'}/\x \id\id]$.
%Then, 
There is no closed term $\uu$ s.t. both  $\uu \pair{\id}{\id}$
and $\uu \id\id$ reduce to pairs. 
\end{lemma}
\begin{proof} By contradiction.
Indeed, assume that there exist a closed term $\uu$
such that both $\uu\pair{\id}{\id}$ and $\uu\id\id$ reduce to
pairs. Since pairs are always typable, then $\uu\pair{\id}{\id}$ and
$\uu\id\id$ are typable by Lem.~\ref{lem:red:exp}.\ref{lem:subexp}.  In any of the
typing derivations of such terms, $\uu$ occurs in a typed position, so
that $\uu$ turns out to be also typable.\\
Now, since $\uu$ is typable and closed, then it reduces to a (typable
and closed) \canonical\ $\vv \in {\cal J}$ by
Thm.~\ref{l:characterization-canonical}. But  $\vv$ cannot be in ${\cal K}$, which only contains open terms.
Moreover, $\vv$ cannot be a pair, otherwise
$\uu\pair{\id}{\id} \Rewn{} \vv \pair{\id}{\id} \Rewn{} \pair{\vv_1}{\vv_2}\pair{\id}{\id} \Rewn{} \fail$ 
which contradicts (by Lem.~\ref{lem:inf:uno}) the fact that
$\uu\pair{\id}{\id}$ reduces to a pair. We then have two possible forms
for $\vv$.\\
If $\vv = \sss[\pair{\p_1}{\p_2}/\kk]$, where $\sss \in {\cal J}$
and $\kk \in {\cal K}$. Then $\kk$ is an open term which implies $\vv$ is
an open term. Contradiction. \\
If $\vv = \l\p. \sss$, where $\sss \in {\cal J}$, then  $\p$ is necessarily a
variable, say $\z$, since otherwise $\vv \id$ reduces to $\fail$, and hence
$\uu\id\id \Rewn{} \vv\id\id \Rewn{} \fail$, which contradicts (by Lem.~\ref{lem:inf:uno}) the fact that
$\uu\id \id$ reduces to a pair. 
We analyze the possible forms of $\sss$.\\
\begin{itemize}
\item If $\sss$ is a pair, then $\uu \id \id \Rewn{} (\l\z. \sss) \id \id \Rewn{} \fail$, 
which contradicts (by Lem.~\ref{lem:inf:uno}) the fact that
$\uu\id \id$ reduces to a pair.
\item If $\sss$ is an abstraction, then $\uu \pair{\id}{\id} \Rewn{} (\l\z. \sss)\pair{\id}{\id}$
which reduces to an abstraction, contradicting  (by Lem.~\ref{lem:inf:uno}) the fact that
$\uu\pair{\id}{\id} $ reduces to a pair.
\item If $\sss$ is in ${\cal K}$, then $\sss = \x \s_1 \ldots \s_n$ with $n \geq 0$. Remark
that  $\z \neq \x$ is not possible since $\vv=\l \z. \sss$ is closed. Then
$\z = \x$. If $\sss = \z$, then $\uu \id \id$ reduces to $\id$ which contradicts
 (by Lem.~\ref{lem:inf:uno}) the fact that
$\uu\id \id$ reduces to a pair. Otherwise, $\sss = \z \s_1 \ldots \s_n$ with $n \geq 1$, and thus 
$\uu \pair{\id}{\id} $ reduces to $\pair{\id}{\id} \s_1 \ldots \s_n \Rewn{} \fail$, 
which contradicts again 
 (by Lem.~\ref{lem:inf:uno}) the fact that
$\uu\pair{\id}{\id}$ reduces to a pair.
\item If $\sss$ is $\sss'[\pair{\p_1}{\p_2}/\kk]$, with $\kk \in {\cal K}$, then
$\kk = \z \s_1 \ldots \s_n$ with $n \geq 0$, since any other head variable for $\kk$ would contradict
$\vv$ closed. Now, in the first case we have
$\uu \id \id$ reduces to $\fail$ which contradicts
 (by Lem.~\ref{lem:inf:uno}) the fact that
$\uu\id \id$ reduces to a pair. 
Otherwise, $\kk = \z \s_1 \ldots \s_n$ with $n \geq 1$ implies
$\uu \pair{\id}{\id}$ reduces to $\fail$ which contradicts
 (by Lem.~\ref{lem:inf:uno}) the fact that
$\uu\pair{\id}{\id}$ reduces to a pair. 
\end{itemize} \end{proof}


\ignore{Based on the (quite natural) observation that product and arrow types
cannot live together since they represent different data types, the
previous example suggests that the typing system can be restricted by
introducing a compatibility predicate between types (details can be found in~\cite{tech-rep}) 
This natural observation is nevertheless not sufficient to solve the
problem, \ie\ there exist  unsolvable  terms that are still typable in the restricted system
as for example the term $\s_2= \l
\x. \id[\pair{\y}{\z}/\x\pair{\id}{\id}][\pair{\y'}{\z'}/\x \id\id]$. 
Indeed, 
\begin{lemma}
\label{l:para-contraejemplo}
Let $\s_2= \l
\x. \id[\pair{\y}{\z}/\x\pair{\id}{\id}][\pair{\y'}{\z'}/\x \id\id]$.
Then, there is no closed term $\uu$ such that both  $\uu \pair{\id}{\id}$
and $\uu \id\id$ reduce to pairs. 
\end{lemma}

\begin{proof} By contradiction. Details can be found in
Appendix~\ref{appendix-from}.
\end{proof}

Example~\ref{l:para-contraejemplo} shows that a logical compatibility relation is
not sufficient to obtain soundness of typability with respect to
observability.}


The first non-observable term $\s_1$ in  Ex.~\ref{ex:counterex} could be ruled out by 
introducing a notion of {\it compatibility} between types and
requiring multiset
types to be composed only by compatible strict types.
% The first counterexample could be ruled out by requiring multiset
% types to be composed only by compatible strict types, where product
% and functional types are incompatible. 
%Nevertheless, this would not
%work for the second counterexample. 
Unfortunately, we claim that a compatibility relation defined {\it syntactically},
let us call it {\tt comp}, 
cannot lead to a sound and complete characterization of observability. 
By ``defined syntactically'' we mean that the value of 
${\tt comp}(\sigma\arrow\sig',\rho\arrow\rho')$ 
should only depend on the values of 
${\tt comp}(\sigma,\rho)$ and  ${\tt comp}(\sigma',\rho')$.
Another basic requirement of {\tt comp} would be  that every product type is incompatible with any functional type.
The second non-observable term $\s_2$ in Ex.~\ref{ex:counterex} is appropriate to illustrate our claim,
by keeping in mind that any pair of types assignable to $\x$ in any typing derivation for $\s_2$ need to
be incompatible. 

Indeed, the shortest typing for  $\s_2$ above is obtained by assigning
to $\x$  the two types $\emul  \arrow \oprod$ and  $\emul \arrow \emul
\arrow  \oprod$, and  in  order to  state the  incompatibility
  between   them  it  would   be  necessary   to  define   that  ${\tt
    comp}(\sigma,\rho)$  and   $\neg{\tt  comp}(\sigma',\rho')$  imply
  $\neg{\tt  comp}([\sigma]\arrow\sig',[\rho]\arrow\rho')$.   Another
typing  for $\s_2$  is obtained  by assigning  to $\x$  the  two types
$[\oprod]\arrow\oprod$  and  $[\tau]\arrow[\tau]\arrow\oprod$
respectively,  where  $\tau=[\oprod]\arrow\oprod$,  so  that
$\neg{\tt  comp}(\sigma,\rho)$   and  $\neg{\tt  comp}(\sigma',\rho')$
should  imply $\neg{\tt comp}([\sigma]\arrow\sig',[\rho]\arrow\rho')$.
We  conclude that $\neg{\tt  comp}(\sigma',\rho')$ alone  should imply
$\neg{\tt   comp}([\sigma]\arrow\sig',[\rho]\arrow\rho')$.    However,
arrow  types   $[\sigma]\arrow\sig'$  and  $[\rho]\arrow\rho'$  having
incompatible  right-hand  sides  may  very  well  be  compatible.  For
instance,          letting          $\sigma=\sig'=\oprod$          and
$\rho=\rho'=[\oprod]\arrow\oprod$, one gets  two types for $\id$ which
need  of   course  to  be   compatible.   Hence,  a   {\it  syntactic}
characterization of such a notion of compatibility seems out of reach.

Fortunately,  there exists a sound and complete {\it semantical}
notion of compatibility between types, obtained {\it a posteriori} as follows:
given two \strict\  types $\pi_1$ and $\pi_2$, build  
the corresponding  sets 
of  inhabitants $\K(\emptyset, \pi_1)$ and $\K(\emptyset, \pi_2)$, using  the 
inhabitation algorithm presented in  Sec.~\ref{s:inhabitation}.
Then  $\pi_1$ and $\pi_2$ are {\it semantically} compatible if and only 
if $\K(\emptyset, \pi_1)\cap\K(\emptyset, \pi_2)$ is non-empty.



% \textcolor{magenta}{Unfortunately, a compatibility relation defined syntactically
% cannot lead to a sound and complete characterization of observability. 
% The shortest typing for the term $\s_2$ above is obtained by assigning to $\x$ the two types $\pi_1=\emul \arrow \oprod$ and 
% $\pi_2=\emul \arrow \emul \arrow \oprod$, and it can be easily excluded by a standard definition of compatibility since both are functional types with compatible left part and incompatible right part. But let us consider a more general typing.
% Let $\sigma_i$ be product types derivable for $\pair{\id}{\id}$ and $\tau_j$ be functional types derivable for $\id$
% ($ 1\leq i\leq 3, 1\leq j\leq 2$): each $\sigma_i$ is clearly incompatible with each $\tau_j$. 
% A type derivation for $\s_2$ above  would (tipically)
% assign to the two occurrences of the variable $\x$ types of the shape
% $\rho_1=[\sigma_1]\arrow\sigma_2$ and $\rho_2=[\tau_1]\rightarrow
% [\tau_2]\rightarrow\sigma_3$, respectively. So $\rho_1$ and $\rho_2$ need to be incompatible, but they are functional types with incompatible left part and incompatible right part, so define them incompatible, together with the incompatibility of $\pi_1$ and $\pi_2$ should imply the incompatibility of 
% every pair of functional types with incompatible right parts. But this definition of incompatibility is too restrictive, in fact it would forbid to give type to the term $\l
% \x. \id[\pair{\y}{\z}/\x\id(\l \w.\pair{\id}{\id})\id][\pair{\y'}{\z'}/\x \id\pair{\id}{\id}]$, which is clearly observable (just the replacement of $\x$ by $\id$ guarantees the success of the two matchings).
% Hence, a {\it syntactic} characterization of such a notion of compatibility 
% seems out of reach.
% On the other hand,  there exists a sound and complete {\it semantical}
% notion of compatibility between types, obtained {\it a posteriori} as follows:
% given two simple types $\pi_1$ and $\pi_2$, build  
% the corresponding  sets 
% of  inhabitants $\K(\emptyset, \pi_1)$ and $\K(\emptyset, \pi_2)$, using  the 
% inhabitation algorithm presented in  Sec.~\ref{s:inhabitation}.
% Then  $\pi_1$ and $\pi_2$ are compatible if and only 
% if $\K(\emptyset, \pi_1)\cap\K(\emptyset, \pi_2)$ is non-empty.
% }


% What follows is an informal argument supporting our feeling that  a 
% {\it ``compatibility''} relation inductively defined on the set of types,
% and considering only their syntactical form, 
% cannot lead to a sound and complete characterization of observability:
% a typing derivation for  the term $\s_2$ above  would (tipically)
% assign to the two occurrencies of the variable $\x$ the types 
% $\alpha_1=[\oprod]\rightarrow\oprod$ and $\alpha_2=[[\oprod]\rightarrow\oprod]\rightarrow
% [[\oprod]\rightarrow\oprod]\rightarrow\oprod$, respectively. Hence, these two 
% types should be incompatible, since otherwise the whole term would get the type
% $[\alpha_1,\alpha_2]\rightarrow[\oprod]\rightarrow \oprod$ (remark that
% $[\oprod]\rightarrow \oprod$ is a type for $\id$ in the
% empty context).
% Now, $\alpha_1$ and $\alpha_2$ are both arrow types, and in both of them 
% only unary multisets do appear. Moreover, the  left-hand sides
% of the arrows  are the multisets composed by $\oprod$ 
% and $[\oprod]\rightarrow \oprod$ respectively, which are clearly incompatible.
% This should imply the compatibility between $\alpha_1$ and $\alpha_2$, 
% rather then their incompatibility. 
% Hence, a {\it syntactic} characterization of such a notion of compatibility 
% seems out of reach.

% On the other hand,  there exists a sound and complete {\it semantical}
% notion of compatibility between types, obtained {\it a posteriori} as follows:
% given two simple types $\alpha_1$ and $\alpha_2$, build  
% the corresponding  sets 
% of  inhabitants $A_1$ and $A_2$, using  the 
% inhabitation algorithm presented in  Sec.~\ref{s:inhabitation}.
% Then  $\alpha_1$ and $\alpha_2$ are compatible if and only 
% if $A_1\cap\ A_2$ is non-empty.


% However, system $\Pu$ can actually be used to obtain
% such a characterization by combining the notions of typability {\it
%   and} inhabitation.

\ignore{
In order to explain the
main ideas behind this characterization, let us first recall 
the characterizations of solvable terms in the $\l$-calculus.
\begin{description}
\item[(1)] $\ccontext[\s]$ reduces to $\id$ for an appropriate head-context $\ccontext$;  
\item[(2)] $\s$ has a head-normal form; 
\item[(3)]  $\s$ has type different from $\oprod$, in a
suitable intersection type system including a constant $\oprod$
typing all terms (e.g. the systems in~\cite{tipoA-BCD:JSL,krivine93book}).  
\end{description}
Statement {\bf (1)} is the definition of solvability,
statement {\bf (2)} (resp. {\bf (3)}) is known as the
{\it syntactical} (resp. {\it logical} characterization). 
We now recall how the proof of the logical characterization  is generally performed,
see for example~\cite{Dezani-Ghilezan:TYPES-2002}.
The implication {\bf (3)} $\Rightarrow$ {\bf (1)}
is proved by semantical arguments.
The syntactical characterization is well known (see \cite{barendregt84nh}).
The implication {\bf (2)} $\Rightarrow$ {\bf (3)}
consists in proving that every term $\s$ which reduces to a head normal form
is typable. So first it is proved that every head normal form can be typed, and the 
result follows by the subject expansion property.
The proof that every head normal form can be typed is performed in a constructive way, as follows. 
A closed head normal form $\vv$ has the general shape $\lambda
\x_1...\x_n.\x_i \s_1...\s_m$, where $n, m \geq 0, 1\in\{1,...,n\}$.
It is then possible to assign to $\vv$ a type of the shape
$\underbrace{\oprod \to...\to \oprod}_{i-1} \to
(\underbrace{\oprod\arrow...\arrow \oprod}_m\arrow \nu\to \nu)\to
\underbrace{\oprod \to...\to \oprod}_{n-i} \to \nu\to\nu$, for any
$\nu$ in the typing system. 
This type can be seen as a description of a context $\ccontext$,
such that $\ccontext[\vv]$ reduces to $\id$, so $\vv$
is solvable. 

To construct such a 
context 
observe that  $\nu\to\nu$ is inhabited by the term $\id$,
$\omega$ is trivially inhabited by any term
and $\underbrace{\omega\arrow...\arrow \omega}_m\arrow \nu\to\nu$ is
inhabited by the term $O^m=\l \x_1...\x_m\x.\x$.
Now, it is easy to  see that  the head context
$\ccontext=\Box \uu_1...\uu_{i-1}O^m \uu_{i+1}...\uu_m$, where
$\uu_1,...,\uu_{i-1},\uu_{i+1},...,\uu_m$ are arbitrary terms,  verifies
$\ccontext[\vv] \Rewn{\beta} \id$. \\ 
%Thus  $\s$ is solvable.  

We thus get the following  alternative logical characterization of  solvability in the 
$\l$-calculus, 
which is completely independent
from the syntactical one.

\begin{lemma}
A closed
  $\l$-term $\s$ is solvable if and only if it can be assigned an
  intersection type of the shape $\mu_1\to...\to\mu_n \to \alpha \to
  \alpha$, where $\alpha$ is a type constant, and every $\mu_i\ (1\leq i \leq n)$ is
  inhabited by a closed $\l$-term.
\end{lemma} }

%To the best of knowledge, this relation between solvability and
%inhabitation has never been explored before, may be since the
While the inhabitation problem for (idempotent) intersection types is
undecidable~\cite{Urzyczyn99}, it becomes decidable
for non-idempotent intersection types~\cite{bkdlr14}, which is just a
subsystem of our typing system $\Pu$ introduced in
Sec.~\ref{l:type-system}. We will prove in the following that
inhabitation is also decidable for the non-trivial extension  $\Pu$.  We
will then use this result for characterizing observability in the pattern calculus without
referring to a complete syntactic characterization, which is not
possible in this framework, as illustrated by Example~\ref{ex:counterex}.

%in which $\x_i$ has type
%$\sig= \underbrace{\omega\arrow...\arrow \omega}_m\arrow \tau$, for
%some $\tau \not=\omega$,
%and all other $\x_j\ (j \neq i)$ have type $\omega$ so that $\vv$ can be
%assigned type $\tau$.  Moreover,
%$O^m=\l \x_1...\x_m\x.\x$ can be assigned the type $\sig$ (it is an
%inhabitant of $\sig$) and any $\uu_i$, for $i\in \{1, \ldots ,
%i-1,i+1, \ldots ,n\}$ can be assigned the type $\omega$ (they are
%inhabitants of $\omegha$); thus there are terms $\uu_1 \ldots \uu_{i-1}
%O^m \uu_{i+1} \ldots \uu_n$, which are inhabitants of their
%corresponding types, such that $\s\uu_1...\uu_{i-1}
%O^m \uu_{i+1}...\uu_{n}$ $\beta$-reduces to the identity, proving that
%$t$ is solvable.
%
