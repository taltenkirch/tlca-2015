\section{Characterizing Observability}
\label{s:charact}

We are now able to state the main result of this paper, \ie\ the
characterization of observability for the pattern
calculus. The following lemma assures that types reflect correctly the structure of the data types.

\begin{lemma}
\label{l:closed-pair}
Let $\s$ be a closed and typable term, then
\begin{itemize}
\item If $\s$ has functional type, then $\s$ reduces to an abstraction.
\item If $\s$ has  product type, then $\s$  reduces to a pair.
\end{itemize}
\end{lemma}

\begin{proof}
Let $\s$ be a closed and typable term. By Thm.~\ref{l:characterization-canonical} we know that 
$\s$ reduces to a (closed) canonical form in ${\cal J}$. The proof is  by induction on the
maximal length of such reduction sequences.

If $\s$ is already a canonical form, we analyze all the cases.
\begin{itemize}
\item  If $\s$ is a variable, then this gives a contradiction with $\s$ closed. 
\item  If $\s$ is a function, then the property trivially holds.
\item If $\s$ is a pair, then the property trivially holds.
\item If $\s$ is an application, then $\s$ has the form 
$\x \s_1 \ldots \s_n$. Therefore 
at least $\x$ belongs to the set of free variables of $\s$, which leads to a
contradiction with $\s$ closed.
\item If $\s$ is a closure, \ie\ $\s=\uu[ \pair{\p_1}{\p_2}/\vv]$,
where $\vv \in {\cal K}$
has the form $\x \s_1 \ldots \s_n$, then at least $\x$ belongs to the set of
free variables of $\s$, which leads to a contradiction with $\s$ closed.
\end{itemize}

Otherwise, $\s \Rew{} \s' \Rewn{} \uu$, where $\uu$ is in ${\cal J}$. 
The term $\s'$ is also closed and typable
(Lem.~\ref{lem:red:exp}.\ref{lem:reduction}), then the \ih\ gives the desired result for
$\s'$, so the property holds also for $\s$.
\end{proof}


\begin{theorem}[Characterizing  Observability]
\label{t:main-result}
A term $\s$ is observable  iff
$\Pi \dem \x_1: \A_1; \ldots; \x_n: \A_n \der  \s: \D_1 \arrow \ldots \arrow 
\D_m \arrow \alpha$, where $n \geq 0, m \geq 0$, 
$\alpha$ is a product type
and all  $\A_1, \ldots \A_n, \D_1,  \ldots \D_m$ are
inhabited. 
\end{theorem}

\begin{proof}
The left-to-right implication: if $\s$ is observable, then there exists 
a head-context  $\ccontext$ such that $\ccontext[\s] \Rewn{} \pair \uu \vv$. Since $\vdash \pair \uu \vv : \oprod$, we get $\Pi'\dem \vdash\ccontext[\s]: \oprod$ by Lem.~\ref{lem:red:exp}.\ref{lem:subexp}. 
By definition $\ccontext[\s]=( \l\p_1...\l\p_n.\s)\uu_1...\uu_m$, so $\Pi$ has a subderivation $\Pi' \dem \der \l\p_1...\l\p_n.\s: \D_1 \arrow \ldots \arrow 
\D_m \arrow \oprod$ (by rule $(\trarrowe)$), where $\D_i$ is inhabited by $\uu_i$ ($1\leq i\leq m$). Since $n\leq m$, $\Pi'$ has a subderivation $\Pi'' \dem \Gam \der \s: \D_{n+1} \arrow \ldots \arrow 
\D_m \arrow \oprod$ (by rule $(\trarrowi)$), where $\Gam|_{\p_i}\pder \p_i: \D_i$ ($1\leq i \leq n$). The result follows since 
%a simple inspection of the typing rules allows to conclude  that if  
$\x_1:A_1,\ldots,.\x_l:A_l \pder \p:B$  and $B$ is  inhabited
implies that all the $A_i$ are inhabited.
%so 
%$\x_1:\D_1,..., \x_n:\D_n \der \s:\D_{n+1} \arrow \ldots \arrow 
%\D_m \arrow \oprod$.\\ 
% is head context, by inspecting the rules of the system it is easy to see that $\s$ occurs in a typed occurrence of $\Pi'$, so it exists a subederivation of $\Pi'$ proving
% Starting from $\Pi'$ 
%it is not difficult to construct a derivation for $\s$ having the form
%$\Pi \dem \x_1: \A_1; \ldots; \x_n: \A_n \der  \s: \D_1 \arrow \ldots \arrow 
%\D_m \arrow \oprod$, where $\A_1, \ldots \A_n, \D_1,  \ldots \D_m$ are
%all inhabited. }
The right-to-left implication: 
if $\A_1, \ldots \A_n, \D_1,  \ldots \D_m$ are all inhabited,
then  there exist $\uu_1, \ldots \uu_n, \vv_1, \ldots \vv_m$ such that 
$\der \uu_i : \sig_i^j$ for every type $\sig_i^j$ of $\A_i\ (1 \leq i \leq n)$
and $\der \vv_i:  \rho_i^j$ for every type $\rho_i^j$ of $\D_i\ (1 \leq i \leq m)$.
Let $\ccontext = (\l \x_1 \ldots \x_n. \Box) \uu_1 \ldots \uu_n \vv_1 \ldots \vv_n$
be a head-context. We have
$\der \ccontext[\s] : \alpha$, which in turn implies
that $\ccontext[\s]$ reduces to a pair, by  Lem.~\ref{l:closed-pair}. 
Then the term $\s$ is observable  by definition.
\end{proof}

The notion of observability is conservative with respect to that of solvability in $\l$-calculus.

\begin{theorem}[Conservativity]\label{thm:con}
A $\l$-term $\s$ is solvable in the  $\l$-calculus if and only if $\s$ 
is observable  in $\Lp$.
\end{theorem}
\begin{proof}
\begin{itemize}
\item{(if)} Take an unsolvable $\l$-term $\s$ so that $\s$
does not have head normal-form. Then  $\s$ (seen as a term of our calculus) has no
\canonical, and thus $\s$ is not typable by Thm.~\ref{l:characterization-canonical}. 
It turns out that $\s$ is not observable in $\Lp$
by Thm.~\ref{t:main-result}.
\item{(only if)}  Take  a   solvable  $\l$-term  $\s$  so  that   there  exist  a
  head-context $\ccontext$ such that $\ccontext[\s]$ reduces to $\id$, 
  then it is  easy to construct a head  context $\ccontext'$ such that
  $\ccontext'[\s]$   reduces  to  a   pair  (just   take  $\ccontext'=
  \ccontext\ \pair{\s_1}{\s_2}$ for some terms $\s_1, \s_2$).
\end{itemize}
\end{proof}

