\section{Sequent calculus}\label{sec:ultimate}
%\section{The improved $\ol$-calculus}\label{sec:ultimate}
%\section{The ultimate $\ol$-calculus}\label{sec:ultimate}

%-----------------------------------------
\subsection{The $\uol$-calculus}
The proof-expressions of $\uol$ \footnote{We would have adopted the name $\ol\mut$ (to suggest $\ol+\mut$), had it not been already in use \cite{CurienHerbelin2000}.} are given by the following grammar:
$$
\begin{array}{rrcl}
\textrm{(Terms)} & t,u,v & ::= & \lambda x.t\,|\,\der xk\,|\,tk\\
\textrm{(Generalized vectors)} & k & ::= & \nil\,|\,\mut x.v\,|\,u::k
\end{array}
$$
The typing rules are in Fig.~\ref{fig:typing-ultimate}. They handle two kinds of sequents: $\Gamma\vdash t:A$ and $\Gamma|A\vdash k:B$. The distinguished formula $A$ in the latter is not exactly a ``stoup'' or a focused formula, because the operator $\mut x.t$ may select an arbitrary formula from the context $\Gamma$. The construction $\der xk$ comes from the $\ol$-calculus, but here it forms a pair with $\mut x.t$: logically, these are a activation/passification pair, in the style of the $\lmu$-calculus, but acting on the l.h.s. of sequents.
%----------------------------------------
\begin{figure}[t]\caption{Typing rules of the $\uol$-calculus}\label{fig:typing-ultimate}
%$$
%\begin{array}{c}
%Ax\ \frac{}{\displaystyle \Gamma|A\vdash \nil:A}\qquad Cut\ \frac{\displaystyle\Gamma\vdash t:A\quad\Gamma|A\vdash k:B}{\displaystyle\Gamma\vdash tk:B}\\
%\\
%L\supset\ \frac{\displaystyle\Gamma\vdash u:A\quad\Gamma|B\vdash k:C}{\displaystyle\Gamma|A\supset B\vdash u::k:C}\qquad
%R\supset\ \frac{\displaystyle\Gamma,x:A\vdash t:B}{\displaystyle\Gamma\vdash\lambda
%x.t:A\supset B}\\ \\
%Pass\ \frac{\displaystyle\Gamma,x:A|A\vdash k:B}{\displaystyle\Gamma,x:A\vdash \der xk:B}\qquad Act\ \frac{\displaystyle\Gamma,x:B\vdash %v:B}{\displaystyle\Gamma|A\vdash \mut x.v:B}
%\end{array}
%$$
$$
\begin{array}{c}
Ax\ \frac{}{\displaystyle \Gamma|A\vdash \nil:A}\qquad Cut\ \frac{\displaystyle\Gamma\vdash t:A\quad\Gamma|A\vdash k:B}{\displaystyle\Gamma\vdash tk:B}\qquad Pass\ \frac{\displaystyle\Gamma,x:A|A\vdash k:B}{\displaystyle\Gamma,x:A\vdash \der xk:B}\\
\\
L\supset\ \frac{\displaystyle\Gamma\vdash u:A\quad\Gamma|B\vdash k:C}{\displaystyle\Gamma|A\supset B\vdash u::k:C}\qquad
R\supset\ \frac{\displaystyle\Gamma,x:A\vdash t:B}{\displaystyle\Gamma\vdash\lambda
x.t:A\supset B}\qquad Act\ \frac{\displaystyle\Gamma,x:B\vdash v:B}{\displaystyle\Gamma|A\vdash \mut x.v:B}
\end{array}
$$

\end{figure}
%-----------------------------------------------

The notation $\mut x.t$ comes from $\lmmt$ \cite{CurienHerbelin2000}; but here, contrary to what happens in $\lmmt$, the reduction rule that defines the behavior of $\mut$ does not trigger a term-substitution, but rather a context-substitution, in the style of the above presentation of the $\mu$-operator. The construction $\der xk$ is easily recognized as the accompanying fill-instruction, and what remains is to pin down the right notion of context that $\mut$ will capture. The notion is this:
$$
\HH\,::=\,\der x{\hole}\,|\,t(\hole)\,|\,\HH\fh{u::\hole}
$$
These expressions are called \emph{co-continuations}. Later we will argue they are logically dual to continuations.

The reduction rules of $\uol$ are in Fig.~\ref{fig:reduction-rules-ultimate}. Let $\pi:=\pi_1\cup\pi_2$. The co-control rule $\mut$ triggers the context substitution $\sub{\HH}x{\_}$, in whose definition the only non-routine case is:
$$
\sub{\HH}x{(\der xk)}=\HH\fh{k'}\textrm{  with $k'=\sub{\HH}xk$}\enspace.
$$
This equation gives the meaning of $\der xk$: fill $k$ in the hole of the $\HH$ that will substitute $x$. The $\pi_i$-rules employ concatenation of generalized vectors $k@k'$, defined by the obvious equations $\nil@k'=k'$ and $(u::k)@k'=u::(k@k')$, together with $(\mut x.t)@k'=\mut x.tk'$.

%$$
%\nil@k'=k'\qquad(\mut x.t)@k'=\mut x.tk'\qquad(u::k)@k'=u::(k@k')\enspace.
%$$
%-----------------------------------------------
\begin{figure}[t]\caption{Reduction rules of $\uol$}\label{fig:reduction-rules-ultimate}
%$$
%\begin{array}{rcrcl}
%(\beta)&& (\lb x.t)(u::k) & \to & u(\mut x.tk)\\
%(\beta)&& (\lb x.t)(u::k) & \to & (u(\mut x.t))k\\
%(\mut)&& \HH\fh{\mut x.t} & \to & \sub{\HH}xt\\
%(\epsilon)&& t\nil & \to & t\\
%(\pi_1)&& (\der xk)k' & \to & \der x{(k@k')}\\
%(\pi_2)&& (tk)k' & \to & t(k@k')\\
%\end{array}
%$$
$$
\begin{array}{rcrclcrcrcl}
%(\beta)&& (\lb x.t)(u::k) & \to & u(\mut x.tk)\\
(\beta)&& (\lb x.t)(u::k) & \to & (u(\mut x.t))k&\qquad&(\epsilon)&& t\nil & \to & t\\
(\mut)&& \HH\fh{\mut x.t} & \to & \sub{\HH}xt&\qquad&(\pi_1)&& (\der xk)k' & \to & \der x{(k@k')}\\
&&&&&\qquad&(\pi_2)&& (tk)k' & \to & t(k@k')\\
\end{array}
$$
\end{figure}
%-------------------------------------------------
Rule $\mut$ eliminates all occurrences of the $\mut$-operator. The remaining rules eliminate all occurrences of cuts $tk$. So the $\beta\mut\epsilon\pi$-normal forms correspond to a well-known representation of $\beta$-normal $\lb$-terms. There is a critical pair generated by rules $\mut$ and $\pi$. This is the \emph{call-by-name vs call-by-value} dilemma \cite{CurienHerbelin2000}.

%Discussion of the reduction rule $\mut$: justification of its design through natural deduction;
%why elimination of every occurrence of $\mut$? why not just cut-elimination? why not cut=redex?
%why not just $\sigma$? notice $\beta\mut\pi$-nfs are not the the ordinary cut-free forms.

Two particular cases of the reduction rule $\mut$ are:
$$
(\rho)\qquad\der y{(\mut x.t)} \to  \sub{\der y{\hole}}xt\qquad\qquad
(\sigma)\quad u(\mut x.t) \to  \sub{u(\hole)}xt
$$
%$$
%\begin{array}{rcrcl}
%(\rho)&& \der y{(\mut x.t)} & \to & \sub{\der y{\hole}}xt\\
%(\sigma)&& u(\mut x.t) & \to & \sub{u(\hole)}xt
%\end{array}
%$$
We let $\tau:=\mut\backslash(\rho\cup\sigma)$. The particular case $\rho$ may be called the \emph{renaming} rule (as sometime does the ``dual'' rule in the $\lmu$-calculus). Indeed, the particular case $\sub{\der y{\hole}}xt$ of context substitution is almost indistinguishable from a substitution operation that renames variables, since the critical case of its definition reads\footnote{We say ``almost'' because, don't forget, variables are not expressions \emph{per se}.}
$$
\sub{\der y{\hole}}x{(\der xk)}=\der y{k'}\textrm{  with $k'=\sub{\der y{\hole}}xk$}\enspace.
$$
%\noindent On the other hand, the critical case in the definition of the other particular case of context substitution used in $\sigma$ reads:
%$$
%\begin{equation}\label{eq:term-subst}
%\sub {u(\hole)}x{(\der xk)}=uk'\textrm{ with $k'=\sub {u(\hole)}xk$}\enspace.
%\end{equation}
%$$
%\noindent We thus rediscover, in the setting of $\uol$, the special substitution defined by (\ref{eq:special-subst}) we had introduced in the setting $\ol$.
If we wanted a set of small step $\mut$-rules, in the style of the original $\lmu$-calculus, we would have taken $\rho$ and $\sigma$, together with $u::(\mut x.t) \to \mut x.\sub{\der x{(u::\hole)}}xt$.
%$$
%\begin{array}{rcrcl}
%&& u::(\mut x.t) & \to & \mut x.\sub{\der x{(u::\hole)}}xt\enspace.
%\end{array}
%$$
The particular case $\sub{\der x{(u::\hole)}}xt$ of context substitution gives a form of ``structural substitution'' dual to that found in $\lmu$.
%found in the original formulation of $\lmu$.


