%\section{The proof theory of computational $\lb$-calculus}\label{sec:computational}
\subsection{Computational interpretation}\label{subsec:computational-interpret}
%\textbf{Computational interpretation.}

We argue that $\unat$ is a \emph{bidirectional, agnostic, computational $\lb$-calculus}. The word ``bi-directional'' comes from \cite{PierceTurnerPOPL98}, where the organization of a typing system for $\lb$-terms with two kinds of sequents (for synthesis, like $\Gamma\vdash M:A$, and for checking, like $\Gamma\rhd H:A$) is already found. The word ``agnostic'' comes from \cite{ZeilbergerAPAL08} and means coexistence or superimposition of call-by-name (CBN) and call-by-value (CBV).

Let us go back to Fig.~\ref{fig:reduction-rules-nat-ded}. Rule $\beta$ generates a let-expression, which can be executed by the separate rule $\LET$. Let-expressions enjoy ``associativity'': the particular case of $\HD_2$ where $\K=\lt{\hole}yQ$ reads $\lt{\hd{\lt HxP}}yQ \to \lt Hx{\lt {\hd P}yQ}$.
%$$
%(\assoc) \quad \lt{\hd{\lt HxP}}yQ \to \lt Hx{\lt {\hd P}yQ}\enspace.
%$$
%\noindent
In addition, there is a pair of reduction rules to cancel a sequence of two coercions. So we might view $\unat$ as a sort of computational $\lb$-calculus \cite{MoggiLFCS88,SabryWadler97} where rule $\LET$ does not require the computation of a value prior to substitution triggering, and where a pair of trivial reduction rules ($\TRIV$ and $\HD_1$) eliminates odd accumulations of coercions caused by a clumsy syntax.

However, this is not the right view. %Rule $\HD_1$ is an important rule - the only rule that increases the length of heads; and
$\HD_1$ works together with $\HD_2$ to reduce every non-abstraction term to one of the forms $\K\fh{x}$ or $\K\fh{\hd{\lb x.M}}$. This is a hint of what we see next: all reduction rules of $\unat$ have quite clear roles in CBV and CBN computation, and through these roles we will understand how different rules $\TRIV$ and $\HD_1$ are.

Let us make a technical point. Rule $\HD_1$ is a relation on heads. As with all other reduction rules, $\HD_1$ generates by compatible closure a relation $\to_{\HD_1}$ on heads and another on terms. The relation $\to_{\HD_1}$ on terms would have been the same, had we taken the rule $\HD_1$ as the relation on terms $\K\fh{\hd{\app{H}}}\to \K\fh H$. A similar remark applies to $\BETA$. In the discussion of CBN and CBV that follows, we take $\HD_1$ and $\BETA$ in their alternative formulation, so that it makes sense to speak about $\HD_1$- or $\BETA$-reduction at root position of a term.


CBN and CBV are defined through priorities among reduction rules \cite{CurienHerbelin2000}:
%--------------------
%\begin{definition} In $\unat$:
\begin{itemize}
\item CBV strategy: reduction at root position of a closed, non-abstraction term with priority given to $\HD$.
\item CBN strategy: reduction at root position of a closed, let-free, non-abstraction term with priority given to $\TRIV$.
\end{itemize}
%\end{definition}
%-------------------

We will give an alternative characterization of these strategies. For CBN we need the rule $\K\fh{\hd{\lb x.M}N}\to\K\fh{\hd{\sub{\hd N}xM}}$, which we call $\mathit{CBN-\BETA}$, and is obtained by $\BETA$ followed by $\LET$.

%Let us analyze the strategies further. For CBV, after root position $\HD$-reduction, a closed non-abstraction has one of the three forms: $\K\fh{\hd{\lb x.M}N}$, $\lt{\hd{\lb x.M}}yP$, or $\app{\hd{\lb x.M}}$. In the first two cases, computation continues with application of $\BETA$ or $\LET$, respectively; in the third case, computation stops after application of $\TRIV$, since an abstraction is obtained. For CBN, since the term is let-free, a $\TRIV$-redex has two possible forms: $\app{\hd{\lb x.M}}$ or $\app{\hd{\app{H}}}$. Notice that the reduction of the latter term can count as a $\HD_1$-reduction. So, after root position $\HD_1$-reduction, a closed, let-free non-abstraction has one of the two forms: $\K\fh{\hd{\lb x.M}N}$ or $\app{\hd{\lb x.M}}$. In the first case, computation continues with application of $\BETA$, followed by the immediate $\LET$-reduction of the let-expression generated (we call $CBN-\BETA$ the rule obtained by this chain of two reduction steps); in the second case, computation stops after application of $\TRIV$, since an abstraction is obtained. Summarizing:
%--------------------------------
\begin{theorem}[Agnosticism]\label{thm:agnosticism} The following is an equivalent description of the CBN and CBV strategies. In this description, ``reduction'' means root-position reduction of a closed, non-abstraction term. We assume additionally that the initial term is let-free.
\begin{itemize}
\item CBV. Do $\HD$-reduction as long as possible, until the term becomes either a $\BETA$, $\LET$, or $\TRIV$ redex. In the two first cases, reduce and restart; in the last case, reduce to return the computed abstraction.
\item CBN. Do $\HD$-reduction as long as possible, until the term becomes either a $\BETA$ or $\TRIV$ redex. In the first case, reduce (with $\mathit{CBN-\BETA}$) and restart; in the last case, reduce to return the computed abstraction.
\end{itemize}
\end{theorem}
%-------------------------------------

This description is not in terms of priorities, but rather reveals the shared organization of the computation and the roles of the different reduction rules, which are the same in both strategies - see Fig.~\ref{fig:shared-organization}. The shared organization, in turn, shows how ``superimposed'' CBV and CBN are in the system, in other words, how agnostic the system is.

%------------------------
\begin{figure}[t]\caption{Agnosticism: the shared organization of CBN and CBV strategies}\label{fig:shared-organization}
\begin{center}
\begin{tabular}{c|c|c|c}
\multicolumn{2}{c|}{} & CBV & CBN\\
\hline
iteration & pre-processing & $\HD_1$ + $\HD_2$ & $\HD_1$\\
\cline{2-4}
%of the computation cycle & real computation & $\BETA$ + $CBV-\LET$ & $CBN-\BETA$\\
of the computation cycle & real computation & $\BETA$ + $\LET$ & $\BETA;\LET$\\
\hline
\multicolumn{2}{c|}{return} & $\TRIV$ & $\TRIV$\\
\hline
\end{tabular}
\end{center}
\end{figure}
%----------------------------


