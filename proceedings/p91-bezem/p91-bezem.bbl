\begin{thebibliography}{10}

\bibitem{AwodeyWarren}
Steve Awodey and Michael Warren.
\newblock Homotopy theoretic models of identity types.
\newblock {\em Mathematical Proceedings of the Cambridge Philosophical
  Society}, 146:45--55, 2009.

\bibitem{BC15}
Marc Bezem and Thierry Coquand.
\newblock A {K}ripke model for simplicial sets.
\newblock {\em TCS}, 2015.
\newblock doi:10.1016/j.tcs.2015.01.035.

\bibitem{Friedman2008}
Greg Friedman.
\newblock An elementary illustrated introduction to simplicial sets.
\newblock Preprint, \url{http://arxiv.org/abs/0809.4221}, 2008.

\bibitem{GabrielZisman1967}
Peter Gabriel and Michel Zisman.
\newblock {\em Calculus of fractions and homotopy theory}.
\newblock Springer, 1967.

\bibitem{SimplHT}
Paul Goerss and J.F. Jardine.
\newblock {\em Simplicial Homotopy Theory}.
\newblock Modern Birkh{\"a}user Classics. Birkhauser Verlag GmbH, 2009.
\newblock Reprint of Vol. 174 of Progress in Mathematics, 1999.

\bibitem{Hofmann}
Martin Hofmann.
\newblock Syntax and semantics of dependent types.
\newblock In A.M. Pitts and P.~Dybjer, editors, {\em Semantics and logics of
  computation}, volume~14 of {\em Publ. Newton Inst.}, pages 79--130. Cambridge
  University Press, Cambridge, 1997.

\bibitem{KLV}
Chris Kapulkin, Peter~LeFanu Lumsdaine, and Vladimir Voevodsky.
\newblock The simplicial model of univalent foundations.
\newblock Preprint, \url{http://arxiv.org/abs/1211.2851}, 2012.

\bibitem{KripkeSem}
Saul Kripke.
\newblock Semantical analysis of intuitionistic logic {I}.
\newblock In M.~Dummett and J.N. Crossley, editors, {\em Formal Systems and
  Recursive Functions}, pages 92--130. North--Holland, Amsterdam, 1965.

\bibitem{Lubarsky2015}
Bob Lubarsky.
\newblock Personal communication, March 2015.

\bibitem{SimplObj}
Jon~Peter May.
\newblock {\em Simplicial Objects in Algebraic Topology}.
\newblock Chicago Lectures in Mathematics. University of Chicago Press, 2nd
  edition, 1993.

\bibitem{McCarty2015}
Charles McCarty.
\newblock Two questions about {IZF} and intuitionistic validity.
\newblock Pdf file, personal communication, March 2015.

\bibitem{Moore1956}
John~C. Moore.
\newblock Algebraic homotopy theory.
\newblock Lectures at Princeton,
  \url{http://faculty.tcu.edu/gfriedman/notes/aht1.pdf}, 1956.

\bibitem{Nikolaus2010}
Thomas Nikolaus.
\newblock Algebraic models for higher categories.
\newblock Preprint, \url{http://arxiv.org/abs/1003.1342}, 2010.

\bibitem{Voevodsky2009}
Vladimir Voevodsky.
\newblock Notes on type systems.
\newblock
  \url{http://www.math.ias.edu/~vladimir/Site3/Univalent_Foundations_files/expressions_current.pdf},
  2009.

\end{thebibliography}
