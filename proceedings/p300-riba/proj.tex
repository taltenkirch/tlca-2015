%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Projection and Fibred Simple Coproducts}
\label{sec:proj}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\noindent
We now check that automata can be equipped with existential quantifications
in the fibered sense.
Namely, given a projection $\pi \in \Alpha[\Sigma\times \Gamma,\Sigma]$,
the induced weakening functor
$\subst \pi : \clos\Aut^{(\W)}_{\Sigma} \to \clos\Aut^{(\W)}_{\Sigma \times \Gamma}$
has a left-adjoint
$\projaut_{\Sigma,\Gamma}$, and moreover this structure
is preserved by substitution, in the sense of the Beck-Chevalley condition
(see \eg~\cite{jacobs01book}).
%
This will lead %in Sect.~\ref{sec:nd}
to a (weak) completeness property of the synchronous
arrow on \emp{non-deterministic} automata, to be discussed below.

Recall from~\cite[Thm. IV.1.2.(ii)]{maclane98book}
than an adjunction $\projaut_{\Sigma,\Gamma} \ladj \subst \pi$,
with $\subst \pi$ a functor, is completely determined by the following data:
To each object $\Sigma \times \Gamma \thesis \At A$,
an object $\Sigma \thesis \projaut_{\Sigma,\Gamma} \At A$,
and a map
\(
\eta_{\At A} :
\Sigma \times \Gamma \thesis \At A
\longto
\Sigma \times \Gamma \thesis
  (\projaut_{\Sigma,\Gamma}\At A)[\pi]
\)
satisfying the following universal lifting property:
\begin{equation}
\label{eq:proj:adj:unit:univ}
\begin{array}{l}
\text{For every}\\
\multicolumn{1}{r}{
\sigma \quad:\quad \Sigma \times \Gamma \thesis \At A
  \quad\longto\quad \Sigma \times \Gamma \thesis \At B[\pi]}
\\
\text{there is a unique}\\
\multicolumn{1}{r}{
\tau \quad :\quad
\Sigma \thesis \projaut_{\Sigma,\Gamma} \At A
\quad\longto\quad
\Sigma \thesis \At B}
\end{array}
\qquad\text{s.t.}\qquad
\xymatrix@C=50pt{
  \At A
  \ar[r]^{\eta_{\At A}}
  \ar[dr]_{\sigma}
& (\projaut_{\Sigma,\Gamma} \At A)[\pi]
  \ar[d]^{\subst\pi(\tau)}
\\
& \At B[\pi]
}
\end{equation}

\noindent
In our context, the Beck-Chevalley condition
amounts to the equalities
\begin{equation}
\label{eq:proj:bc}
\Delta \thesis (\projaut_{\Sigma,\Gamma} \At A)[\beta]
\ =\
\Delta \thesis \projaut_{\Delta,\Gamma}(\At A[\beta \times \Id_\Gamma])
\qquad\quad
\eta_{\At A[\beta \times \Id_\Gamma]}
\ =\
\subst{(\beta \times \Id_\Gamma)}(\eta_{\At A})
\end{equation}


\renewcommand\fntext{It is well-known that the projection
operation is correct \wrt\ the recognized
languages only on \emp{non-deterministic automata}.}
\noindent
It turns out that the usual projection operation
on automata (see \eg~\cite{walukiewicz02tcs})
is not functorial.
Surprisingly, this is independent from whether automata
are non-deterministic or not\fn.
We devise a \emp{lifted} projection operation, which indeed
leads to a fibered existential quantification,
and which is correct, on non-deterministic automata,
\wrt\ the recognized languages.
%Non-deterministic automata are discussed in Sect.~\ref{sec:nd}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subparagraph{The Lifted Projection.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Consider $\Sigma \times \Gamma \thesis \At A$
with
\(
\At A = (Q,\init q,\trans,\Omega)
\).
Define $\Sigma \thesis \projaut_{\Sigma,\Gamma} \At A$
as
\(
\projaut_{\Sigma,\Gamma} \At A
\ \deq\
(Q \times \Gamma + \{\init q\},\init q,\trans_{\projaut \At A},\Omega_{\projaut \At A})
\)
where
\[
\begin{array}{r !{\quad\deq\quad} l}
\trans_{\projaut \At A}(\init q,a) & 
%\bigcup_{b \in \Gamma} \alift \trans(\init q,(a,b)) b
\bigcup_{b \in \Gamma} \{ \alift \conj b \tq \conj \in \trans(\init q,(a,b))\}
\\
\trans_{\projaut \At A}((q,\_),a) & 
%\bigcup_{b \in \Gamma} \alift \trans(q,(a,b)) b
\bigcup_{b \in \Gamma} \{ \alift \conj b \tq \conj \in \trans(q,(a,b))\}
\end{array}
\]
and, given $\conj \in \Po(Q \times \Dir)$ and $b \in \Gamma$,
we let
$\alift \conj b \deq \{ ((\alift q b,d) \tq (q,d) \in \conj \}$
with
$\alift q b \deq (q,b)$.

\noindent
For the acceptance condition, we let
\(
\init q \cdot (q_0,b_0) \cdot \ldots \cdot (q_n,b_n) \cdot \ldots 
\)
in $\Omega_{\projaut \At A}$
iff
\(
\init q \cdot q_0 \cdot \ldots \cdot q_n \cdot \ldots
~\in~ \Omega
\).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subparagraph{Action on Plays of The Lifted Projection.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The action on plays of $\projaut_{\Sigma,\Gamma}$ is characterized
by the map
\(
\Play(\projaut) :
%\quad:\quad
\Play_{\Sigma \times \Gamma}(\At A)
%\quad\longto\quad
\longto
\Play_\Sigma(\projaut_{\Sigma,\Gamma} \At A)
\)
inductively defined as
$\Play(\projaut)(\es,\init q) \deq (\es,\init q)$
and
\[
\begin{array}{r @{\quad\deq\quad} l}
%  \Play(\projaut)(\es : (\es,\init q)) & \es : (\es,\init q)
%\\
%  \Play(\projaut)((\es,\init q) \edge (\es,(a,b),\conj))
%& (\es,\init q) \edge (\es,a,\{((q,b),d) \tq (q,d) \in \conj\})
%\\
  \Play(\projaut)((\es,\init q) \edge^* (p,q) \edge (p,(a,b),\conj))
%& \Play(\projaut)((\es,\init q) \edge^* (p,q)) \edge
%  (p,a, \{((q',b),d) \tq (q',d) \in \conj\})
& \Play(\projaut)((\es,\init q) \edge^* (p,q)) \edge (p,a, \alift \conj b)
\\
  \Play(\projaut)((\es,\init q) \edge^* (p,(a,b),\conj) \edge (p.d,q))
%& \Play(\projaut)((\es,\init q) \edge^* (p,(a,b),\conj)) \edge (p.d,(q,b))
& \Play(\projaut)((\es,\init q) \edge^* (p,(a,b),\conj)) \edge (p.d,\alift q b)
\end{array}
\]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}
\label{prop:proj:play:bij}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
If $\At A$ is a complete automaton, then $\Play(\projaut)$
is a bijection.
\end{proposition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subparagraph{The Unit Maps $\eta_{(-)}$.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Consider the injection
\(
\iota_{\Sigma,\Gamma} :
\Play_\Sigma(\projaut_{\Sigma,\Gamma} \At A)
\longto
\Play_{\Sigma\times \Gamma}((\projaut_{\Sigma,\Gamma} \At A)[\pi])
\)
inductively defined as
\(
  \iota_{\Sigma,\Gamma}((\es,\init q_{\At A}))
\deq
(\es,\init q_{\At A})
\)
and
\(
  \iota_{\Sigma,\Gamma}(s \edge (p, \alift q b))
\deq
  \iota_{\Sigma,\Gamma}(s) \edge (p,\alift q b)
\)
and
\(
  \iota_{\Sigma,\Gamma}(s \edge (p,a, \alift \conj b))
\deq
  \iota_{\Sigma,\Gamma}(s) \edge (p,(a,b), \alift \conj b)
\).

\noindent
If $\Sigma \times \Gamma \thesis \At A$ is complete,
we let
the unit $\eta_{\At A}$ be the unique
strategy of $\clos\SAG^\W_{\Sigma\times\Gamma}$
such that
\(
\HS(\eta_{\At A})
%\quad=\quad
=
\{ (t, \iota_{\Sigma,\Gamma} \comp \Play(\projaut)(t))
  \tq t \in \Play_{\Sigma\times \Gamma}(\At A) \}
\).
%
%\noindent
We do not detail the B.-C. condition~(\ref{eq:proj:bc}).
%which is direct to check.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subparagraph{The Unique Lifting Property~(\ref{eq:proj:adj:unit:univ}).}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Consider some
\(
\Sigma \times \Gamma \thesis 
\sigma  : \At A \synchto \At B[\pi]
\)
with $\At A$ complete.
%
We let $\tau$ be the unique strategy such that
\(
\HS(\tau)
%\quad=\quad
=
\{(\Play(\projaut)(s),\Play(\pi)(t)) \tq
  (s,t) \in \HS(\sigma) \}
\).
%
%\noindent
It is easy to see that $\tau$ is winning whenever $\sigma$ is winning.
Moreover

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{lemma}
\label{lem:proj:tau:lift}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\(
\sigma = \subst\pi(\tau) \comp \eta_{\At A}
\).
\end{lemma}
\noindent
For the unicity part of the lifting property
of $\eta_{\At A}$, it is sufficient to check:

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{lemma}
\label{lem:proj:tau:unique}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
If
\(
  \subst\pi(\theta) \comp \eta_{\At A}
= \subst\pi(\theta') \comp \eta_{\At A}
\)
then $\theta = \theta'$.
\end{lemma}

