%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Symmetric Monoidal Structure}
\label{sec:mon}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\noindent
We now consider a synchronous product of automata.
When working on \emp{complete} automata
(to be defined in Sect.~\ref{sec:mon:compl} below),
it gives rise to split symmetric monoidal
fibrations, in the sense of~\cite{shulman08tac}.
%\[
%\clos\game \quad:\quad \clos\SAG^{(\W)} \quad\longto\quad \Tree
%\qquad
%\clos\aut \quad:\quad \clos\SAG^{(\W)} \quad\longto\quad \Alpha
%\]

%\noindent
According to~\cite[Thm. 12.7]{shulman08tac},
split symmetric monoidal fibrations can equivalently be
obtained from 
split symmetric monoidal indexed categories.
%strict mononoidal functors.
In our context, this means that the functors $\subst{(-)}$
extend to
\[
\subst{(-)} ~~~:~~~ \Tree^\op ~~~\longto~~~ \SymMonCat
\qquad\quad
\subst{(-)} ~~~:~~~ \Alpha^\op ~~~\longto~~~ \SymMonCat
\]
where $\SymMonCat$ is the category of symmetric monoidal categories
and strong monoidal functors.
%
Hence, we equip our categories of (complete) acceptance games
and automata with a symmetric monoidal structure.
Substitution turns out to be \emp{strict} symmetric monoidal.

We refer to~\cite{mellies09ps} for background on symmetric monoidal categories.
%which is preserved on the noise by substitution.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Complete Tree Automata}
\label{sec:mon:compl}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\noindent
An automaton $\At A$ is \emp{complete} if
for every $(q,a) \in Q \times \Sigma$, the set
$\trans(q,a)$ is not empty and moreover for every
$\conj \in \trans(q,a)$ and every
$d \in \Dir$,
we have $(q',d) \in \conj$ for some $q' \in Q$.

Given an automaton
\(
\At A \ =\ (Q,\init q,\trans,\Omega)
\)
its \emp{completion}
is the automaton
\(
\clos{\At A} \ \deq\
  (\clos Q,\init q,\clos\trans,\clos \Omega)
\)
with states
$\clos Q \deq Q + \{\true,\false\}$,
with acceptance condition 
$\clos\Omega \deq \Omega + Q^* \cdot \true \cdot {\clos Q}^\omega$,
and with transition function $\clos\trans$ defined as
\[
\begin{array}{r !{\ \deq\ } l !{\qquad} r !{\ \deq\ } l}
  \clos\trans(\true,q)
& \{\{(\true,d) \tq d \in \Dir\}\}
& \clos\trans(\false,q)
& \{\{(\false,d) \tq d \in \Dir\}\}
\\[0.5em]
  \clos\trans(q,a)
& \{\{(\false,d) \tq d \in \Dir\}\}
& \multicolumn{2}{l}{\text{if $q \in Q$ and $\trans(q,a) = \emptyset$}}
\\
  \clos\trans(q,a)
& \{ \clos\conj \tq \conj \in \trans(q,a)\}
& \multicolumn{2}{l}{\text{otherwise}}
\end{array}
\]
where, given $\conj \in \trans(q,a)$,
we let
\(
\clos\conj 
%\quad\deq\quad
\ \deq\ 
\conj \cup \{(\true,d) \tq \text{there is no $q\in Q$ s.t.\@ $(q,d) \in \conj$}\}
\).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}
\label{prop:mon:clos:cor}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$\Lang(\At A) = \Lang(\clos{\At A})$.
\end{proposition}

\noindent
Restricting to complete automata gives rise to full
subcategories
$\clos\SAG^{(\W)}_{\Sigma}$
and
$\clos\Aut^{(\W)}_{\Sigma}$
of resp.\@ $\SAG^{(\W)}_\Sigma$ and $\Aut^{(\W)}_\Sigma$,
and thus induces fibrations
\[
\clos\game \quad:\quad \clos\SAG^{(\W)} \quad\longto\quad \Tree
\qquad\qquad
\clos\aut \quad:\quad \clos\Aut^{(\W)} \quad\longto\quad \Alpha
\]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Synchronous Product}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Assume given complete
automata $\Sigma \thesis \At A$ and $\Sigma \thesis \At B$.
Define
\(
\Sigma \thesis \At A \synchprod \At B
\)
as %follows:
\[
\At A \synchprod \At B
\quad\deq\quad
(Q_{\At A} \times Q_{\At B},(\init q_{\At A},\init q_{\At B}),
\trans_{\At A \synchprod \At B}, \Omega_{\At A \synchprod \At B})
\]

\noindent
where
$(q^n_{\At A},q^n_{\At B})_{n \in \Nat} \in \Omega_{\At A \synchprod \At B}$
iff
($(q^n_{\At A})_{n \in \Nat} \in \Omega_{\At A}$
and
$(q^n_{\At B})_{n \in \Nat} \in \Omega_{\At B}$),
and where %for the transition function we let
we let
$\trans_{\At A \synchprod \At B}((q_{\At A},q_{\At B}),a)$
be the set of all the $\conj_{\At A} \synchprod \conj_{\At B}$
for
$\conj_{\At A} \in \trans_{\At A}(q_{\At A},a)$
and
$\conj_{\At B} \in \trans_{\At B}(q_{\At B},a)$,
with
\(
\conj_{\At A} \synchprod \conj_{\At B}
%\quad\deq\quad
\ \deq \
\{ ((q'_{\At A},q'_{\At B}),d) \tq d \in \Dir 
   ~\text{and}~ (q'_{\At A},d) \in \conj_{\At A}
   ~\text{and}~ (q'_{\At B},d) \in \conj_{\At B}
\}
\).

Note that since $\At A$ and $\At B$ are complete,
each
\(
\conj_{\At A \synchprod \At B} \in
\trans_{\At A \synchprod \At B}((q_{\At A},q_{\At B}),a)
\)
uniquely decomposes as
$\conj_{\At A \synchprod \At B} = \conj_{\At A} \synchprod \conj_{\At B}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subparagraph{Action on Plays.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The unique decomposition property
of $\conj_{\At A \synchprod \At B}$
allows to define projections %maps
%(denoted $\Proj_i$ in both cases):
\[
\begin{array}{r @{~~~~:~~~~} r @{~~~~\longto~~~~} l}
\Proj_i
&
\Play_\Sigma(\At A_1 \synchprod \At A_2,M)
%\quad\to\quad
&
\Play_\Sigma(\At A_i,M)
\\
\Proj_i^{\synchto}
&
  \Play_\Sigma \left(
    \G(\At A_1 \synchprod \At B_1,M) \synchto
    \G(\At A_2 \synchprod \At B_2,N)
  \right)
%\quad\to\quad
&
  \Play_\Sigma \left(
    \G(\At A_i,M) \synchto \G(\At B_i,N)
  \right)
\end{array}
\]

\noindent 
%which satisfy the expected universal property:
We write $\SP \deq \pair{\Proj_1,\Proj_2}$
and $\synch\SP \deq \pair{\Proj_1^{\synchto},\Proj_2^{\synchto}}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}
\label{prop:mon:pb}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
We have, in $\Set$:
\[
\xymatrix{%@C=50pt{
  \Play_\Sigma(\At A \synchprod \At B,M)
  \ar@{} [dr] |<{\pb}
  \ar[r]^(0.55){\Proj_2}
  \ar[d]_{\Proj_1}
& \Play_\Sigma(\At B,M)
  \ar[d]^{\tr}
\\
  \Play_\Sigma(\At A,M)
  \ar[r]^{\tr}
& \Tr_\Sigma
}
\quad
\xymatrix@C=-35pt@R=15pt{
  \Play^\Prop_\Sigma(\G(\At A \synchprod \At B, M)
  \synchto \G(\At C \synchprod D, N))
  \ar@{} [ddr] |<{\pb}
  \ar[dr]_(.4){\Proj_2^{\synchto}}
  \ar[dd]_{\Proj_1^{\synchto}}
&
\\
& \Play^\Prop_\Sigma(\G(\At B, M) \synchto \G(\At D, N))
  \ar[d]^{\tr}
\\
  \Play^\Prop_\Sigma(\G(\At A, M) \synchto \G(\At C, N))
  \ar[r]_(0.7){\tr}
& \Tr_\Sigma
}
\]
\end{proposition}

%We write $\SP \deq \pair{\Proj_1,\Proj_2}$
%and $\synch\SP \deq \pair{\Proj_1^{\synchto},\Proj_2^{\synchto}}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subparagraph{Action on Synchronous Games.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The action of $\synchprod$ on the objects of $\clos\SAG^{(\W)}_\Sigma$
is given by
\[
(\Sigma \thesis \G(\At A,M))
\synchprod
(\Sigma \thesis \G(\At B,N))
\quad\deq\quad
\Sigma \thesis \G(\At A[\pi] \synchprod \At B[\pi'],\pair{M,N})
\]
where $\pi$ and $\pi'$ are suitable projections.
%
For morphisms, let
%On morphisms, given
\(
\Sigma \thesis \sigma : \G(\At A_0,M_0) \synchto \G(\At A_1,M_1)
\)
and
\(
\Sigma \thesis \tau : \G(\At B_0,N_0) \synchto \G(\At B_1,N_1)
\).
Then since
\(
\Sigma \thesis \G(\At A_i[\pi_i],\pair{M_i,N_i})
\ = \
\Sigma \thesis \G(\At A_i,M_i)
\)
and
\(
\Sigma \thesis \G(\At B_i[\pi'_i],\pair{M_i,N_i})
\ = \
\Sigma \thesis \G(\At B_i,N_i)
\),
thanks to
Prop.~\ref{prop:mon:pb}
we can simply let
$\sigma \synchprod \tau \deq \synch\SP^{-1}(\sigma,\tau)$.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}
\label{prop:mon:prod}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The %synchronous
product $\_\synchprod\_$ gives %binary
functors
\(
\clos\SAG^{(\W)}_\Sigma \times \clos\SAG^{(\W)}_\Sigma \longto
\clos\SAG^{(\W)}_\Sigma
\).
\end{proposition}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Symmetric Monoidal Structure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Thanks to Prop.~\ref{prop:mon:pb}
and Prop.~\ref{prop:sag:lift}
the symmetric monoidal structure of $\synchprod$
in $\clos\SAG^{(\W)}_\Sigma$ can be directly obtained
from the symmetric monoidal structure of the tensorial
product of $\Rel(\Set/\Tr_\Sigma)$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subparagraph{Symmetric Monoidal Structure in $\Rel(\Set/J)$.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
We define a product $\tensor$ in
$\Rel(\Set/J)$:% as follows:
\begin{description}
\item[On Objects:]
for $(A,g)$ and $(B,h)$ objects in $\Rel(\Set/J)$
the product
$A \tensor B$ is
$A \times_J B$
with the corresponding map,
that is
\[
A \tensor B \quad\deq\quad
\{(a,b) \in A \times B \tq g(a) = h(b)\}
\quad\stackrel{g \comp \pi_1 = h \comp \pi_2}\longto\quad J
\]
\item[On Morphisms:]
given $R \in \Rel(\Set/J)[A,C]$ and
$P \in \Rel(\Set/J)[B,D]$,
we define $R \tensor P \in \Rel(\Set/J)[A \tensor B,C \tensor D]$
as
\[
R \tensor P \quad\deq\quad
\{((a,b),(c,d)) \in (A \tensor B) \times (C \tensor D) \tq 
%(a,b) \in A \tensor B ~\text{and}~
%(c,d) \in C \tensor D ~\text{and}~
(a,c) \in R ~\text{and}~ (b,d) \in P \}
\]
\end{description}

\noindent
For the unit, we \emp{choose} some
\(
\tu \ =\ (\umap : I \stackrel\iso\longto J)
\).
Note that $\umap$ is required to be a bijection.

\noindent
The natural isomorphisms are given by:
\[
\begin{array}{r !{\quad\deq\quad} l}
  \rel\alpha_{A,B,C}
& \{(((a,b),c) ~,~ (a,(b,c))) \tq
  %(a,b,c) \in A \times B \times C
  %~\text{and}~ b \in B ~\text{and}~ c \in C
  %~\text{and}~
  g_A(a) = g_B(b) = g_C(c) \}
\\
  \rel\lambda_A
& \{((e,a) ~,~ a) \tq \umap(e) = g_A(a) \}
\\
  \rel\rho_A
& \{((a,e) ~,~ a) \tq g_A(a) = \umap(e) \}
\\
  \rel\gamma_{A,B}
& \{((a,b) ~,~ (b,a)) \tq
  %a \in A ~\text{and}~ b \in B ~\text{and}~ 
  g_A(a) = g_B(b) \}
\end{array}
\]



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}
\label{prop:mon:relset:smc}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The category $\Rel(\Set/J)$, equipped with the above data,
is symmetric monoidal.
\end{proposition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subparagraph{Unit Automata.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The requirement that the monoidal unit $\umap : I \to J$
of $\Rel(\Set/J)$ should be a bijection leads us to the following
unit automata.
We let
%We define $\Sigma \thesis \AF$ as
\(
\AU \deq (Q_\AU, q_\AU,\trans_\AU,\Omega_\AU)
\)
where $Q_\AU \deq \one$, $q_\AU \deq \bullet$,
$\Omega_\AU = Q^\omega_\AU$
and
\(
\trans_\AU(q_\AU,a) \deq
  \{\{(q_\AU,d) \tq d \in \Dir \}\}
\).

Note that since $\trans_{\AU}$ is constant, we have
\(
\Sigma \thesis \G(\AU,M) = \Sigma \thesis \G(\AU,\Id)
\).
Moreover, %as expected:

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}
\label{prop:mon:unit}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given $M \in \Tree[\Sigma,\Gamma]$, we have, in $\Set$, a bijection
\[
\tr \quad:\quad \Play_\Sigma(\AU,M) \quad\stackrel\iso\longto\quad \Tr_\Sigma
\]
\end{proposition}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subparagraph{Symmetric Monoidal Structure.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Using Prop.~\ref{prop:sag:lift},
the structure isos of $\Rel(\Set/\Tr_\Sigma)$
can be lifted to $\clos\SAG^{(\W)}_\Sigma$
(winning is trivial).
Moreover, the required equations (naturality and coherence)
follows from Prop.~\ref{prop:sag:hs:pb}, Prop~\ref{prop:mon:pb},
and the fact that 
\(
((\SP \times \SP) \comp \HS)(\sigma \synchprod \tau) \ =\ 
\HS(\sigma) \tensor \HS(\tau)
\)
(where composition on the left is in $\Set$,
and the expression denotes the actions of the resulting function on the
set of plays $(\sigma \synchprod \tau)$).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The categories $\clos\SAG^{(\W)}_\Sigma$ and $\clos\Aut^{(\W)}_\Sigma$
equipped with the above data, are symmetric monoidal.
\end{proposition}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Symmetric Monoidal Fibrations}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


In order to obtain symmetric monoidal fibrations,
by~\cite[Thm. 12.7]{shulman08tac},
it remains to check that substitution is strong monoidal.
It is actually \emp{strict} monoidal: 
it directly commutes with $\synchprod$ and preserves the unit,
as well as all the structure maps.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\hfill
\begin{enumerate}[(i)]
\item Given $L \in \Tree[\Sigma,\Gamma]$,
the functors $\subst L : \clos\SAG^{(\W)}_\Gamma \to \clos\SAG^{(\W)}_\Sigma$
are strict monoidal.

\item Given $\beta \in \Alpha[\Sigma,\Gamma]$,
the functors $\subst \beta : \clos\Aut^{(\W)}_\Gamma \to \clos\Aut^{(\W)}_\Sigma$
are strict monoidal.
\end{enumerate}
\end{proposition}

