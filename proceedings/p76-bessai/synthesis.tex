In this section we denote type assignment in $\lambdaR$ by $\vdashr$ and fix the following ingredients:
\begin{itemize}
\item A finite set of classes $\mathcal{C}$.
\item For each $C \in \mathcal{C}$ types $\sigma_C \in \TT, \rho_C \in \TTR$ such that $\interpret{\sigma_C \to \rho_C}$ is defined and $\emptyset \vdashr C : \sigma_C \to \rho_C$.
\item A finite set of mixins $\mathcal{M}$.
\item For each $M \in \mathcal{M}$ types $\sigma_M \in \TT$ and $\rho_M^1, \rho_M^2 \in \TTR$ such that $\interpret{\sigma_M}, \interpret{\rho_M^1}, \interpret{\rho_M^2}$ are defined and for all types $\rho \in \TTR$ we have 
$\emptyset \vdashr M : (\sigma_M \to \rho \cap \rho_M^1) \to (\sigma_M \to \rho + \rho_M^2)$.
\item For each $M \in \mathcal{M}$ the non-empty set of labels $L_M = \lbl(\rho_M^2) \subseteq \mathcal{L}$.
\end{itemize}

We translate given classes and mixins to the following a repository $\DeltaLCM$ of combinators
\begin{align*}
\DeltaLCM = & \{C : \interpret{\sigma_C \to \rho_C} \mid C \in \mathcal{C}\} \\
& \cup \{M : w_{L_M}(\alpha) \to (\interpret{\sigma_M} \to \alpha \cap \interpret{\rho_M^1}) \to (\interpret{\sigma_M} \to \alpha \cap \interpret{\rho_M^2}) \mid M \in \mathcal{M}\}\\
& \cup \Delta_\mathcal{L} \cup \{W_{L_M} \in \Delta_{\wp(\mathcal{L})} \mid M \in \mathcal{M}, |L_M| > 1\}
\end{align*}

To simplify notation, we introduce the infix metaoperator $\pipe$ such that $x \pipe f = f\ x$. It is right associative and has the lowest precedence. Accordingly, $x \pipe f \pipe g = g\ (f\ x)$.

Although types in $\DeltaLCM$ do not contain record-merge, we show by following Theorem \ref{thm:soundness} that types of mixin compositions in \bcl\ are sound.

\begin{theorem}[Soundness]
\label{thm:soundness}
Let $M_1, \ldots, M_n \in \mathcal{M}$ be mixins, let $L_1, \ldots, L_n \subseteq \mathcal{L}$ be sets of labels, let $C \in \mathcal{C}$ be a class and let $\sigma \in \TT, \rho \in \TTR$ be types such that $\interpret{\sigma \to \rho}$ is defined.\\
If $\DeltaLCM \vdashbcl C \pipe (M_1\ W_{L_1}) \pipe (M_2\ W_{L_2}) \pipe \ldots \pipe (M_n\ W_{L_n}) : \interpret{\sigma \to \rho}$, \\
then $\emptyset \vdashr C \pipe M_1 \pipe M_2 \pipe \ldots \pipe M_n : \sigma \to \rho$.
\end{theorem}

\begin{proof} (Sketch)
Induction on $n$ proving $L_i = L_{M_i}$ followed by Lemma \ref{lem:enc_soundness} and $(\ArrE)$.
\end{proof}

Complementary, we show by the following Theorem \ref{thm:partial_completeness} that typing of mixin compositions in \bcl\ is complete with respect to previously described typing in $\TT$.

\begin{theorem}[Partial Completeness]
\label{thm:partial_completeness}
Let $\Gamma \subseteq \{x_C : \sigma_C \to \rho_C \mid C \in \mathcal{C}\} \cup \{x_M^\rho : (\sigma_M \to \rho \cap \rho_M^1) \to (\sigma_M \to \rho + \rho_M^2) \mid M \in \mathcal{M}, \rho \in \TTR, \interpret{\rho} \text{ is defined}\}$ be a finite context and let $\sigma \in \TT, \rho \in \TTR$ be types such that $\interpret{\sigma \to \rho}$ is defined.\\
If $\Gamma \vdashr x_C \pipe x_{M_1}^{\rho_1} \pipe x_{M_2}^{\rho_2} \pipe \ldots \pipe x_{M_n}^{\rho_n} : \sigma \to \rho$,\\
then $\DeltaLCM \vdashbcl C \pipe (M_1\ W_{L_{M_1}}) \pipe (M_2\ W_{L_{M_2}}) \pipe \ldots \pipe (M_n\ W_{L_{M_n}}) : \interpret{\sigma \to \rho}$.
\end{theorem}

\begin{proof} (Sketch)
Induction on $n$ choosing for each $x_{M}^{\rho}$ where $\rho = \bigcap\limits_{l \in L} \record{l : \tau_{l}}$ (resp. $\rho = \record{}$) the substitution $S_i(\alpha) = \bigcap\limits_{l \in L \setminus L_M} \rrecord{l(\tau_{l})}$ to type $M \in \DeltaLCM$ and using $(\ArrE)$ and Lemma \ref{lem:enc_completeness}.
\end{proof}

Coming back to our running example, we obtain
%$\Point : \Int \to \record{\get : \Int, \set : \Int \to \Int, \shift : \Int \to \Int}$ and for all types $\rho \in \TTR$ we have $\Movable \colon (\Int \to \rho \cap \record{\set : \Int \to \Int, \shift : \Int \to \Int}) \to (\Int \to \rho + \record{\set : \Int, \move : \Int})$.
%\begin{align*}
%\Delta_{\{\get, \set, \shift, \move\}}^{\{\Point\},\{\Movable\}} & = \{ \\
%& \Point : && \Int \to \rrecord{\get(\Int) \cap \set(\Int \to \Int) \cap \shift(\Int \to \Int)}, \\
% & \Movable : &&
%\begin{aligned}[t]
%& w_{\{\set, \move\}}(\alpha) \\
%& \to (\Int \to \alpha \cap \rrecord{\set(\Int \to \Int) \cap \shift(\Int \to \Int)})\\
%& \to (\Int \to \alpha \cap \rrecord{\set(\Int) \cap \move(\Int)}),
%\end{aligned}\\
%&W_{\{\get\}} : && w_{\{\get\}}(\rrecord{\set(\alpha_1) \cap \shift( \alpha_2) \cap \move(\alpha_3)}),\\
%&W_{\{\set\}} : && w_{\{\set\}}(\rrecord{\get(\alpha_1) \cap \shift( \alpha_2) \cap \move(\alpha_3)}),\\
%&W_{\{\shift\}} : && w_{\{\shift\}}(\rrecord{\get(\alpha_1) \cap \set( \alpha_2) \cap \move(\alpha_3)}),\\
%&W_{\{\move\}} : && w_{\{\move\}}(\rrecord{\get(\alpha_1) \cap \set( \alpha_2) \cap \shift(\alpha_3)}),\\
%&W_{\{\set, \move\}} : && w_{\{\set\}}(\alpha) \to w_{\{\move\}}(\alpha) \to w_{\{\set, \move\}}(\alpha)\}
%\end{align*}
\begin{align*}
\Delta_{\{\get, \set, \shift, \move\}}^{\{\Point\},\{\Movable\}} = \{\;
& \Point : && \Int \to \rrecord{\get(\Int) \cap \set(\Int \to \Int) \cap \shift(\Int \to \Int)}, \\
 & \Movable : &&
\begin{aligned}[t]
& w_{\{\set, \move\}}(\alpha) \\
& \to (\Int \to \alpha \cap \rrecord{\set(\Int \to \Int) \cap \shift(\Int \to \Int)})\\
& \to (\Int \to \alpha \cap \rrecord{\set(\Int) \cap \move(\Int)}),
\end{aligned}\\
&W_{\{\get\}} : && w_{\{\get\}}(\rrecord{\set(\alpha_1) \cap \shift( \alpha_2) \cap \move(\alpha_3)}),\\
&W_{\{\set\}} : && w_{\{\set\}}(\rrecord{\get(\alpha_1) \cap \shift( \alpha_2) \cap \move(\alpha_3)}),\\
&W_{\{\shift\}} : && w_{\{\shift\}}(\rrecord{\get(\alpha_1) \cap \set( \alpha_2) \cap \move(\alpha_3)}),\\
&W_{\{\move\}} : && w_{\{\move\}}(\rrecord{\get(\alpha_1) \cap \set( \alpha_2) \cap \shift(\alpha_3)}),\\
&W_{\{\set, \move\}} : && w_{\{\set\}}(\alpha) \to w_{\{\move\}}(\alpha) \to w_{\{\set, \move\}}(\alpha)\}
\end{align*}
We may ask inhabitation questions such as $$\Delta_{\{\get, \set, \shift, \move\}}^{\{\Point\},\{\Movable\}} \vdashbcl ? : \interpret{\Int \to \record{\shift : \Int \to \Int, \move : \Int}}$$
and obtain the combinatory term  ``$\Movable \ W_{\{\set, \move\}} \ \Point$'' as a synthesized result. From Theorem \ref{thm:soundness} we know $$\emptyset \vdashr \Movable \ \Point : \Int \to \record{\shift : \Int \to \Int, \move : \Int}$$

On the other hand, if we want to inhabit $\interpret{\Int \to \record{\set : \Int \to \Int, \move : \Int}}$ we obtain no results. From Lemma \ref{thm:partial_completeness} we know that, restricted to the previously described typing in $\TT$, there is no mixin composition applied to a class with the resulting type $\Int \to \record{\set : \Int \to \Int, \move : \Int}$.

The presented encoding has several benefits with respect to scalability. 
First, the size of the presented repositories is polynomial in $|\mathcal{L}| * |\mathcal{C}| * |\mathcal{M}|$. 
Second, expanding the label set $\mathcal{L}$ requires only to update combinators in $\Delta_{\mathcal{L}}$ leaving existing types of classes and mixins untouched. 
Third, adding a class/mixin to an existing repository is as simple as adding one typed combinator for the class/mixin and at most one logical combinator. Again, it is important that the existing combinators in the repository remain untouched. As an example, we add the following mixin $\MovableBy$ to $\Delta_{\{\get, \set, \shift, \move\}}^{\{\Point\},\{\Movable\}}$.
\begin{align*}
\MovableBy = \lambda \argClass. \Y (\lambda & \myClass. \lambda \state. \\
& \letin{\super = \argClass \ \state} \\
& \letin{\self = \myClass \ \state} \super \oplus \record{\move = \super.\shift})
\end{align*}

In $\lambdaR$ for all types $\rho \in \TTR$ we have 
$$\emptyset \vdashr \MovableBy \colon (\Int \to \rho \cap \record{\shift : \Int \to \Int}) \to (\Int \to \rho + \record{\move : \Int \to \Int})$$

We obtain the following extended repository
\begin{align*}
&\Delta_{\{\get, \set, \shift, \move\}}^{\{\Point\},\{\Movable, \MovableBy\}} = \Delta_{\{\get, \set, \shift, \move\}}^{\{\Point\},\{\Movable\}} \cup \{ \MovableBy : w_{\{\move\}}(\alpha)\\
&\qquad \to (\Int \to \alpha \cap \rrecord{\shift(\Int \to \Int)}) \to (\Int \to \alpha \cap \rrecord{\move(\Int \to \Int)}) \}
\end{align*}

Asking the inhabitation question $$\Delta_{\{\get, \set, \shift, \move\}}^{\{\Point\},\{\Movable, \MovableBy\}} \vdashbcl ? : \interpret{\Int \to \record{\set : \Int, \move : \Int \to \Int}}$$ synthesizes ``$\Point \pipe (\Movable \ W_{\{\set, \move\}}) \pipe (\MovableBy \ W_{\{\move\}})$''. Note that even in such a simplistic scenario the order in which mixins are applied can be crucial mainly because $\Override$ is not commutative. Moreover, the early binding of self and the associated preservation of overwritten methods may make multiple applications of a single mixin meaningful.