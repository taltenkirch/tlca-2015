%!TEX root = submission.tex


\newcommand{\State}{\textsf{state}}


% \subsection{A $\lambda$-calculus with record and record merge}
\subsection{Intersection and record types}

We consider a type-free $\lambda$-calculus of extensible records, equipped with a  merge operator.
The term syntax is defined by the following grammar:
\[
\begin{array}{rlll}
 \lambdaR \ni M,N, M_i  & ::= &  x \mid (\lambda x.M) \mid (MN)  \mid (M.l) \mid R \mid (M \oplus R) & \text{terms} \\ [1mm]
R & ::= & \record{ l_i = M_i \mid i \in I} & \text{records}
\end{array}
\]
where $x\in\Variable$ and $l\in\Label$ range over denumerably many {\em term variables} and {\em labels} respectively, and the sets of indexes $I$ are finite.
Free and bound variables are defined as usual for ordinary $\lambda$-calculus, and we name $\lambdaR^0$ the set of all closed terms
in $\lambdaR$; terms are identified up to renaming of bound variables and $M\Subst{N}{x}$ denotes capture avoiding substitution of 
$N$ for $x$ in $M$. We adopt notational conventions from \cite{Barendregt84}; in particular application associates to the left and external 
parentheses are omitted when unnecessary; also the dot notation for record selection takes precedence over $\lambda$, so that
$\lambda x. \;M.l$ reads as $\lambda x. (M.l)$. If not stated otherwise $\Override$ also associates to the left, and we avoid external parentheses when 
unnecessary.

Terms $R \equiv \record{ l_i = M_i \ | \ i \in I }$ (writing $\equiv$ for syntactic identity) represent {\em records}, 
with fields $l_i$ and $M_i$ as the respective values; we set $\lbl(\record{ l_i = M_i \mid i \in I}) = \Set{l_i \mid i\in I}$. The term
$M.l$ is {\em field selection} and $M\oplus R$ 
is record {\em merge}. In particular if $R_1$ and $R_2$ are records then $R_1\Override R_2$ is the record with as fields
the union of the fields of $R_1$ and $R_2$ and as values those of the original records but in case of ambiguity, where the values in
$R_2$ prevail. The syntactic constraint that $R$ is a record in $M \oplus R$ is justified after Definition \ref{def:typeAssignment}. 

The actual meaning of these operations is formalized by the following reduction relation:

\begin{definition}[$\lambdaR$ reduction]
Reduction $\reduces\, \subseteq \lambdaR^2$ is the least compatible relation such that:
\[\begin{array}{lrcl}
(\beta) & \ (\lambda x.M)N &\reduces & M\Subst{N}{x} \\ [1mm]
(\redSel) & \Pair{l_i=M_i  \mid  i\in I}.l_j & \reduces & M_j  \hspace{4cm} \mbox{if $j \in I$} \\ [1mm]
(\redMergeThree) & \Pair{l_i=M_i  \mid  i\in I} \Override \Pair{l_j=N_j  \mid \ j\in J} & \reduces &
	\Pair{l_i=M_i, \ l_j=N_j \mid  i\in I\setminus J, \  j\in J}
\end{array}\]
\end{definition}
We claim that $\reduces^*$ is Church-Rosser, and that $(M\Override R_1)\Override R_2$ is equivalent to $M\Override (R_1\Override R_2)$ under any reasonable
observational semantics (e.g. by extending to $\lambdaR$ applicative bisimulation from the lazy $\lambda$-calculus).

Record merge subsumes field update: $M.l := N \equiv M \Override \record{l = N}$, but merge is not uniformly definable in terms of update as long as
labels are not expressions in the calculus.


\medskip

In the spirit of Curry's assignment of polymorphic  types and of intersection types in particular, 
types are introduced as a syntactical tool to capture semantic properties of terms, rather than as constraints to term formation.

\begin{definition}[Intersection types for $\lambdaR$]  \label{def:intertype:lambdaR}
%\record{l_i:\sigma_i \mid i \in I} 
\[\begin{array}{rlll}
\TT \ni \sigma, \sigma_i & ::= &  a \mid \omega \mid \sigma_1\to\sigma_2 \mid \sigma_1 \inter \sigma_2 \mid  \rho & \text{types} \\ [1mm]
\TTR \ni \rho, \rho_i & ::= &   \record{} \mid \record{l:\sigma} \mid \rho_1 + \rho_2 \mid \rho_1 \inter \rho_2 & \text{record types}
\end{array}\]
where %$\alpha,\beta \in \VV$ are {\em type variables},  
$a$ ranges over {\em type constants}, $l\in\Label$.
\end{definition}
We use $\sigma,\tau$, possibly with sub and superscripts, for types in $\TT$ and $\rho,\rho_i$, possibly with superscripts, for record types in $\TTR$ only. Note that $\to$ associates to the right, and $\inter$ binds stronger than $\to$.
As with intersection type systems for the $\lambda$-calculus, the intended meaning of types are sets, provided 
a set theoretic interpretation of constants $a$.
%The type  $\omega$ is the universe;  $\sigma\arrow\tau$ represents the set of functions,
%sending elements of $\sigma$ to elements in $\tau$, and $\sigma \inter \tau$ is set theoretic intersection. 

Following \cite{bcd}, type semantics is given axiomatically by means of the subtyping relation $\leq$, that can be interpreted as subset inclusion. It is
the least pre-order over  $\TT$ such that:
\begin{definition}[Type inclusion: arrow and intersection types]\label{def:typeInclusionInt}
\[\begin{array}{l@{\hspace{1cm}}l@{\hspace{2cm}}l}
\sigma \leq \omega, & \omega \leq \omega\to\omega, & \\ [1mm]
\sigma\inter\tau \leq \sigma, & \sigma\inter\tau \leq \tau, & \sigma \leq \tau_1 \And \sigma\leq \tau_2 \Then \sigma \leq \tau_1\inter\tau_2, \\ [1mm]
\multicolumn{2}{l}{(\sigma\to\tau_1) \inter (\sigma\to\tau_2) \leq \sigma \to \tau_1\inter\tau_2} & 
\sigma_2 \leq \sigma_1 \And \tau_1 \leq \tau_2 \Then \sigma_1\to\tau_1 \leq \sigma_2\to\tau_2
\end{array}\]
\end{definition}

We write $\sigma = \tau$ for $\sigma\leq \tau$ and $\tau\leq\sigma$.

\begin{definition}[Type inclusion: record types]\label{def:typeInclusionRec}
\[\begin{array}{l@{\hspace{0.5cm}}l@{\hspace{0.5cm}}}
\Pair{l:\sigma}\inter\Pair{l:\tau} \leq \Pair{l:\sigma\inter\tau}, &
\sigma \leq \tau \Then \Pair{l:\sigma} \leq \Pair{l:\tau},  \\ [1mm]
\record{l:\sigma} \leq \record{}, & \record{l:\sigma} + \record{} = \record{l:\sigma} =  \record{} + \record{l:\sigma}, \\ [1mm]
\Pair{l:\sigma} + \Pair{l:\tau} =  \Pair{l:\tau}, &
\Pair{l:\sigma} + \Pair{l':\tau} =  \Pair{l:\sigma} \inter \Pair{l':\tau} \qquad (l\neq l'), \\ [1mm]
\Pair{l:\sigma} + (\Pair{l:\tau} \inter \rho) = \Pair{l:\tau} \inter \rho,
&
\Pair{l:\sigma} + (\Pair{l':\tau} \inter \rho) = \Pair{l':\tau} \inter (\Pair{l:\sigma} + \rho) \qquad (l\neq l').
\end{array}\]
\end{definition}

While Definition \ref{def:typeInclusionInt} is standard after \cite{bcd}, comments on Definition \ref{def:typeInclusionRec} are in order.
%The use of intersection types for record stem from \cite{deLiguoro01}, and the usage of unary record types $\Pair{l:\sigma}$ appears
%in \cite{Bakeld08}. 
Type $\record{}$ is the type of all records. 
Type $\Pair{l:\sigma}$ is a unary record type, whose meaning is the set of records having at least a field labeled by 
$l$, with value of type $\sigma$; therefore
$\record{l:\sigma} \inter \record{l:\tau}$ is the type of records having label $l$ with values both of type $\sigma$ and $\tau$, that is of type $\sigma\inter\tau$. 
In fact the equation $\record{l:\sigma} \inter \record{l:\tau} = \Pair{l:\sigma\inter\tau}$ is derivable. On the other hand
 $\record{l:\sigma} \inter \record{l':\tau}$, with $l\neq l'$, is the type of records having fields labeled by $l$ and $l'$, with values of type $\sigma$ and 
 $\tau$ respectively. It follows that intersection of record types can be used to express properties of records with arbitrary (though finitely) many fields, which justifies the abbreviation
$
\Pair{l_i:\sigma_i \mid i\in I \neq \emptyset} = \bigcap_{i\in I }  \Pair{l_i:\sigma_i}
$ and $\Pair{l_i:\sigma_i \mid i\in \emptyset} = \record{}$,
where we assume that the $l_i$ are pairwise distinct. %, and say that it is a record type in {\em normal form}.
Finally, as it will be apparent from Definition \ref{def:typeAssignment} below, $\rho_1+ \rho_2$ is the type of all records obtained by merging 
a record of type $\rho_1$ 
with a record of type $\rho_2$, which is intended to type $\Override$ that
is at the same time a record extension and field updating operation. Since this is the distinctive feature of the system introduced here, we comment
on this by means of a few lemmas, illustrating its properties.


\begin{lemma}\label{lem:normal-recordTypes}
~\hfill
\begin{enumerate}
\item \label{lem:normal-recordTypes-a}
	$(\forall j \in J \subseteq I.~ \sigma_j \leq \tau_j) \Then \Pair{ l_i : \sigma_{i} \mid i \in I } \leq \Pair{l_j : \tau_{j} \mid j \in J }$,
\item \label{lem:normal-recordTypes-b}
	$ \Pair{ l_i : \sigma_{i} \ \mid \ i \in I } +  \Pair{ l_j : \tau_{j} \mid j \in J } =
		\Pair{l_i : \sigma_{i},  m_j : \tau_{j} \mid i\in I\setminus J, j\in J}$,
\item \label{lem:normal-recordTypes-c}
	$\forall \rho \in \TTR .\; \exists \ \Pair{l_i:\sigma_i  \mid  i\in I}.~
		\rho = \Pair{l_i:\sigma_i \mid  i\in I} $.
\end{enumerate}
\end{lemma}

Part (\ref{lem:normal-recordTypes-a}) of Lemma \ref{lem:normal-recordTypes} states that subtyping among intersection of unary record types
subsumes subtyping in width and depth of ordinary record types from the literature. Part (\ref{lem:normal-recordTypes-b}) shows that
the $+$ type constructor reflects at the level of types the operational behavior of the merge operator $\oplus$.
Part (\ref{lem:normal-recordTypes-c}) says that any record type is equivalent to an intersection of unary record types; this
implies that types of the form $\rho_1 + \rho_2$ are eliminable in principle. 
However they play a key role in typing mixins, motivating the issue of control of negative information in the synthesis process: 
see sections \ref{subsec:Mixin} and \ref{sec:synthesis}.
More properties of subtyping record types w.r.t. $+$ and $\inter$ are
listed in the next lemma. Let us preliminary define the map 
$\lbl: \TTR \to \wp(\Label)$ (where $\wp(\Label)$ is the powerset of $\Label$) by:
\[
\begin{array}{llll}
\lbl(\record{l:\sigma}) = \Set{l}, & \lbl(\rho_1 \inter \rho_2) = \lbl(\rho_1 + \rho_2) = \lbl(\rho_1) \cup \lbl(\rho_2).
\end{array}
\]
Then we immediately have:
\begin{lemma}\label{lem:label}
~\hfill
%Let 
%$\rho_1 \equiv \record{l_i:\sigma_i \mid i\in I}$, 
%$\rho_2 \equiv \record{l_j:\sigma'_j \mid j\in J}$, 
%$\rho_3 \equiv \record{l_k:\sigma''_k \mid k\in K}$. Then:
\begin{enumerate}
%\item \label{lem:basicEq}
%	$\rho_1 + \rho_2 = \record{l_i:\sigma_i, l_j:\sigma'_j \mid i\in I\setminus J, j\in J}$,
\item \label{lem:lem:label-a}
	$\rho_1 = \rho_2 \Then \lbl(\rho_1) = \lbl(\rho_2)$,
\item \label{lem:label-b}
	$\lbl(\rho_1) \cap \lbl(\rho_2) = \emptyset \Then \rho_1 + \rho_2 = \rho_1 \inter \rho_2$.
\end{enumerate}
\end{lemma}

In (\ref{lem:lem:label-a}) above $\rho_1 = \rho_2$ is $\rho_1 \leq \rho_2 \leq \rho_1$. About (\ref{lem:label-b}) note that 
condition $\lbl(\rho_1) \cap \lbl(\rho_2) = \emptyset$ is essential, since
$\rho_1 + \rho_2 \neq \rho_2 + \rho_1$ in general, as it immediately follows by
Lemma \ref{lem:normal-recordTypes}.\ref{lem:normal-recordTypes-b}.

\begin{lemma}\label{lem:typePlus}
~\hfill
%Let 
%$\rho_1 \equiv \record{l_i:\sigma_i \mid i\in I}$, 
%$\rho_2 \equiv \record{l_j:\sigma'_j \mid j\in J}$, 
%$\rho_3 \equiv \record{l_k:\sigma''_k \mid k\in K}$. Then:
\begin{enumerate}
%\item \label{lem:basicEq}
%	$\rho_1 + \rho_2 = \record{l_i:\sigma_i, l_j:\sigma'_j \mid i\in I\setminus J, j\in J}$,
\item \label{lem:typePlus-ass}
	$(\rho_1 + \rho_2) + \rho_3 = \rho_1 + (\rho_2 + \rho_3)$,
\item \label{lem:typePlus-distr}
	$(\rho_1\inter\rho_2)+\rho_3 = (\rho_1+\rho_3)\inter(\rho_2+\rho_3)$,
\item \label{lem:typePlus-mon}
	$\rho_1 \leq \rho_2 \Rightarrow \rho_1+\rho_3 \leq \rho_2+\rho_3$,
\item \label{lem:typePlus-inter}
	$\rho_1 + \rho_2 = \rho_1\inter\rho_2 \Iff  \rho_1+\rho_2 \leq \rho_1$.
\end{enumerate}
\end{lemma}

\begin{remark}
%In view of (\ref{lem:normal-recordTypes-b}) of Lemma \ref{lem:normal-recordTypes}, $\rho_1 + \rho_2 \neq \rho_2 + \rho_1$
In general $\rho_1+(\rho_2\inter\rho_3) \neq (\rho_1+\rho_3)\inter(\rho_2+\rho_3)$: take $\rho_1\equiv \record{l_1:\sigma_1, l_2:\sigma_2}$,
$\rho_2 \equiv \record{l_1:\sigma'_1}$ and $\rho_3 \equiv \record{l_2:\sigma'_2}$, with $\sigma_1 \neq \sigma'_1$ and $\sigma_2 \neq \sigma'_2$. Then we have:
$\rho_1+(\rho_2\inter\rho_3) = \rho_1 + \record{l_1:\sigma'_1, l_2:\sigma'_2} = \record{l_1:\sigma'_1, l_2:\sigma'_2}$,
while
$ (\rho_1+\rho_3)\inter(\rho_2+\rho_3) = \record{l_1:\sigma_1, l_2:\sigma'_2} \inter\record{l_1:\sigma'_1, l_2:\sigma_2} =
\record{l_1:\sigma_1 \inter \sigma'_1, l_2:\sigma_2 \inter\sigma'_2}.
%\record{m:\sigma_1\inter\sigma_3, n:\sigma_2\inter\sigma_4}.
$
The last example suggests that $(\rho_1+\rho_3)\inter(\rho_2+\rho_3) \leq \rho_1+(\rho_2\inter\rho_3)$.
On the other hand $\rho_2\leq\rho_3 \not\Rightarrow \rho_1+\rho_2 \leq \rho_1+\rho_3$. Indeed: 
\[
\begin{array}{llll}
\record{l_0:\sigma_1,l_1:\sigma_2} + \record{l_1:\sigma_3, l_2:\sigma_4} & = & \record{l_0:\sigma_1,l_1:\sigma'_1, l_2:\sigma_2} \\
& \not\leq &  \record{l_0:\sigma_0,l_1:\sigma_1, l_2:\sigma_2} & \mbox{if $\sigma'_1\not\leq\sigma_1$}\\
& = & \record{l_0:\sigma_1,l_1:\sigma_2} + \record{l_2:\sigma_4}
\end{array}\]
even if $\record{l_1:\sigma_1, l_2:\sigma_2} \leq \record{l_2:\sigma_2}$.
From this and (\ref{lem:typePlus-mon}) of Lemma \ref{lem:typePlus}, we conclude that $+$ is monotonic in its first argument, but not in its second one.
%The above remarks also account for (\ref{lem:typePlus-inter}) of Lemma \ref{lem:typePlus}.
\end{remark}

\medskip
We come now to the type assignment system. A {\em basis} (also called a context in the literature) 
is a finite set $\Gamma = \Set{x_1:\sigma_n,\ldots, x_n:\sigma_n}$, where the variables $x_i$ are pairwise distinct; we set
$\dom(\Gamma) = \Set{x \mid \exists \, \sigma.~x:\sigma \in\Gamma}$ and we write $\Gamma, x:\sigma$ for $\Gamma\cup\Set{x:\sigma}$
where $x\not\in\dom(\Gamma)$. Then we consider the following extension of the
system in \cite{bcd}, also called {\bf BCD} in the literature.


\begin{definition}[Type Assignment]\label{def:typeAssignment}
The rules of the assignment system are:
\[\small
\begin{array}{ccc }
\prooftree
	x:\sigma \in \Gamma
	\justifies
	\Gamma \deduces x:\sigma
	\using (\Axiom)
\endprooftree 
&
\prooftree
	\Gamma, x:\sigma \deduces M: \tau
	\justifies
	\Gamma \deduces \lambda x.M: \sigma\to\tau
	\using (\ArrI)
\endprooftree 
&
\prooftree
	\Gamma \deduces M: \sigma\to\tau \qquad \Gamma \deduces N:\sigma
	\justifies
	\Gamma \deduces MN:\tau
	\using (\ArrE)
\endprooftree 

\\ [6mm]

\prooftree
	\Gamma \deduces M: \sigma \quad 
	\Gamma \deduces M: \tau
	\justifies
	\Gamma \deduces M: \sigma\inter\tau
	\using (\inter)
\endprooftree 
&
\prooftree
	\justifies
	\Gamma \deduces M:\omega
	\using (\omega)
\endprooftree 
&
\prooftree
	\Gamma \deduces M: \sigma \qquad \sigma\leq\tau
	\justifies
	\Gamma \deduces M : \tau
	\using (\leq)
\endprooftree 


\\ [6mm]

\prooftree
	\justifies
	\Gamma \deduces \record{ l_i = M_i \ | \ i \in I } : \record{}
	\using (\record{})
\endprooftree 

&

\prooftree
	\Gamma \deduces M_k: \sigma \quad 
	k \in I
	\justifies
	\Gamma \deduces \record{ l_i = M_i \ | \ i \in I } : \Pair{l_k: \sigma}
	\using (\recRule)
\endprooftree 
&
\prooftree
	\Gamma \deduces M: \Pair{l:\sigma}
	\justifies
	\Gamma \deduces M.l : \sigma
	\using (\selRule)
\endprooftree 
%&
%\prooftree
%	\Gamma \deduces M: \rho_1 \qquad \Gamma \deduces R: \rho_2 \quad (*)
%	\justifies
%	\Gamma \deduces M \oplus R : \rho_1 + \rho_2
%	\using (+)
%\endprooftree 

\\ [6mm]

\multicolumn{3}{c}{
\prooftree
	\Gamma \deduces M: \rho_1 \qquad \Gamma \deduces R: \rho_2 \quad (*)
	\justifies
	\Gamma \deduces M \oplus R : \rho_1 + \rho_2
	\using (+)
\endprooftree 
}

\end{array}\]
where $(*)$ in rule $(+)$ is the side condition: $\lbl(R) = \lbl(\rho_2)$. %$\lbl(R)\cap\lbl(\rho_1)=\emptyset$.
\end{definition}
%\noindent
%Record types express just partial information about the fields in a record, as it is apparent from rule $(\recRule)$;  typings of more than one field in a record are obtained by means of intersection (see below). Rule $(\selRule)$ is the expected one. Rules $(\oplus_l)$ and $(\oplus_r)$ are not symmetric because of the restriction
%$a \not\in \lbl(R)$ in the last rule. Such a restriction is essential for the soundnes of the system, in particular for subject reduction to hold. E.g. we have
%$
%\record{a=M} \Override \record{a=N} \reduces \record{a=N}
%$; now supposing that $M:\sigma$, $N:\tau$ where $\sigma\neq\tau$ and that $\sigma$ cannot be assigned to $N$, without the restriction we could type
%$\record{a=M} \Override \record{a=N}$ by $\Pair{a:\sigma}$ and even by $\Pair{a:\sigma}\inter\Pair{a:\tau} = \Pair{a:\sigma\inter\tau}$, but
%we couldn't type  $\record{a=N}$ by neither of these types.


\noindent
Using Lemma \ref{lem:normal-recordTypes}.\ref{lem:normal-recordTypes-a}, the following rule is easily shown to be admissible:
\[
\prooftree
	\Gamma \deduces M_j:\sigma_j \qquad \forall j \in J \subseteq I
	\justifies
	\Gamma \deduces \record{l_i=M_i \mid  i\in I}: \Pair{l_j:\sigma_i \mid  j\in J} 
\using (\recRule')
\endprooftree 
\]
Contrary to this, the side condition $(*)$ of rule $(+)$ is equivalent to ``exact'' record typing in \cite{BrachaC90}, disallowing record subtyping in width.
Such a condition is necessary for soundness of typing.
%; indeed without such a restriction and combining with rule $(\leq)$ and Lemma \ref{lem:normal-recordTypes}.\ref{lem:normal-recordTypes-b} we could derive
%\[
%\prooftree
%	\Gamma \der M : \Pair{l_i:\sigma_i\ \mid i \in I} \qquad 
%	\Gamma \der R : \Pair{l_j:\tau_j \mid  j \in J}
%\justifies
%	\Gamma \der M \Override R :  \record{l_i:\sigma_i , l_j:\tau_j  \mid  i \in I\setminus J, \ j \in J}
%\using (+')
%\endprooftree
%\]
Indeed suppose that $\Gamma\der M_0:\sigma$ and $\Gamma\der M'_0:\sigma'_0$ but $\Gamma \not\der M'_0:\sigma_0$; then without $(*)$
we could derive:
\[
\prooftree
	\Gamma \der \record{l_0=M_0} : \Pair{l_0:\sigma_0} \qquad 
	\Gamma \der \record{l_0=M'_0,l_1:\sigma_1} : \Pair{l_1:\sigma_1}
\justifies
	\Gamma \der \record{l_0=M_0}\Override \record{l_0=M'_0,l:_1:\sigma_1} :  \record{l_0:\sigma_0, l_1:\sigma_1}
\endprooftree
\]
from which we obtain that $\Gamma \der (\record{l_0=M_0}\Override \record{l_0=M'_0,l:_1:\sigma_1}).l_0:\sigma_0$ breaking subject reduction, since
$(\record{l_0=M_0}\Override \record{l_0=M'_0,l:_1:\sigma_1}).l_0 \reduces^* M'_0$. The essential point is that proving that
$\Gamma\der N: \record{l:\sigma}$ doesn't imply that $l'\not\in\lbl(R')$ for any $l'\neq l$, which follows only by the uncomputable (not even r.e.)
statement that $\Gamma\not\der N: \record{l':\omega}$, a negative information. 

%implies the positive information that if $M \reduces^*R'$ for some record $R'$, then $l \in \lbl(R')$, from which we cannot infer that $l'\not\in\lbl(R')$ for any $l'\neq l$, that is a negative information. 


This explains the restriction to record terms as the second argument of $\Override$: 
in fact   allowing $M\Override N$ to be well formed for an arbitrary $N$ we might have $N\equiv x$ in $\lambda x.\,(M\Override x)$.
But extending $\lbl$ to all terms in $\lambdaR$ is not possible without severely limiting the expressiveness of the assignment system. In fact to say that
$\lbl(N) = \lbl(R)$ if $N\reduces^* R$ would make the $\lbl$ function non computable; on the other hand putting $\lbl(x)=\emptyset$, which is the only 
reasonable and conservative choice as we do not know of possible substitutions for $x$ in $\lambda x.\,(M\Override x)$, implies that the latter term
has type $\omega\to\omega = \omega$ at best.

As a final remark, let us observe that we do not adopt exact typing of records in general, but only for typing the right-hand side of $\oplus$-terms, a feature that will be essential when typing mixins.

%\bigskip
%Apparently these rules, and especially rule $(+')$, could replace rules $(\recRule)$ and $(+)$ in the system, making the $+$ type constructor unnecessary.
%However we observe that the record type notation is an abbreviation, and not a primitive concept of our system. 
%Suppose we had dropped the $+$ constructor from the definition of $\TTR$. Since $M \Override N$ is morally a record, we should
%provide a way to deduce $M\Override N:\record{l:\sigma}$ given some assumptions about the typings of $M$ and $N$. Now
%if $N:\record{l:\sigma}$ then we also expect that $M\Override N:\record{l:\sigma}$, but what about typing this last term when we just know that
%$M:\record{l:\sigma}$? In fact in this case, for the typing $M\Override N:\record{l:\sigma}$ to be sound
%it must be ensured that for all $\rho$ such that $N:\rho$, it is $l\not\in\lbl(\rho)$, that is a negative and infintary condition. 
%Since $N$ needs not to be a record, as for example when $N\equiv x$ in $M\Override x$, we cannot inspect into $N$ to guess the set of labels 
%of its possible types.



%\medskip
%If $\Gamma$ is a basis, $x$ a variable and $\sigma$ a type, we write $\Gamma(x) = \sigma$ if either
%$x:\sigma \in \Gamma$ or $x\not\in\dom(\Gamma)$ and $\sigma = \omega$. 


\begin{lemma}\label{lem:generation}
Let $\sigma \neq \omega$:
\begin{enumerate}

\item\label{gen:var} 
	$\Gamma \deduces x: \sigma  \iff  \exists \tau.\; x:\tau \in \Gamma \And \tau \leq \sigma$,

\item\label{gen:fun} 
	$\Gamma \deduces \lambda x.M : \sigma \iff  
	\exists \; I, \sigma_i, \tau_i.~~\Gamma, x: \sigma_i \deduces M: \tau_i \And
	\bigcap_{i\in I}  \sigma_i \arrow \tau_i \leq \sigma$,

\item\label{gen:app} 
	$\Gamma \deduces MN : \sigma  \iff  \exists \; \tau.~
	\Gamma \deduces M:\tau\to\sigma \And \Gamma \deduces N:\tau$,
	
\item\label{gen:rec} 
	$\Gamma \deduces \record{l_i=M_i \mid  i\in I}: \sigma  \iff
	\forall i \in I \ \exists \; \sigma_i.\; \Gamma \deduces M_i:\sigma_i \And \record{l_i:\sigma_i \mid i\in I} \leq \sigma$,

\item\label{gen:sel} 
	$\Gamma \deduces M.l: \sigma  \iff \Gamma \deduces M: \record{l:\sigma}$,

\item\label{gen:merge}
	 $\Gamma \deduces M \oplus R : \sigma  \iff 
	 \exists \: \rho_1,\rho_2.~ \Gamma \deduces M:\rho_1 \And \Gamma \deduces R: \rho_2 \And
	 \lbl(R)  = \lbl(\rho_2) \And \rho_1+\rho_2 \leq \sigma$.
\end{enumerate}
\end{lemma}

\begin{theorem}[Subject reduction]\label{thm:subjectRed}
$\Gamma \deduces M:\sigma \And M \reduces N \Then \Gamma \deduces N:\sigma$.
\end{theorem}

\input{proof_SBR}
%\input{int-class-mixin}

