%!TEX root = submission.tex

Starting with Cardelli's pioneering work \cite{Cardelli84}, various typed $\lambda$-calculi extended with records have been thoroughly studied to model sophisticated features of object-oriented programming languages, like recursive objects and classes, object extension, method overriding and inheritance (see e.g.~\cite{AbadiCardelli,BruceBook,KiselyovLaemmel05}).

Here, we focus on the synthesis of mixin compositions. In the object-oriented paradigm, mixins \cite{BrachaThesis,BrachaC90} have been introduced as an alternative construct for code reuse that improves over the limitations of multiple inheritance, e.g. connecting incompatible base classes and semantic ambiguities caused by the diamond problem. 
%Mixins can be understood as class transformers that take classes as an argument and return a modified class by adding or modifying methods and fields. These modifications are compositionally expressible by applications of a record merge operator,
%intrdouced by Wand \cite{Wand89} and Remy \cite{Remy89}, and presented in a simpler form by Cook and others in \cite{CookHC90} as ``\textbf{with}''. 
Together with abstract classes and traits, mixins (functions over classes) can be considered as an advanced construct to obtain flexible implementations of module libraries and to enhance code reusability; many popular programming languages miss native support for mixins, but they are an object of intensive study and research (e.g. \cite{BonoPS99,OderskyZ05}). 
In this setting we aim at synthesizing classes from a library of mixins that can be used in programming languages like Java, which do not natively support mixins. Our particular modeling approach is inspired by modern language features (e.g. ECMAScript ``\texttt{bind}'') to preserve contexts in order to prevent programming errors \cite{ecma2011rev51}. 


We formalize synthesis of classes from a library of mixins as an instance of the relativized type inhabitation problem in bounded combinatory logic with intersection types \cite{RehofEtAlCSL12}. Relativized type inhabitation is the decision problem: given a combinatory type context $\Delta$ and a type $\tau$ does there exist an applicative term $e$ such that $e$ has type $\tau$ under the type assumptions in $\Delta$? We denote type inhabitation by $\Delta\,\vdashbcl\,?:\tau$ and implicitly include the problem of constructing a term inhabiting $\tau$. 

Relativized type inhabitation, which is undecidable in general \cite{RehofEtAlCSL12}, is decidable in $k$-bounded combinatory logic \bclk, that is, combinatory logic typed with arrow and intersection types of depth at most $k$,
and hence an algorithm for semi-deciding type inhabitation for $\mbox{\bcl} = \bigcup_k \mbox{\bclk}$ can be obtained by iterative deepening over $k$ and solving the corresponding decision problem in \bclk\; \cite{RehofEtAlCSL12}. In the present paper, we 
enable combinatory synthesis of classes via intersection typed mixin combinators. Intersection types \cite{bcd} play an important 
r\^{o}le in combinatory synthesis, because they allow for semantic specification of components and synthesis goals \cite{RehofEtAlCSL12,BessaiDDM14}.

Now, looking at $\Set{C_1:\sigma_1,\ldots,C_p:\sigma_p, M_1:\tau_1,\ldots,M_q:\tau_q } \subseteq \Delta$
as the abstract specification of a library including classes $C_i$ and mixins $M_j$ with interfaces $\sigma_i$ and $\tau_j$ respectively,
and given a type $\tau$ specifying an unknown class, we
may identify the class synthesis problem with the type inhabitation problem $\Delta\,\vdashbcl\,?:\tau$.
To make this feasible, we have to bridge the gap between the expressivity of highly sophisticated type systems used for typing classes and mixins, for instance
$F$-bounded polymorphism used in \cite{CHOM89,CookHC90}, and the system of intersection types from \cite{bcd}.
In doing so, %, in \S \ref{sec:intersection}, 
we move from the system originally presented in \cite{deLiguoro01}, consisting of a type assignment system of intersection and record
types to a $\lambda$-calculus which we enrich here with record merge operation (called ``\textbf{with}'' in  \cite{CookHC90}), to allow for expressive mixin combinators.
The type system is modified by reconstructing record types $\record{l_i:\sigma_i \mid i \in I}$ as intersection of unary record types $\record{l_i:\sigma_i}$, and considering a subtype relation extending the one in \cite{bcd}. This is however not enough for typing record merge, for which we consider a type-merge operator $+$. The problem of typing extensible records and merge, faced for the first time in \cite{Wand91,Remy92}, is notoriously hard; to circumvent difficulties the theory of record subtyping in \cite{CookHC90} (where a similar type-merge operator is considered) allows just for ``exact'' record typing, which involves subtyping in depth, but not in width. Such a restriction, that has limited effects w.r.t. a rich and expressive type system like $F$-bounded polymorphism, would be too severe in our setting. Therefore, we undertake a study of the type algebra of record types with intersection and type-merge, leading to a type assignment system where exact record typing is required only for the right-hand side operand of the term merge operator, which is enough to ensure soundness of typing.

The next challenge is to show that we can type in a meaningful way in our system classes and mixins, where the former are essentially recursive records and the latter are made of a combination of fixed point combinators and record merge. Such combinators, which usually require recursive types, can be typed
in our system by means of an iterative method exploiting the ability of intersection types to represent approximations of the potentially infinite unfolding of recursive definitions. 

The final problem we face is the encoding of intersection types with record types and type-merge into the language of \bcl. For this purpose we
consider a conservative extension of bounded combinatory logic, called $\bclc$, where we allow unary type constructors that are monotonic and distribute over intersection. We show that the (semi) algorithm solving inhabitation for \bcl\ can be adapted to $\bclc$, by proving that the key properties necessary to solve the inhabitation problem in \bcl\ are preserved in $\bclc$ and showing how the type-merge operator can be simulated in $\bclc$. In fact, type-merge is not monotonic in its second argument, due to the lack of negative information caused by the combination of $+$ and $\inter$. Our work culminates in two theorems that ensure soundness and completeness of the so obtained method w.r.t. synthesis
of classes by mixins composition.

\medskip\noindent
{\bf Related works.}
This work evolves  from the contributions \cite{BessaiDDM14, UdLTC2014, besDDTU15} to the workshop \textsf{ITRS'14}. The papers that have inspired our work,
mainly by Cook and others, have been cited above.
The theme of using intersection types and bounded-polymorphism for typing object-oriented languages and inheritance has been treated in \cite{CompagnoniP96,Pierce97}.
%in a framework where bounded-polymorphism is enriched by intersection types. This is orthogonal w.r.t. our approach that, for %technical reasons, is reductionist in nature rather an extension of system $F$.
Type inhabitation has been recently used for synthesis of object oriented code \cite{Gkkp13, Duedder14}, but to our best knowledge the present paper provides, for the first time, a theory of type-safe mixin composition synthesis based on the component-oriented approach of combinatory logic synthesis.

%It is in general harder than type inhabitation, e.g. type inhabitation in simple typed $\lambda$-calculus is \textsc{PSpace}-complete~\cite{statman79}, whereas relativized type inhabitation in the same system is undecidable \cite{PostLinial,RehofUrzyczyn11}. 


%\bigskip
%
%
%
%
%We present intersection types for a $\lambda$-calculus with records that is used to express mixins and classes as functions over recursively defined classes. This system is compositional and minimal in the sense that it introduces as few type constructors as necessary. Such records can be assigned meaningful types without resorting to recursive types. Typed terms are then translated to combinators typed by intersection types with type constructors in $\bclc$, a conservative extension of \bcl. A relation between the record-merge and the intersection operator is shown. \bcl\ is defined as the union of \bclk\ over all type variable substitution depths $k$. An algorithm for semi-deciding type inhabitation for \bcl\ can be obtained by iterative deepening over $k$ and solving the corresponding decision problem in \bclk\cite{RehofEtAlCSL12} for $k$ until a solution is found. Proofs for correctness and partial completeness of the encoding of mixins in $\bclc$ are presented. The framework (CL)S \cite{BDDMR14} implementing the algorithm is available.
%
%Intersection types are a natural choice for our task for three main reasons. First and foremost, the type inhabitation problem for intersection types in \bclk\ is non-trivial and decidable \cite{RehofEtAlCSL12}. Other type systems like many variants of recursive types encompass universal value inhabitation, turning the inhabitation question trivial\footnote{By typing a fixed point combinator $\emptyset \vdash Y : (\sigma \to \sigma) \to \sigma$ and using $\emptyset \vdash Y (\lambda x . x) : \sigma$ to inhabit any $\sigma$}. In fact, type inhabitation can be seen as a logic programming language \cite{JR13}, a fact we exploit to encode predicates simulating type system extensions in the $\lambda$-calculus. Second, associativity, commutativity and subtyping of intersection types are well defined and necessary for compositionality as well as succinct object encodings. Usually, succinctness is only achievable by highly specialized type systems, such as row types \cite{BonoPS99}. 
%Finally, semantic types which are a feature of intersection types \cite{JR13}, are a powerful mechanism for specification. They guide inhabitation by eliminating arbitrariness caused by under-specification of behavior in implementation types. Their additional specification is formulated using a user-defined vocabulary on an adaptable level of granularity. Semantic types in the context of mixin synthesis have been exploited by (CL)S in past applications \cite{BessaiDDM14} and will be subject to further study.
%
%
%The paper is organized as follows: in Sect.~2, intersection types for a $\lambda$-calculus with records are introduced as a domain specific language for representing mixins and classes. Sect.~3 presents an encoding of record types in $\bclc$. Sect.~4 presents mixin composition synthesis by type inhabitation in $\bclc$. Sect.~5 extends our running example. 
%Sect.~6 provides a conclusion and presents future work.
%
%
%Our main contribution is a theory of compositional construction of object oriented classes by combinatory synthesis. It is made possible by a translation of type system extensions, i.e. records and record-merge, necessary in the $\lambda$-calculus to a purely intersection-typed combinatory system. We prove the translation to be correct and apply it to compose mixins and classes. This work evolves from the contributions \cite{BessaiDDM14, UdLTC2014} to the workshop \textsf{ITRS’14} and the yet to appear post-proceedings.