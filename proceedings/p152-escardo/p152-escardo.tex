\documentclass[a4paper,UKenglish]{lipics}

\usepackage{amsmath, amssymb}
\usepackage{bbm}
\usepackage{url}
\title{The Inconsistency of a Brouwerian Continuity Principle with the Curry--Howard Interpretation}
%\titlerunning{The inconsistency of a continuity principle with the Curry--Howard interpretation}

\author[1]{Mart\'\i{n} H\"otzel Escard\'o}
\author[1]{Chuangjie Xu}

\affil[1]{School of Computer Science, University of Birmingham, UK}

\Copyright{Mart\'\i{n} H\"otzel Escard\'o and Chuangjie Xu}

\subjclass{F.4.1}

\keywords{
Dependent type,
intensional Martin-L\"of type theory,
Curry-Howard interpretation,
constructive mathematics,
Brouwerian continuity axioms,
anonymous existence,
propositional truncation,
function extensionality,
homotopy type theory,
topos theory.
}

\newcommand{\blackboard}[1]{\mathbbm{#1}}
\newcommand{\N}{{\mathbb{N}}}
\newcommand{\two}{\blackboard{2}}
\newcommand{\cantor}{{\two^\N}}
\newcommand{\baire}{{\N^\N}}
\newcommand{\tc}{\hspace{1pt}\mathord{:}\hspace{1pt}}
\newcommand{\Id}{{\operatorname{Id}}}
\newcommand{\refl}{{\operatorname{refl}}}
\newcommand{\trunc}[1]{{\mathord{\parallel}#1\mathord{\parallel}}}
\newcommand{\Trunc}[1]{\lvert #1 \rvert}


\begin{document}

\maketitle 

\volumeinfo{Thorsten Altenkirch}
{1}
{13th International Conference on Typed Lambda Calculi and Applications (TLCA'15)}
{38}
{1}
{152}%{x}
\EventShortName{TLCA'15}%
\DOI{10.4230/LIPIcs.TLCA.2015.152}
\serieslogo{}

\begin{abstract}
  If all functions $(\N\to \N)\to \N$ are continuous then $0=1$. We
  establish this in intensional (and hence also in extensional)
  intuitionistic dependent-type theories, with existence in the
  formulation of continuity expressed as a $\Sigma$ type via the
  Curry-Howard interpretation. But with an intuitionistic notion of
  anonymous existence, defined as the propositional truncation of
  $\Sigma$, it is consistent that all such functions are continuous. A
  model is Johnstone's topological topos. On the other hand, any of
  these two intuitionistic conceptions of existence give the same,
  consistent, notion of uniform continuity for functions $(\N\to
  \two)\to \N$, again valid in the topological topos. It is open
  whether the consistency of (uniform) continuity extends to homotopy
  type theory.  The theorems of type theory informally proved here are
  also formally proved in Agda, but the development presented here is
  self-contained and doesn't show Agda code.
\end{abstract}

\section{Introduction}

We show that a continuity principle that holds in Brouwerian
intuitionistic mathematics becomes false when we move to its
Curry--Howard interpretation. We formulate and prove this in an
intensional version of intuitionistic type theory
(Section~\ref{section:continuity:false}).  Another Brouwerian
(uniform) continuity principle, however, is logically equivalent to
its Curry--Howard interpretation
(Section~\ref{section:uniform:continuity}).  

In order to be able to formulate and prove this logical equivalence,
we need a type theory in which both a formula and its Curry--Howard
interpretation can be expressed
(Section~\ref{section:relationship}). For example, toposes admit both
$\forall,\exists$ (via the subobject classifier) and $\Pi, \Sigma$
(via their local cartesian closedness) and hence qualify. We adopt the
HoTT-book~\cite{hott} approach of working with propositional
truncation $\trunc{-}$ to express $\exists(x:X).A(x)$ as the
propositional truncation of $\Sigma(x:X).A(x)$. This is related to
NuPrl's squash types~\cite{mendler}, Maietti's
mono-types~\cite{maietti98}, and Awodey--Bauer bracket types in
extensional type theory~\cite{awodey:bauer:propositions}. Here by a
proposition we mean a type whose elements are all equal in the sense
of the identity type, as in the HoTT book.  In a topos with identity
types understood as equalizers, the propositions are the truth values
(subterminal objects), and the propositional truncation of an object
$X$ is its support, namely the image of the unique map $X \to 1$ to
the terminal object. This gives the truth value of the inhabitedness
of~$X$, without necessarily revealing an inhabitant of $X$, and we have
that \[ (\exists(x:X).A(x)) = \trunc{\Sigma(x:X).A(x)}.\] In HoTT, 
this is taken as the definition of $\exists$, with
truncation taken as a primitive notion. But we don't (need to) work
with the homotopical understanding of type theory or the univalence
axiom here.

\subsection{The continuity of all functions \texorpdfstring{$\baire \to \N$}{Lg}}

In Brouwerian intuitionistic mathematics, all functions $f : \baire
\to \N$ on the Baire space $\baire = (\N \to \N)$ are
continuous~\cite{beeson:foundations,troelstra:vandalen:constructivism}. This
means that, for any sequence $\alpha : \baire$ of natural numbers, the
value $f\alpha $ of the function depends only on a finite prefix of
the argument $\alpha : \baire$.  If we write $\alpha =_n \beta$ to
mean that the sequences $\alpha$ and $\beta$ agree at their first $n$
positions, a precise formulation of this continuity principle is
\[
\forall(f:\baire \to \N).\ \forall(\alpha : \baire).\ \exists(n :
\N).\ \forall(\beta : \baire).\ \alpha =_n \beta \to f\alpha = f\beta.
\]
It is well known that this statement cannot be proved in higher-type
Heyting arithmetic ($\operatorname{HA}^\omega$), but that is
consistent and validated by the model of Kleene--Kreisel continuous
functionals, and also realizable with Kleene's second combinatory
algebra $K_2$~\cite{beeson:foundations}.

We show that, in intensional Martin-L\"of type theory, the
Curry--Howard interpretation of the above continuity principle is
false: It is a theorem of intensional MLTT, even without universes,
that
\[
\left(\Pi(f:\baire \to \N).\Pi(\alpha : \baire).\Sigma(n :
  \N).\Pi(\beta : \baire).\alpha =_n \beta \to f\alpha = f\beta
\right) \to 0 = 1.
\]
We prove this by adapting Kreisel's well-known argument that, e.g.\ in
$\operatorname{HA}^\omega$, extensionality, choice and continuity are
together impossible~\cite{Kreisel:weak:completness}\cite{troelstra:1977}\cite[page 267]{beeson:foundations}.  The difference
here is that
\begin{enumerate}
\item We work in \emph{intensional} type theory.
\item Choice for the $\Sigma$ interpretation of existence is a theorem
  of type theory.
\end{enumerate}
So what is left to understand is that extensionality is not needed in
Kreisel's argument when it is rendered in type theory
(Section~\ref{section:continuity:false}). 

\nocite{xu:escardo:tlca2013,escardo:xu:kk} The above two versions of
the notion of continuity can be usefully compared by considering the
interpretations of $\operatorname{HA}^\omega$ and MLTT in Johnstone's
\emph{topological topos}~\cite{johnstone:topological}.  The point of
this topos is that it fully embeds a large cartesian closed category
of continuous maps of topological spaces, the sequential topological
spaces, and the larger locally cartesian closed category of Kuratowski
limit spaces~\cite{menni:simpson}.  As discussed above, any topos has
$\exists,\forall, \Sigma, \Pi$ and therefore models both intuitionistic
predicate logic and dependent type theory.  We have that
\begin{enumerate}
\item The formula
  \[
  \forall(f:\baire \to \N).\ \forall(\alpha : \baire).\ \exists(n :
  \N).\ \forall(\beta : \baire).\ \alpha =_n \beta \to f\alpha =
  f\beta
  \]
  is true in the topological topos.

  ~

  The informal reading of this is ``all functions $\baire \to \N$ are
  continuous''.

  ~

\item There is a function
  \[
  (\Pi(f \colon \baire \to \N). \Pi(\alpha : \baire). \Sigma(n : \N).
  \Pi(\beta : \baire). \alpha =_n \beta \to f \alpha = f \beta) \to
  0=1
  \]
  in the topological topos, or indeed in any topos whatsoever, by our
  version of Kreisel's argument.

  ~

  The informal reading of this is ``not all functions $\baire \to \N$
  are continuous''.
\end{enumerate}
But there is no contradiction in the formal versions of the above
statements: they simultaneously hold in the same world, the
topological topos.
From a hypothetical inhabitant of
\[
\Pi(f \colon \baire \to \N). \Pi(\alpha : \baire). \Sigma(n : \N).
  \Pi(\beta : \baire). \alpha =_n \beta \to f \alpha = f \beta
\]
we get a
modulus-of-continuity functional
\[
M : (\baire \to \N) \times \baire \to \N, 
\]
by projection (rather than by choice in the topos-logic sense), which
gives a modulus of continuity $n = M(f,\alpha)$ of the function $f :
\baire \to \N$ at the point~$\alpha : \baire $.  Kreisel's argument
derives a contradiction from the existence of $M$.  What this shows,
then, is that although every function \emph{is} continuous, there is
no continuous way of finding a modulus of continuity of a given
function $f$ at a given point~$\alpha$. There is no continuous
$M$. Perhaps the difference between the seemingly contradictory
statements becomes clearer if we formulate them type theoretically
with and without propositional truncation. In the topological topos,
the object
\[
\Pi(f : \baire \to \N).\ \Pi(\alpha : \baire).\ \trunc{\Sigma(n :
  \N).\ \Pi(\beta : \baire).\ \alpha =_n \beta \to f\alpha = f\beta }
\]
is inhabited, but
\[ 
\Pi(f : \baire \to \N).\ \Pi(\alpha : \baire).\ \Sigma(n : \N).\
\Pi(\beta : \baire).\ \alpha =_n \beta \to f\alpha = f\beta
\]
is not.
 
\subsection{The uniform continuity of all functions \texorpdfstring{$\cantor \to \N$}{Lg}}
The above situation changes radically when we move from the Baire
space to the Cantor space, and from continuous functions to uniformly
continuous functions.  Another Brouwerian continuity principle is that
all functions from the Cantor space $\cantor = (\N \to \two)$ to the
natural numbers are uniformly continuous:
\[
\forall(f \colon \cantor \to \N) .\ \exists (n \colon \N) .\
\forall(\alpha, \beta \colon \cantor).\ \alpha =_n \beta \to f\alpha =
f\beta.
\]
Again this is not provable in $\operatorname{HA}^\omega$ but
consistent and validated by the model of continuous functionals, by
realizability over $K_2$, and by the topological topos. We have also
constructively developed a model analogous to the topological topos
in~\cite{xu:escardo:tlca2013}.

By the above discussion, the above principle is equivalent to
\[
\Pi(f \colon \cantor \to \N) .\ \trunc{\Sigma (n \colon \N) .\
  \Pi(\alpha, \beta \colon \cantor).\ \alpha =_n \beta \to f\alpha =
  f\beta}.
\]
We show that this, in turn, is logically equivalent to its untruncated
version
\[
\Pi(f \colon \cantor \to \N) .\ \Sigma (n \colon \N) .\ \Pi(\alpha,
\beta \colon \cantor).\ \alpha =_n \beta \to f\alpha = f\beta.
\]
In particular, it follows that this object is inhabited (by a global
point) in the topological topos. Each inhabitant gives, by projection,
a ``fan functional'' $(\cantor \to \N) \to \N$ that continuously
assigns a modulus of uniform continuity to its argument. There is a
canonical one, which assigns the least modulus of uniform continuity.

In order to establish the above logical equivalence, we prove the
following general principle for ``exiting truncations'': If $A$ is a
family of types indexed by natural numbers such that
  \begin{enumerate}
  \item $A(n)$ is a proposition for every $n \colon \N$, and
  \item $A(n)$ implies that $A(m)$ is decidable for every $m<n$,
  \end{enumerate}
then
\[ \trunc{\Sigma(n \tc \N). A(n)} \to \Sigma(n \tc \N). A(n). \]
From anonymous existence one gets explicit existence in this case.

\subsection{A question regarding Church's Thesis}

Troelstra~\cite{troelstra:1977} also shows that extensionality, choice
and Church's Thesis (CT) are together impossible, and
Beeson~\cite[page 268]{beeson:foundations} adapts this argument to
conclude that extensional Martin-L\"of type theory refutes CT, with
existence expressed by~$\Sigma$. But CT with existence expressed as
the truncation of $\Sigma$ is consistent with MLTT, and validated by
Hyland's \emph{effective topos}~\cite{hyland1982effective}. What seems
to be open is whether CT formulated with $\Sigma$ is already refuted
by \emph{intensional}~MLTT (including the $\xi$-rule). This question
has been popularized by Maria Emilia Maietti.


\section{Continuity of functions \texorpdfstring{$\baire \to \N$}{Lg}}
\label{section:continuity:false}

We reason informally, but rigorously, in type theory, where, as above,
we use the equality sign to denote identity types, unless otherwise
indicated. A formal proof, written in
Agda~\cite{bove:dybjer:dependent:types,bove:dybjer:norell:agda,norell:agda},
is available at~\cite{agda:continuity:false:all}, but the development
here is self-contained and doesn't show Agda code.

The following says that the Curry--Howard interpretation of ``all
functions $\baire \to \N$ are continuous'' is false.
\begin{theorem} \label{main}
  \label{theorem:continuity:false}
  If
  \[
  \Pi(f \colon \baire \to \N) . \Pi(\alpha \colon \baire) . \Sigma(n
  \colon \N) . \Pi(\beta \colon \baire) .\ \alpha =_n \beta \to
  f\alpha = f\beta
  \]
  then $0=1$.
\end{theorem}
We take the conclusion to be $0 = 1$ rather than the empty type
because we are not assuming a universe for the sake of generality.
The argument below gives $0=1$, and, as is well known, to get to the
empty type from $0=1$ a universe is needed.
\begin{proof}
  Let $0^\omega$ denote the infinite sequence of zeros, that is, $\lambda i.0$,
  and let $ 0^n k^\omega$ denote the sequence of $n$ many zeros followed by
  infinitely many $k$'s.  Then
  \[ (0^n k^\omega) =_n 0^\omega \quad\text{and}\quad (0^n k^\omega)(n) = k.\]
  Assume $ \Pi(f \colon \baire \to \N). \Pi(\alpha \colon \baire). \Sigma(n
  \colon \N). \Pi(\beta \colon \baire). \alpha=_n \beta \to f\alpha  =
  f\beta .  $ By projection, with $\alpha = 0^\omega$, this gives a
  modulus-of-continuity function
  \[
  M \colon (\baire \to \N) \to \N
 \]
 such that
 \begin{equation}
   \label{eq:0}
   \Pi(f \colon \baire \to \N). \Pi(\beta \colon \baire). 0^\omega =_{M f} \beta \to f(0^\omega) = f\beta .    
 \end{equation}
 We use $M$ to define a function $f \colon \baire \to \N$ such that
 $M(f)$ cannot be a modulus of continuity of $f$ and hence get a
 contradiction. Let
 \[
 m = M(\lambda \alpha. 0),
 \]
 and define $f \colon \baire \to \N$ by
 \[
 f \beta = M(\lambda \alpha. \beta(\alpha m)).
 \]
 The crucial observation is that, by simply expanding the definitions,
 we have the judgemental equalities
 \[
 f(0^\omega) = M(\lambda \alpha. 0^\omega(\alpha m)) = M(\lambda \alpha. 0)  = m,
 \]
 because $0^\omega(\alpha m)=0$.  
 By the defining property (\ref{eq:0}) of $M$, and the crucial observation,
 \begin{equation}
   \label{eq:1}
   \Pi(\beta \colon \baire). 0^\omega =_{ M f } \beta \to m = f \beta. 
 \end{equation}
 For any $\beta \colon \baire$, by the continuity
 of $\lambda \alpha. \beta(\alpha m)$, by the definition of $f$, and by the
 defining property (\ref{eq:0}) of $M$, we have that
 \[
 \Pi(\alpha \colon \baire). 0^\omega =_{f \beta} \alpha \to \beta 0 =
 \beta(\alpha m).
 \]
 If we choose $\beta = 0^{M f+1} 1^\omega$, we have $0^\omega =_{M f+1} \beta$, and so $0^\omega =_{M f} \beta$, and hence $f(\beta) =
 m$ by~(\ref{eq:1}). This gives
 \[
 \Pi(\alpha \colon \baire). 0^\omega =_m \alpha \to \beta 0 = \beta(\alpha m).
 \]
 Considering $ \alpha = 0^m (M f+1)^\omega$, we have $0^\omega =_m \alpha$,
 and therefore \[ 0 = \beta 0 = \beta(\alpha m) = \beta(M f +1) = 1.\]  
\end{proof}


\begin{remark}[Thomas Streicher, personal communication]
The conversion
 \[
 f(0^\omega) = M(\lambda \alpha. 0^\omega(\alpha m)) = M(\lambda \alpha. 0)  = m
 \]
 in the above proof relies on the $\xi$-rule (reduction under
 $\lambda$), which is not available in a system based on the
 combinators $S$ and $K$ rather than the $\lambda$-calculus. Usually
 $\operatorname{HA}^\omega$ is taken in combinatory form, in which
 case one needs some form of extensionality to conclude that
 $f(0^\omega) = m$, and this explains how we avoid the extensionality
 hypothesis in Kreisel's original argument. But notice that the
 $\xi$-rule holds in categorical models.
\end{remark}
Therefore the argument of the above proof shows that:
\begin{theorem}
  In $\operatorname{HA}^\omega$, the $\xi$-rule, the axiom of choice,
  and the continuity of all functions $\baire \to \N$ are together
  impossible.
\end{theorem}

Another observation, offered independently by Thorsten Altenkirch,
Thierry Coquand and Per Martin-L\"of (personal communication), is that
the continuity of a function $\baire \to \N$ implies that it is
extensional in the sense that it maps pointwise equal arguments to
equal values, and so the continuity axiom has some amount of
extensionality built into it.

The above formulation and proof of Theorem~\ref{main} assumes natural
numbers, identity types, $\Pi$ and $\Sigma$ types, and no
universes. But it uses only the identity type of the natural numbers.
If we assume a universe $U$, this identity type doesn't need to be
assumed, because it can be defined by induction. We first define a
$U$-valued equality relation, where $\blackboard{O}$ is the empty type
and~$\blackboard{1}$ is the unit type with element $\star$,
\begin{gather*}
  (0 = 0)  =  \blackboard{1}, \quad
  (m+1 = 0)  = 
  (0 = n+1)  =  \blackboard{O}, \quad
  (m+1 = n+1)  =  (m = n).
\end{gather*}
Then we define $\refl : \Pi(n : \N).n = n$ by induction as
\begin{gather*}
  \refl(0) = \star, \qquad \refl(n+1)=\refl(n),
\end{gather*}
and 
$
J : \Pi(A : \Pi (m, n).m = n \to U).
      (\Pi n. A \, n \, n \, (\refl(n))) \to \Pi m, n, p. A \, m \, n \, p
$
by
\[
\begin{array}{llllllll}
J & A & r & 0 & 0 & \star & = & r \, 0, \\
J & A & r & (m+1) & 0 & p & = & \operatorname{\blackboard{O}-rec}\,(A\, (m+1) \, 0) \, p, \\
J & A & r & 0 & (n+1) & p & = & \operatorname{\blackboard{O}-rec}\,(A\, 0 \,\,\, (n+1)) \, p, \\
J & A & r & (m+1) & (n+1) & p & = & J \, (\lambda m n. A(m+1) (n+1)) \, (\lambda n.r(n+1))\, m \, n \, p.
\end{array}
\]
where $\operatorname{\blackboard{O}-rec} : \Pi(X:U). \blackboard{O} \to X$
is the recursion combinator of the empty type.  The usual computation
rule, or judgemental equality, for $J$ when it is given as primitive
doesn't hold here, but the above $J$ is enough to define transport
(substitution) and hence symmetry, transitivity and application
(congruence), which are enough to carry out the above proof formally
(and we have checked this in
Agda~\cite{agda:continuity:false:all}). Hence the theorem and its
proof can be expressed in a type theory without a primitive equality
type. All is needed to formulate and prove Theorem~\ref{main} is a
type theory with $\blackboard{O},\blackboard{1},\N,\Pi,\Sigma,U$.



\section{Propositional truncation and existential quantification}
\label{section:relationship}

\newcommand{\isprop}{\operatorname{isProp}} 

We recall the notion of propositional truncation from the HoTT book
and use it to define the quantifiers $\exists,\forall$, in a slightly
different way from that in the HoTT book, so that they satisfy the
Lawvere's adjointness conditions that correspond to their
intuitionistic introduction and elimination rules.
Another difference is that, instead of adding propositional
truncations for all types to our type theory, we define what a
propositional truncation for a given type is. For some types, their
propositional truncation already exist, including the types needed in
our discussion of uniform continuity in
Section~\ref{section:uniform:continuity}.

\subsection{Propositional truncation}

We adopt the terminology
of the HoTT book, which clashes with the terminology of the
Curry--Howard interpretation of (syntactical) propositions as types.
For us, a \emph{proposition} is a subsingleton, or a type whose
elements are all equal, in the sense of the identity type, here
written ``$=$'' again as in the HoTT book:
\[
\isprop X = \Pi(x, y \colon X). x = y.
\]
Perhaps a better terminology, compatible with that of topos theory,
would be \emph{truth value}, in order to avoid the clash. But we will
stick to the terminology \emph{proposition}, and occasionally use
\emph{truth value} synonymously, for emphasis.


A propositional truncation of a type $X$, if it exists, is a
proposition $\trunc{X}$ together with a map $\Trunc{-} : X \to
\trunc{X}$ such that for any proposition $P$ and $f : X \to P$ we can
find $\bar{f}:\trunc{X}\to P$. Because $P$ is a proposition, this map $\bar{f}$
is automatically unique up to pointwise equality, and we have
$\bar{f}\Trunc{x}=f(x)$, and hence a propositional truncation is a
reflection in the categorical sense, giving a universal map of $X$
into a proposition. This can also be understood as a recursion
principle, or elimination rule,
\[
\isprop P \to (X \to P) \to \trunc{X} \to P,
\]
for any types $P$ and $X$. The induction principle, in this case, can
be derived from the recursion principle, but in practice it is seldom
needed.

In HoTT, propositional truncations for all types are given as
higher-inductive types, with the judgemental equality
$\bar{f}\Trunc{x}=f(x)$. From the existence of the truncation of the
two-point type $\blackboard{2}$ with this judgemental equality, one can
prove function extensionality (any two pointwise equal functions are
equal)~\cite{KECA:anonymous:existence}.  The assumption that
$\trunc{X} \to X$ for every type~$X$ gives a constructive taboo (and
also contradicts univalence)~\cite{KECA:hedberg:theorem}.

However, for some types $X$, not only can a propositional truncation
$\trunc{X}$ be constructed in MLTT, but also there is
a map $\trunc{X} \to X$:
\begin{enumerate}
\item If $P=\blackboard{O}$ or $P=\blackboard{1}$, or more generally if $P$
  is any proposition, we can take $\trunc{P}=P$, of course. In
  particular, if $X \to \blackboard{O}$, we can take $\trunc{X}=X$; but
  also we can take $\trunc{X}=\blackboard{O}$, even though we can't say
  $X=\blackboard{O}$ without univalence.

\item If we have an inhabitant of $X$ then we can take
  $\trunc{X}=\blackboard{1}$. The map $\trunc{X} \to X$ simply picks the
  given inhabitant.

\item More generally, if $X$ is logically equivalent to a proposition
  $P$, then we can take $\trunc{X}=P$, and we make profitable use of this simple fact.

\item If $X$ is any type and $g:X \to X$ is a constant map in the
  sense that any two of its values are equal, we can take $\trunc{X}$
  to be the type $\Sigma(x:X).g(x) = x$ of fixed points of $g$,
  together with the function $X \to \trunc{X}$ that maps $x$ to
  $(g(x),p)$, where $p$ is an inhabitant of the type $g(g(x)) = g(x)$
  coming from the constancy witness~\cite{KECA:hedberg:theorem}. In
  this case the first projection gives a map $\trunc{X} \to X$. Given
  a map $f : X \to P$ into a proposition, we let $\bar{f} : \trunc{X}
  \to P $ be the first projection followed by $f: X \to P$ (and we
  don't use the fact that $P$ is a proposition).

\item For any $f : \N \to \N$, the type $\Sigma(n : \N).f(n)=0$, which
  may well be empty, has a constant endomap that sends $(n,p)$ to
  $(n',p')$, where we take the least $n'\le n$ with $p':f(n')=0$,
  using the decidability of equality of $\N$ and bounded search. Hence
  not only $\trunc{\Sigma(n : \N).f(n)=0}$ exists, but also
  $\trunc{\Sigma(n : \N).f(n)=0} \to \Sigma(n : \N).f(n)=0$.
\end{enumerate}

\subsection{Quantification}

\newcommand{\Prop}{\operatorname{Prop}}
For a universe $U$, let $\Prop$ be the type of propositions in $U$:
\[
\Prop = \Sigma(X : U).\isprop X.
\]
If we assume that all types in $U$ come with
designated propositional truncations, then we have a reflection 
\[
r : U \to \Prop
\]
that sends $X:U$ to the pair $(\trunc{X},p)$ with $p :
\isprop\trunc{X}$ coming from the assumption. In the other direction, we have an embedding
\[
s : \Prop \to U,
\]
given by the projection. For $X : U$ we have
\[
s(r(X)) =\trunc{X}.
\]
(We also have that $s$ is a section of $r$ if propositional univalence
holds.)  For a fixed type $X:U$, the type constructors $\Sigma$ and
$\Pi$ can be regarded as having type
\[
\Sigma, \Pi : (X \to U) \to U.
\]
We define
\[
\exists, \forall : (X \to \Prop) \to \Prop,
\]
by, for any $A : X \to \Prop$,
\[
  \exists(A)  =  r(\Sigma(s \circ A)), \quad
  \forall(A)  =  r(\Pi(s \circ A)),
\]
which we also write more verbosely as
\begin{eqnarray*}
  (\exists(x:X).A(x)) & = & r(\Sigma(x :X). s(A(x))), \\
  (\forall(x:X).A(x)) & = & r(\Pi(x :X). s(A(x))).
\end{eqnarray*}
This is essentially the same as the definition in the HoTT book,
except that we give different types to $\exists,\forall$. With the
type given in the book, $\forall$ gets confused with $\Pi$, because,
with function extensionality, a product of propositions is a
proposition, and so there is no need to distinguish $\forall$ from $\Pi$ in the book.

The point of choosing the above types is that now it is easy to
justify that these quantifiers do satisfy the intuitionistic rules for
quantification.  It is enough to show that they satisfy Lawvere's
adjointness conditions. For $P,Q : \Prop$, define
\[
(P \le Q) = (s(P) \to s(Q)).
\]
This is a pre-order (and a partial order if propositional univalence
holds).  Now endow the function type $(X \to \Prop)$ with the
pointwise order (using $\Pi$ to define it). Then the quantifiers
$\exists,\forall : (X \to \Prop) \to \Prop$ are the left and right
adjoints to the exponential transpose $\Prop \to (X \to \Prop)$ of the
projection
\begin{eqnarray*}
  \Prop \times X \to \Prop, 
\end{eqnarray*}
using the universal property of truncation.  The exponential transpose
maps $P$ to $\lambda x.P$. Hence the adjointness condition for the
existential quantifier amounts to
\begin{eqnarray*}
\exists(A) \le P & \iff & A \le \lambda x.P.
\end{eqnarray*}
Expanding the definitions, this amounts to
\begin{eqnarray*}
\trunc{\Sigma(x :X). s(A(x))} \to s(P) & \iff & \Pi(x:X).s(A(x)) \to s(P), \\
                                       & \iff & (\Sigma(x:X).s(A(x))) \to s(P). 
\end{eqnarray*}
So we need to check that 
\begin{eqnarray*}
\trunc{\Sigma(x :X). s(A(x))} \to s(P) & \iff & (\Sigma(x:X).s(A(x))) \to s(P)
\end{eqnarray*}
holds, but this is the case by the defining property of propositional
truncation. For the sake of completeness, we also check the
adjointness condition
\begin{eqnarray*}
P \le \forall(A) & \iff & \lambda x.P \le A. 
\end{eqnarray*}
For this we need function extensionality (which follows from the
assumption of truncations supporting the judgemental equality
discussed above). By definition, this amounts to
\begin{eqnarray*}
s(P) \to \trunc{\Pi(x :X). s(A(x))} & \iff & \Pi(x:X). s(P) \to s(A(x)). 
\end{eqnarray*}
But 
\begin{eqnarray*}
\Pi(x:X). s(P) \to s(A(x)) & \iff & s(P) \to \Pi(x:X). s(A(x)), 
\end{eqnarray*}
and
so the above is equivalent to
\begin{eqnarray*}
s(P) \to \trunc{\Pi(x :X). s(A(x))} & \iff &s(P) \to \Pi(x:X). s(A(x)).
\end{eqnarray*}
But this again holds by the defining property of truncation, because,
by function extensionality, a product of propositions is a
proposition, and each $s(A(x))$ is a proposition. This explains why
$\forall$ is identified with $\Pi$ in the HoTT book.


Having established that the quantifiers $\exists,\forall : (X \to
\Prop) \to \Prop$ defined from $\Sigma$ and $\Pi$ with truncation (via
the reflection $r : U \to \Prop$) do satisfy the adjointness
conditions corresponding to the introduction and elimination rules of
intuitionistic logic, in practice we prefer to use the notation of the
HoTT book, with $\exists(x:X).A(x)$ defined as
$\trunc{\Sigma(x:X).A(x)}$ for propositionally-valued $A:X \to U$, or
even avoid $\exists$ altogether, and just use truncation explicitly,
as in the next section.



\section{Uniform continuity of functions \texorpdfstring{$\cantor \to \N$}{Lg}}
\label{section:uniform:continuity}

We now compare the untruncated formulation of the uniform continuity principle
\[
\Pi (f \colon \cantor \to \N) .\ \Sigma (n \colon \N) .\ \Pi (\alpha,
\beta \colon \cantor) .\ \alpha =_n \beta \to f\alpha = f\beta
\]
with its truncated version
\[
\Pi (f \colon \cantor \to \N) .\ \trunc{\Sigma (n \colon \N) .\ \Pi
  (\alpha, \beta \colon \cantor) .\ \alpha =_n \beta \to f\alpha =
  f\beta}.
\]
A formal counter-part in Agda of this section is available
at~\cite{agda:continuity:false:all}.

We work in a type theory with $\blackboard{O},\blackboard{1},\two,\N,\Pi,\Sigma,\Id$. 
This time, identity types for types other than $\N$ are needed, but universes are not.
But we need more:
\begin{enumerate}
\item In principle, we would have to assume the presence of
  truncations, for example as defined in the HoTT book and explained in
  the previous section, from which function extensionality
  follows~\cite{KECA:anonymous:existence}.

\item However, in turns out that function extensionality alone
  suffices, because it implies the existence of the propositional
  truncation mentioned above, and hence we can omit propositional
  truncations from our type theory.
  (But it doesn't seem to be possible to remove the
  assumption of function extensionality in the theorem proved here.) 
\end{enumerate}
Hence we don't assume propositional truncations in our type theory.
\begin{theorem} \label{theorem:uc:truncation} 
Assuming function extensionality, for every $f:\cantor \to \N$ the type
\[
\Sigma (n \colon \N) .\ \Pi (\alpha,
  \beta \colon \cantor) .\ \alpha =_n \beta \to f\alpha = f\beta
\]
has a propositional truncation, and the proposition
\[
\Pi (f \colon \cantor \to \N) .\ \trunc{\Sigma (n \colon \N) .\ \Pi
  (\alpha, \beta \colon \cantor) .\ \alpha =_n \beta \to f\alpha =
  f\beta}
\]
is logically equivalent to the type
\[
\Pi (f \colon \cantor \to \N) .\ \Sigma (n \colon \N) .\ \Pi (\alpha,
\beta \colon \cantor) .\ \alpha =_n \beta \to f\alpha = f\beta.
\]
\end{theorem}
\begin{lemma} \label{lemma:non:trivial}
  Function extensionality implies that, for any $f : \cantor \to \N$, the type family
\[
A(n) = \Pi (\alpha, \beta \colon \cantor) .\ \alpha =_n \beta \to
f\alpha = f\beta
\]
satisfies the following conditions:
  \begin{enumerate}
  \item $A(n)$ is a proposition for every $n \colon \N$, and
  \item $A(n)$ implies that $A(m)$ is decidable for every $m<n$,
  \end{enumerate}
\end{lemma}
\begin{proof}
  By Hedberg's Theorem~\cite{hedberg_coherence}, equality of natural
  numbers is a proposition.  Hence so is $A(n)$, because, by function
  extensionality, a product of a family of propositions is a
  proposition.  To conclude that for all $n$, if $A(n)$ holds then
  $A(m)$ is decidable for all $m<n$, it is enough to show that for all
  $n$, (1) $\neg A(n+1)$ implies $\neg A(n)$, and (2) if $A(n+1)$ holds
  then $A(n)$ is decidable. (1) This follows from $A(n) \to A(n+1)$,
  which says that any number bigger than a modulus of uniform
  continuity is also a modulus, which is immediate. (2): For every
  $n$, the type
   \[
  B(n) = \Pi(s \colon \two^n) .\ f(s 0^\omega) = f(s 1^\omega),
  \]
  is decidable, because~$\N$ has decidable equality and finite
  products of decidable types are also decidable.  Now let $n:\N$ and
  assume $A(n+1)$. To show that $A(n)$ is decidable, it is enough to
  show that $A(n)$ is logically equivalent to $B(n)$, because then
  $B(n) \to A(n)$ and $\neg B(n) \to \neg A(n)$ and hence we can
  decide $A(n)$ by reduction to deciding $B(n)$.

  The implication $A(n) \to B(n)$ holds without considering the
  assumption $A(n+1)$.  To see this, assume $A(n)$ and let
  $s:\two^n$. Taking $\alpha = s 0^\omega$ and $\beta = s1^\omega$, we
  conclude from $A(n)$ that $f(s 0^\omega) = f(s 1^\omega)$, which is
  the conclusion of $B(n)$.

  Now assume $A(n+1)$ and $B(n)$. To establish $A(n)$, let
  $\alpha,\beta:\cantor$ with $\alpha =_n \beta$. We need to conclude
  that $f(\alpha)=f(\beta)$.  By the decidability of equality
  of~$\two$, either $\alpha(n) = \beta(n)$ or not.  If $\alpha(n) =
  \beta(n)$, then $\alpha =_{n+1} \beta$, and hence
  $f(\alpha)=f(\beta)$ by the assumption $A(n+1)$.  If $\alpha(n) \ne
  \beta(n)$ , we can assume w.l.o.g.\ that $\alpha(n)=0$ and
  $\beta(n)=1$.  Now take the finite sequence $s = \alpha(0),\alpha(1),\dots, \alpha(n-1) (=
  \beta(0),\beta(1),\dots, \beta(n-1)$).  Then $\alpha =_{n+1} s 0^\omega
  $ and $s1^\omega =_{n+1} \beta$, which together with $A(n+1)$ imply
  $f(\alpha)=f(s0^\omega)$ and $f(s1^\omega)=f(\beta)$. But
  $f(s0^\omega)=f(s1^\omega)$ by $B(n)$, and hence
  $f(\alpha)=f(\beta)$ by transitivity. 
\end{proof}

\begin{lemma} \label{lemma:deeply:trivial} \label{truncation:logically}
  If a type $X$ is logically equivalent to a proposition $Q$, then
  \begin{enumerate}
  \item $X$ has the propositional truncation $\trunc{X}=Q$, and
  \item $\trunc{X} \to X$. 
  \end{enumerate}
\end{lemma}
\begin{proof}
  We have $X \to \trunc{X}$ because this is the
  assumption $X \to Q$. If $X \to P$ for some proposition~$P$, then
  also $\trunc{X} \to P$, because this means $Q \to P$, which follows
  from the assumption $Q \to X$ and transitivity of implication. This
  shows that our definition of $\trunc{X}$ has the required property
  for truncations. And $\trunc{X} \to X$ is the assumption that $Q \to
  X$. 
\end{proof}

\begin{lemma}
  \label{lemma:trunc}
  Function extensionality implies that for any family $A$ of types
  indexed by natural numbers such that
  \begin{enumerate}
  \item $A(n)$ is a proposition for every $n \colon \N$, and
  \item $A(n)$ implies that $A(m)$ is decidable for every $m<n$,
  \end{enumerate}
  the type $\Sigma(n : \N). A(n)$ is logically equivalent to the proposition
  \[
  P = \Sigma(k : \N). B(k)
  \]
  where \[ B(k)=A(k) \times \Pi(i:\N). A(i) \to k \le i.\]
\end{lemma}
\begin{proof}
  By function extensionality, the product of a family of propositions
  is a proposition, and hence the type $\Pi(n:\N). A(n) \to k \le n$
  is a proposition, because the type $k \le n$ is a
  proposition. Because the product of two propositions is a
  proposition, the type $B(k)$ is a proposition. But now if $B(k)$ and
  $B(k')$ then, by construction, $k=k'$. Hence any two inhabitants of
  $P$ are equal, using the fact that $B(k)$ is a proposition, which
  means that $P$ is indeed a proposition.  By projection, $P \to
  \Sigma(n : \N). A(n)$. Conversely, if we have $(n,a) : \Sigma(n :
  \N). A(n)$, then we can find, by the decidability of $A(m)$
  for $m<n$, the minimal $k$ such that there is $b : A(k)$, by search
  bounded by $n$, and this gives an element $(k,b,\mu):P$ where $\mu:
  \Pi(i:\N). A(i) \to k \le i$ is the minimality witness. This shows
  that $\Sigma(n : \N). A(n) \to P$ and concludes the proof.
\end{proof}

\begin{remark}
Function extensionality in the above lemma can be avoided using the
fact that the type of fixed points of a constant endomap is a
proposition~\cite{KECA:hedberg:theorem}, where a map is constant if
any two of its values are equal.  Given $(n,a) \colon \Sigma(n \tc
\N). A(n)$, we know that $A(m)$ is decidable for all $m<n$ and thus
can find the minimal~$m$ such that $A(m)$, by search bounded by $n$,
which gives an endomap of $\Sigma(n \tc \N). A(n)$.  This map is
constant, because any two minimal witnesses are equal, and because
$A(n)$ is a proposition. Then we instead take $P$ to be the type of
fixed points of this constant map.
\end{remark}

By Lemmas~\ref{lemma:non:trivial}, \ref{lemma:deeply:trivial},
and~\ref{lemma:trunc}, for any $f:\cantor \to \N$, the truncation of
the type
\[
\operatorname{UC}(f) = \Sigma (n \colon \N) .\ \Pi
    (\alpha, \beta \colon \cantor) .\ \alpha =_n \beta \to f\alpha =
    f\beta
\]
exists and implies $\operatorname{UC}(f)$, which establishes
Theorem~\ref{theorem:uc:truncation}. \qed
 
Unfolding the above construction of the truncation, the truncated
version of uniform continuity says that there is, using $\Sigma$ to
express existence, a minimal modulus of uniform continuity, making
this use of $\Sigma$ into a proposition, and, by function
extensionality, the statement of uniform continuity into a proposition
too. Then the theorem says that this proposition is logically
equivalent to the existence, using $\Sigma$ again, of some modulus of
uniform continuity. This statement is not a proposition, because any
number bigger than a modulus of uniform continuity is itself a modulus
of uniform continuity.

The situation here is analogous to that of quasi-inverses and
equivalences in the sense of the HoTT book. The type expressing that a
function has a quasi-inverse is not a proposition in general, but it
is equivalent to the type expressing that the function is an
equivalence, which is always a proposition. Hence being an equivalence
is the propositional truncation of having a quasi-inverse.

\section*{Acknowledgements}

The material of this paper was presented at the Institute Henri
Poicar\'e in Paris in June 2014 during the research programme
\emph{Semantics of proofs and certified mathematics} organized by
Pierre-Louis Curien, Hugo Herbelin, Paul-Andr\'e Mellies, at the
workshop \emph{Semantics of proofs and programs} organized by Thomas
Ehrhard and Alex Simpson. We are grateful to them for the invitation
to take part of such a wonderful programme and workshop, and to the
participants, for useful discussions and input. In particular, we
mention Thorsten Altenkirch, Andrej Bauer, Thierry Coquand, Nicolai
Kraus, Per Martin-L\"of, Paulo Oliva, Bas Spitters and Thomas
Streicher.

We also thank the anonymous referees for various suggestions for
improvement, including a simplification of the proof of
Theorem~\ref{main} by removing a case distinction (the updated Agda
files~\cite{agda:continuity:false:all} contain both our original
proof and the simplified proof presented here).

\bibliographystyle{plain}
\bibliography{p08-escardo}

\end{document}

