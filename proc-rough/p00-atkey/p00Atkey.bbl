\begin{thebibliography}{10}

\bibitem{atkey14conservation}
Robert Atkey.
\newblock From parametricity to conservation laws, via {N}oether's theorem.
\newblock In {\em Proc.~POPL 2014}, pages 491--502, 2014.

\bibitem{birkedal2005categorical}
Lars Birkedal and Rasmus~E M{\o}gelberg.
\newblock Categorical models for {A}badi and {P}lotkin's logic for
  parametricity.
\newblock {\em Mathematical Structures in Computer Science}, 15(04):709--772,
  2005.

\bibitem{brown-spencer}
Ronald Brown and Christopher~B Spencer.
\newblock G-groupoids, crossed modules and the fundamental groupoid of a
  topological group.
\newblock {\em Proc.~Indag. Math.}, 79(4):296--302, 1976.

\bibitem{buckingham1914physically}
Edgar Buckingham.
\newblock On physically similar systems; illustrations of the use of
  dimensional equations.
\newblock {\em Physical Review}, 4(4):345--376, 1914.

\bibitem{curien-garner-hofmann}
P-L Curien, Richard Garner, and Martin Hofmann.
\newblock Revisiting the categorical interpretation of dependent type theory.
\newblock {\em Theoret. Comput. Sci.}, 546:99--119, 2014.

\bibitem{erwig2002adding}
Martin Erwig and Margaret Burnett.
\newblock Adding apples and oranges.
\newblock In {\em Practical Aspects of Declarative Languages}, pages 173--191.
  Springer, 2002.

\bibitem{hermida1999some}
Claudio Hermida.
\newblock Some properties of {F}ib as a fibred 2-category.
\newblock {\em Journal of Pure and Applied Algebra}, 134(1):83--109, 1999.

\bibitem{house1983proposal}
Ronald~T. House.
\newblock A proposal for an extended form of type checking of expressions.
\newblock {\em The Computer Journal}, 26(4):366--374, 1983.

\bibitem{jacobs1999categorical}
Bart Jacobs.
\newblock {\em Categorical logic and type theory}, volume 141.
\newblock Elsevier, 1999.

\bibitem{Kennedy:1997:RPU:263699.263761}
Andrew~J. Kennedy.
\newblock Relational parametricity and units of measure.
\newblock In {\em POPL'97}, 1997.

\bibitem{lawvere-adjointness}
F.~William Lawvere.
\newblock Adjointness in foundations.
\newblock {\em Dialectica}, 23(3-4):281--296, 1969.

\bibitem{manner1986strong}
R~M{\"a}nner.
\newblock Strong typing and physical units.
\newblock {\em ACM Sigplan Notices}, 21(3):11--20, 1986.

\bibitem{MarsCrash}
{Mars Climate Orbiter Mishap Investigation Board}.
\newblock Phase {I} report.
\newblock NASA, 1999.

\bibitem{mellies-zeilberger}
Paul-Andr\'e Melli\`es and Noam Zeilberger.
\newblock Functors are type refinement systems.
\newblock In {\em Proc.~POPL 2015}, pages 3--16, 2015.

\bibitem{reynolds1983types}
John Reynolds.
\newblock Types, abstraction and parametric polymorphism.
\newblock In {\em Information Processing}, 1983.

\bibitem{sonin2001physical}
Ain~A Sonin.
\newblock The physical basis of dimensional analysis.
\newblock Department of Mechanical Engineering, MIT, 2001.

\bibitem{wand1991automatic}
Mitchell Wand and Patrick O'Keefe.
\newblock Automatic dimensional inference.
\newblock In {\em Computational Logic -- Essays in Honor of Alan Robinson},
  pages 479--483, 1991.

\end{thebibliography}
