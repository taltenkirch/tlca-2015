\section{Inhabitation for System $\Pu$}
\label{s:inhabitation}

We now show a sound and complete algorithm to solve the
inhabitation problem for System $\Pu$.  
Given a \strict\  type $\sig$, the 
inhabitation problem consists in finding a closed term
$\s$ such that $\der \s: \sig$ is derivable. We
extend the problem to multiset types by defining $\A$ to be 
inhabited if and only if there is a closed term $\s$ such that $\der \s:
\sig_i$ for every $\sig_i \in \A$. These notions will naturally
be generalized later to non-closed terms. 

We already noticed that the system $\Pu$ allows to type terms
containing untyped subterms through the rule $(\trarrowe)$ with $I=\es$ and the rule $(\trsub)$ with $I=J=\es$. 
In order to  identify 
inhabitants in such cases we  introduce a term constant $\Omega$ to denote a generic
untyped subterm. Our inhabitation algorithm produces 
\deft{approximate normal forms} ($\ap, \bp, \cp$), also written $\anf$, defined as follows: 
\begin{center} $
\begin{array}{lll@{\hspace{1.5cm}}lllllllllll}
     {\ap},{\bp},{\cp} & ::= & \Omega \mid {\cal N} & 
     {\cal N}          & ::= & \l \p. {\cal N}  \mid  
                               \pair{\ap}{\bp}  \mid 
                               {\cal L} \mid   {\cal N} [\pair{\p}{\q}/{\cal L}]\\
     &&& {\cal L}          & ::= &  \x  \mid {\cal L} \ap   \\
   \end{array}  
$ \end{center}
Note that $\anf s$ do not contain redexes, differently
from canonical forms. In particular, thanks to the reduction rule
$(\rdix)$ (resp. $(\ronze)$), they do not contain {\it head} (resp. {\it nested}) explicit matchings. 
This makes the  inhabitation algorithm much more intuitive and simpler. 
\begin{example}
the term  $\l    \pair{\x}{\y}.   (\x    (\id    \id))
[\pair{\z_1}{\z_2}/\y\id]$ is canonical but not an $\anf$, 
while  $\l  \pair{\x}{\y}. (\x \Omega) [\pair{\z_1}{\z_2}/\y\id]$ is
an    $\anf$.
\end{example}


$\Anf s$ 
are ordered by  the smallest contextual  
order $\leq$ such that $\Omega \leq \ap$, for any  $\ap$.
We also write $\ap \leq \s$ when the term $\s$ is obtained from 
 $\ap$ by
replacing each occurrence of $\Omega$ by a term of $\Lp$:
For example $\x \Omega \Omega \leq \x (\id \Del) (\Del \Del)$ is obtained by replacing 
the first (resp. second) occurrence of $\Omega$ by $\id \Del$ (resp. $\Del \Del$). 

Let $\Ap(\s)=\{\ap \ |\ \exists \uu\ \s \Rewn{} \uu \mbox{ and
}\ap\leq \uu\}$ be the set of \deft{approximants}  of the term $\s$, and
let $\bigvee$ denote the least upper bound with respect to $\leq$.  We write
$\uparrow_{\iI}\ap_i$ to denote the fact that $\bigvee\{\ap_i\}_{\iI}$
does exist.  It is easy to check that, for every $\s$ and
$\ap_1,\ldots \ap_n \in \Ap(\s)$,
$\uparrow_{i\in\{1,\ldots,n\}}\ap_i$. 
An $\anf$  $\ap$
is a \deft{head subterm} of $\bp$ if either $\bp = \ap$ or $\bp=\cp
\cp'$ and $\ap$ is a head subterm of $\cp$. System $\Pu$ 
can also  be trivially extended to give types
to $\anf s$, simply assuming that no type can be
assigned to the constant $\Omega$.  It is easy to check that, if
$\Gamma\der \ap:\sigma$ and $\ap\leq\bp$ (resp.  $\ap\leq\s$) then
$\Gamma\der \bp:\sigma$ (resp.  $\Gamma\der \s:\sigma$). 

Given  $\Pi\dem  \Gam  \der   \s:\tau$,  where  $\s$  is  in  $\Pi$-nf
(\cf\ Sec.~\ref{l:type-system}), $\Ap(\Pi)$ is the minimal approximant
$\bp$ of $\s$ such that $\Pi\dem \Gam \der \bp:\tau$.  Formally, given
$\Pi\dem\Gam   \der  \s:\sig$,   where  $\s$   is  in   $\Pi$-nf,  the
\deft{minimal approximant of $\Pi$}, written $\Ap(\Pi)$, is defined by
induction on $\meas(\Pi)$ as follows:
\begin{itemize}
\item $\Ap(\Gam \der \x:\rho)= \x$;
$\Ap(\Gam \der \pair{\s}{\uu}:\oprod)= \pair{\Omega}{\Omega}$.
\item If $\Pi\dem\Gam \der \l \p. \s: \A \arrow \rho$ follows  from 
     $\Pi' \dem\Gam' \der \s:\rho$, then $\Ap(\Pi)= \lambda \p. \Ap(\Pi')$,  $\s$ being in  $\Pi'$-nf. 
\item If $\Pi\dem \Gam \der \pair{\s}{\uu}:\produ{\tau}$ follows
from $\Pi' \dem \Gam \der \s: \tau$, then
$\Ap(\Pi)= \pair{\Ap(\Pi')}{\Omega}$, $\s$ being in  $\Pi'$-nf. 
Similarly for a pair of type $\prodd{\tau}$.
%\item If $\Pi\dem \Gam \der \pair{\uu}{\s}:\prodd{\tau}$ follows
%from $\Pi' \dem \Gam \der \uu: \tau$, then
%$\Ap(\Pi)= \pair{\Omega}{\Ap(\Pi')}$, $\s$ being in  $\Pi'$-nf. 
\item If $\Pi\dem\Gam=\Gam'+_{i\in I}\Delta_i\der \s\uu:\rho$ follows  from $\Pi'\dem\Gam'\der \s:  \multiset{\sigma_{i}}_{\iI} \to \rho$
and $(\Pi'_i\dem\Delta_i \der\uu:\sigma_i)_{i\in I}$,
then $\Ap(\Pi)=\Ap(\Pi')(\bu_{\iI} \Ap(\Pi_i'))$
%(remark that $\s$ is in $\Pi'$-nf, $\uu$ is in $\Pi'_i$-nf, for all
%$\iI$, and  that $\uparrow_{\iI}\Ap(\Pi'_i )$, since $\Ap(\Pi'_i )\in \Ap(\uu)$, for all $\iI$).
\item If $\Pi\dem \Gam = \Gam' +_{\iI} \Del_i \der \s[\p/\uu]: \tau$
follows  from $\Pi' \dem \Gam'' \der \s:\tau$ and 
      $(\Psi_i \dem \Del_i \der \uu: \rho_i)_{\iI}$, then 
      $\Ap(\Pi)=  \Ap(\Pi')[\p/\bu_{\iI} \Ap(\Psi_i)]$ 
%(remark, as above, 
%that $\s$ is in $\Pi'$-nf, $\uu$ is in $\Pi'_i$-nf, for all
%$\iI$, and  that $\uparrow_{\iI}\Ap(\Pi'_i )$, since $\Ap(\Pi'_i )\in \Ap(\uu)$, for all $\iI$).
\end{itemize}


Remark that, in the application case of the definition above, 
the $\anf$ corresponding to $I=\es$ is
$\Ap(\Pi')\Omega$. 
Moreover, in the last case, 
$\p$ cannot be a variable, $\s$ being in $\Pi$-nf.
A simple inspection of the typing rules for $\pder$  
shows that  in this case $I\not=\es$.

% typing rules for patterns 
% allow to $I\not=\es$, as 


% $I=\es$ is not possible,
% since  $\p$ cannot be a variable ($\s$ being in $\Pi$-nf), and 
% since 
% by Prop. \ref{prop:generation}.\ref{PNV}, since 
% $\p$ cannot be a variable ($\s$ being in $\Pi$-nf).


\begin{example}
Consider the following derivation $\Pi$:
\begin{center} $
\infer{
\infer{
\infer{\y:\multiset{\emul \to \oprod}\der \y:\emul \to \oprod}
{\y:\multiset{\emul \to \oprod} \der \y  (\Delta \Delta):\oprod}
\sep
\infer{}{\pder \pair{\z_1}{\z_2}:\oprod} \sep \infer{\x:\multiset{\emul \to \oprod}\der \x:\emul \to \oprod}
        {\x:\multiset{\emul \to \oprod} \der \x\id:\oprod} 
}
{\x:\multiset{\emul \to \oprod} ;\y:\multiset{\emul \to \oprod} \der \y(\Delta \Delta) [\pair{\z_1}{\z_2}/\x\id]:\oprod }
}
{\der \l \x\y.\y (\Delta \Delta) [\pair{\z_1}{\z_2}/\x\id]: \multiset{\emul \to \oprod} \to \multiset{\emul \to \oprod} \to \oprod}  
$ \end{center}
The minimal approximant of $\Pi$ is $\l \x\y.\y\Omega[\pair{\z_1}{\z_2}/\x\Omega]$.
\end{example}
A simple induction on  $\meas(\Pi)$ allows to show the following:

\begin{lemma}
\label{Prop:approx}
If $\Pi \dem \Gam \der \s:\sigma$ and $\s$ is in $\Pi$-nf,  then    
$\Pi \dem \Gam \der \Ap(\Pi):\sigma$.
\end{lemma}
\subsection{The inhabitation algorithm}

The inhabitation algorithm is presented in
Fig.~\ref{fig:inhabitation-algorithm}. As usual, in order to solve
the problem for closed terms, it is necessary to extend 
the algorithm  to open
ones, so, given an environment $\Gam$ and a \strict\  type $\sigma$,
the algorithm builds the set $\K(\Gam, \sig)$ containing {\it all} the
$\anf s$ $\ap$ such that there exists a derivation
$\Pi\dem\Gam \der \ap:\sig$, with $\ap=\Ap(\Pi)$, then stops\footnote{
It is worth noticing that,  given $\Gam$ and $\sig$, the set of $\anf s$   $\ap$ such that there exists a derivation $\Pi\dem\Gam \der \ap:\sig$
is possibly infinite. However, the subset of those verifying  $\ap=\Ap(\Pi)$ is finite; 
they are the minimal ones, those generated
by the inhabitation algorithm (this is proved in Lem. \ref{Lem:main}).}.  Thus,
our algorithm is not an extension of the classical inhabitation
algorithm for simple types \cite{BenYellesPhd,hindley08}. In
particular, when restricted to simple types, it constructs all the
$\anf s$  inhabiting a given type, while the original algorithm
reconstructs just the {\it long $\eta$-normal forms}.
%(we say that the algorithm 
%{\it fails} if it stops  without providing any output).
The algorithm  uses four  auxiliary predicates, namely 
\begin{itemize}

\item
$\Pa{}{\V}(\A)$, where $\V$ is a finite set of variables, contains  the pairs $(\Gam,\p)$ 
such that  (i) $\Gam\pder\p:\A$, and (ii)
$\p$ does
not contain any variable in $\V$\ignore{Notice that
$\Pa{}{\V}(\A)$ contains infinitely many pairs, since 
there are infinitely many variables. This does not affect the termination of the algorithm, since these different pairs give raise 
to $\alpha$-equivalent terms, so that in an implementation, only  the first 
variable non occurring in $\V$, with respect to some enumeration,  will be generated.}.
\item
$\KI(\Gam,\multiset{\sig_i}_{\iI})$, contains all the $\anf s$ $\ap=\bu_{\iI} \ap_i$ such that  $\Gam =  +_{\iI} \Gam_i$,
$\ap_i\in\K(\Gam_i,\sigma_i)$ for all $i\in I$, and $\uparrow_{\iI}\ap_i$.
\item
$\LK{\bp}{\Del}(\Gam, \sig) \donne \tau$  contains all the $\anf s$ $\ap$ such that 
$\bp$ is a head subterm of $\ap$, and such that 
if $\bp \in \K(\Delta,\sigma)$ then 
 $\ap \in \K(\Gamma+\Delta,\tau)$.
\item
$\LKI{\bp}{\Del}(\Gam, \multiset{\sig_i}_{\iI}) \donne \multiset{\rho_i}_{\iI}$
contains all the $\anf$ $\ap=\bu_{\iI} \ap_i$ such that
$\Del =  +_{\iI} \Del_i$, $\Gam =  +_{\iI} \Gam_i$, $\ap_i\in \LK{\bp}{\Del}(\Gam, \sig_i) \donne \rho_i$     and $\uparrow_{\iI}\ap_i$.
\end{itemize}
\ignore{Remark that terms generated by the $(\Subs)$ rule are particularly simple, \ie\
they do not contain nested explicit matchings. This is precisely due to the rewriting rule $(\ronze)$,
which is, at the same time, the source of the non-trivial typing rule $(\trsub)$.}
Note that the algorithm has different kinds of  non-deterministic  behaviours, \ie\
different choices of rules can produce different results. 
 Indeed,   given an input $(\Gam, \sig)$, the algorithm
may apply a rule like $(\Abs)$   in order to decrease the type $\sig$,  or
a rule like  $(\Head)$  in order to decrease the environment $\Gam$.
Moreover,  every rule $(R)$ which is  based on some decomposition of the environment 
and/or the type, like $(\Subs)$,  admits  different applications. 
In what follows we illustrate the non-deterministic behaviour of the algorithm. 
For that, we represent a {\bf run of the algorithm} as a tree whose nodes
are labeled with the name of the rule applied. 


%\begin{example}\label{Exe1}
%We consider different inputs of the form $(\es, \sig)$, for different
%\strict\ types $\sig$. 
%\begin{enumerate}
%\item\label{primo}
%For $\sigma =\multiset{\multiset{\alpha} \to \alpha}\arrow \multiset{\alpha} \to \alpha$,
%the algorithm has two behaviours:
%\begin{enumerate}
%\item The approximate nf is $\l \x\y.\x\y$ by choosing the tree \\
%$\Abs(\Abs(\Head(\Prefix(\Unionk(\Head(\Final)), \Final))))$. 
%\item The approximate nf  is  $\l \x.\x$ by choosing the tree 
%$\Abs(\Head(\Final)))$. 
%\end{enumerate}
%\item  For $\sigma =\multiset{\emul \to \alpha}\arrow \alpha$,
%the algorithm  produces the approximate nf  $\l\x.\x\Omega$
%which corresponds to 
%$\Abs(\Head(\Prefix(\Unionk, \Final)))$. 
%\item For  $\sigma =\multiset{\multiset{\oprod} \to \oprod,\oprod}\arrow \oprod$,
%the algorithm has the following behaviours:
%\begin{enumerate}
%\item Choosing the run represented by the tree:\\ 
%$\Abs(\Head(\Prefix(\Unionk(\Head(\Final)), \Final)))$ the 
%approximate nf  $\l \x.\x\x$ is produced. \ignore{This is actually the only term 
%without explicit substitutions having type $\sigma$; in the following ones, ``dummy'' }
%explicit substitutions are used to consume some, or all, the resources in $\multiset{\multiset{\oprod} \to \oprod,\oprod}$
%\item The term $\l \x.\x[\pair{\y}{\z}/\x\pair{\Omega}{\Omega}]$ is produced by the  run of 
%the algorithm represented by the following tree:\\
%{\scriptsize 
%$\Abs(\Subs(\Unionl(\Prefix(\Unionk(\Pair),\Final)),\Pairp(\Weakp,\Weakp),\Head(\Final))  , \Varp)$. 
%}
%%% {\scriptsize 
%%% \[ \infer{\infer{ \infer{\infer{ \infer{(\Pair)}
%%%                                        {(\Unionk)}\\ (\Final)} 
%%%                                {(\Prefix)}}
%%%                         {(\Unionl) }\\
%%%                  \infer{(\Weakp) \\ (\Weakp) }{(\Pairp)} \\
%%%                  \infer{(\Final)}
%%%                        {(\Head)}}
%%%                 {(\Subs)} \\
%%%           (\Varp)}
%%%          {(\Abs)} \]
%%% }
%\item There are four more possible runs, producing the following approximate nfs:
%$\l \x.\x\pair{\Omega}{\Omega}[\pair{\y}{\z}/\x]$, 
%$\l \x.\pair{\Omega}{\Omega}[\pair{\y}{\z}/\x\x]$, \\
%$\l \x.\pair{\Omega}{\Omega}[\pair{\y}{\z}/\x][\pair{\w}{\tt{s}}/\x\pair{\Omega}{\Omega}]$ and 
%$\l \x.\pair{\Omega}{\Omega}[\pair{\y}{\z}/\x\pair{\Omega}{\Omega}] [\pair {\w}{\tt{s}}   /\x]$.
%\end{enumerate} 
%\end{enumerate} 
%\end{example} 
%
\begin{example}\label{Exe1}
We consider different inputs of the form $(\es, \sig)$, for different
\strict\ types $\sig$. For every such input, we give an output and the corresponding run.
\begin{enumerate}
\item\label{primo}
$\sigma =\multiset{\multiset{\alpha} \to \alpha}\arrow \multiset{\alpha} \to \alpha$.
\begin{enumerate}
\item output: $\l \x\y.\x\y$, run: $\Abs(\Abs(\Head(\Prefix(\Unionk(\Head(\Final)), \Final)),\Varp),\Varp)$.
\item output: $\l \x.\x$, run: $\Abs(\Head(\Final),\Varp)$.
\end{enumerate}
\item $\sigma =\multiset{\emul \to \alpha}\arrow \alpha$. output:  $\l\x.\x\Omega$, run: $\Abs(\Head(\Prefix(\Unionk, \Final)),\Varp)$.
\item $\sigma =\multiset{\multiset{\oprod} \to \oprod,\oprod}\arrow \oprod$.
\begin{enumerate}
\item output: $\l \x.\x\x$, run:
$\Abs(\Head(\Prefix(\Unionk(\Head(\Final)), \Final)),\Varp)$. 
\item Explicit substitutions may be used to consume some, or all, the resources in $\multiset{\multiset{\oprod} \to \oprod,\oprod}$ \\
output: $\l \x.\x[\pair{\y}{\z}/\x\pair{\Omega}{\Omega}]$, run:\\
{\scriptsize 
$\Abs(\Subs(\Unionl(\Prefix(\Unionk(\Pair),\Final)),\Pairp(\Weakp,\Weakp),\Head(\Final))  , \Varp)$.
}
\item There are four additional runs, producing the following outputs:

$\l \x.\x\pair{\Omega}{\Omega}[\pair{\y}{\z}/\x]$, 

$\l \x.\pair{\Omega}{\Omega}[\pair{\y}{\z}/\x\x]$, 

$\l \x.\pair{\Omega}{\Omega}[\pair{\y}{\z}/\x][\pair{\w}{\tt{s}}/\x\pair{\Omega}{\Omega}]$,
 
$\l \x.\pair{\Omega}{\Omega}[\pair{\y}{\z}/\x\pair{\Omega}{\Omega}] [\pair {\w}{\tt{s}}   /\x]$.
\end{enumerate} 
\end{enumerate} 
\end{example} 



\begin{figure}[h!]
\begin{framed}
\begin{center}
$ \begin{array}{c}
\infer{\x \notin \V}
{(\es; \x) \in_{0} \Pa{}{\V}(\emul)}\ (\Weakp)
\quadd 
\infer{\x \notin \V}
      { (\x:\D;  \x) \in_{0} \Pa{}{\V}(\D) }\ (\Varp) \\\\
\infer{(\Gam ;  \p) \in_{i} \Pa{}{\V}(\A_1) \\ 
 (\Delta ;  \q) \in_{j} \Pa{}{\V}(\A_2) \\ 
 \p \# \q}
{(\Gam \duenv  \Delta; \pair{\p}{\q}  ) \in_{1}   \Pa{}{\V}(\A_1 \prodmul  \A_2)}\ (\Pairp) \\\\
\infer
   {\ap \in \K(\Gam \duenv    \Delta ,   \tau)  \\
   \A = \A_1 \munion \A_2  \\
   (\Delta; \p)  \in_{i}  \Pa{}{\dom{\Gam}}(\A_1) \\ 
   \supp{\A_2} \subseteq \struc{\p} }
{ \l \p. \ap \in \K(\Gam  ,  \A \arrow \tau)}\ (\Abs) \\\\
\infer{ (\ap_i \in \K( \Gam_i,  \sig_i))_{\iI} \sep \uparrow_{\iI}\ap_i }
         {\bigvee_{\iI} \ap_i \in  \KI(+_{ \iI} \Gam_i,  \multiset{\sig_i}_{\iI})}\ (\Unionk) 
   \sep 
   \infer{ (\ap_i \in \LK{\bp}{\Del_i}( \Gam_i,  \sig_i) \donne \rho_i)_{\iI} \sep \uparrow_{\iI}\ap_i }
        {\bigvee_{\iI} \ap_i \in  \LKI{\bp}{+_{ \iI} \Del_i}(+_{ \iI} \Gam_i,  \multiset{\sig_i}_{\iI}) \donne \multiset{\rho_i}_{\iI}}\ (\Unionl) \\
\infer{\mbox{}}{\pair{\Omega}{\Omega} \in  \K(\es, \oprod)}\ (\Pair)
\quadd 
\infer{ \ap \in \K(\Gam, \tau)}{ \pair{\ap}{\Omega} \in \K( \Gam,  \produ{\tau})}\ (\Produ)
\quad 
\infer{ \ap \in \K( \Gam, \tau)}{ \pair{\Omega}{\ap} \in \K(\Gam, \prodd{\tau})}\ (\Prodd)\\\\
\infer{ \ap \in \LK{\x}{\x: \multiset{\sig}}( \Gam ,  \sig) \donne   \tau }
         { \ap  \in \K( \Gam +  (\x: \multiset{\sig}), \tau)}\ (\Head)
\quadd
\infer {\sig = \tau}{\ap \in \LK{\ap}{\Del}(\es, \sig) \donne  \tau}\ (\Final) \\\\
\infer{  \Gam =  \Gam_0  + \Gam_1  \\
            \bp \in \KI(\Gam_0, \A) \\ 
            \ap \in \LK{\cp \bp}{\Del + \Gam_0 }(\Gam_1, \sig) \donne \tau  }
            {  \ap\in \LK{\cp}{\Del}( \Gam , \A \to \sig) \donne  \tau   }\ (\Prefix)\\\\
\infer{ \Gam =  \Gam_0  + \Gam_1  \\
           \cp \in \LKI{\x}{\x:\D}(\Gam_0, \D) \donne \fin{\D} \\ 
                    \fin{\D} = \A_1 \munion  \A_2 (*) \\
                    (\Del, \p) \in_{1} \Pa{}{\dom{\Gam_0 + \Gam_1  + ( \x: \D) }}(\A_1) \\ 
                    \supp{\A_2} \subseteq \struc{\p} \\
                    \bp \in \K(\Gam_1 \duenv  \Del, \tau)  }
         {\bp[\p/\cp] \in \K( \Gam  +   (\x: \D), \tau)}\ (\Subs) \\\\
\end{array}$ 
(*) where the operator \fin{} is defined as follows:
$\begin{array}{lll@{\hspace{.7cm}}lll@{\hspace{.7cm}}lll@{\hspace{.7cm}}lll }
\fin{\alpha} &  := & \alpha & \fin{\A \to \tau} & := & \fin{\tau} &
\fin{\emul}  & := & \emul & \fin{\multiset{\sig_i}_{\iI}} & := & \multiset{\fin{\sig_i}}_{\iI} \\
\end{array}$
\end{center}
\caption{The inhabitation algorithm}
\label{fig:inhabitation-algorithm}
\end{framed}
\end{figure}

Along the recursive calls of the inhabitation algorithm, 
the parameters (type and/or environment)
decrease  strictly, for a suitable notion of measure,  
so that every run is finite:



% The proof of the following property can be found in
% Appendix~\ref{appendix-inhabitation}: 


\begin{lemma}
\label{l:termination}
The inhabitation algorithm terminates.
\end{lemma} 


\subsection{Soundness and completeness}

We now prove soundness and completeness of our inhabitation algorithm.


\begin{lemma}\label{Lem:main}
$\ap\in\K(\Gamma,\sigma)$ $\Leftrightarrow$
$\exists \Pi \dem \Gam \der \ap:\sigma$ such that $\ap=\Ap(\Pi)$.


\end{lemma}

\begin{proof} 
  The ``only if'' part is proved by induction on the rules in
  Fig.~\ref{fig:inhabitation-algorithm}, and the ``if'' part is proved
  by induction on the definition of $\Ap(\Pi)$.  In both parts,
  additional statements concerning the predicates of the inhabitation
  algorithm other than $\K$ are required, in order to strenghten the
  inductive hypothesis.

%See  Appendix~\ref{appendix-inhabitation}. 
\end{proof}


\begin{theorem} [Soundness and Completeness]
\label{t:soundness-completeness} \mbox{}
\begin{enumerate} 
\item \label{t:soundness}
If $\ap \in \K(\Gam, \sig)$  then, for all $\s$ such that $\ap\leq\s$,  $\Gam  \der \s:\sig$.
%$\s\in \lift{\ap}$.

\item
 \label{t:completeness}
If $\Pi\dem\Gam \der \s:\sig$ then there exists  $\Pi'\dem\Gam \der \s':\sig$ such that  $\s'$ is in $\Pi'$-nf, 
and  $\Ap(\Pi') \in \K(\Gam,\sig)$.

\end{enumerate}
\end{theorem}
\begin{proof}
Soundness follows from  Lem.~\ref{Lem:main} ($\Rightarrow$)
and the fact that $\Gamma\der \ap:\sigma$ and $\ap\leq\s$ imply 
$\Gamma\der \s:\sigma$. 
For completeness we first apply  Lem.~\ref{lem:red:exp}.\ref{lem:reduction}
that guarantees the existence of  $\Pi'\dem\Gam \der \s':\sig$ such that  $\s'$ is in $\Pi'$-nf, 
and then Lem.~\ref{Prop:approx} and Lem.~\ref{Lem:main} ($\Leftarrow$).
\end{proof}



