\section{Introduction}

In these last years there has been  a growing interest in {\it pattern
  $\l$-calculi}~\cite{PeytonJones,Kahl-2004a,CK04,KvOdV08,JK09,Petit09} which
are used to model the pattern-matching primitives of functional
programming languages (\eg\ OCAML, ML, Haskell) and proof assistants
(\eg\ Coq, Isabelle).  These calculi are extensions of $\l$-calculus, where  abstractions are
written as $\l \p. \s$, where $\p$ is 
a {\it pattern} specifying the expected structure of the argument.
\ignore{There are pattern $\l$-calculi modeling exactly functional
languages, and thus based only on algebraic {\it static}
patterns~\cite{Kahl-2004a}, whereas more advanced languages integrate also a notion of
{\it dynamic} patterns~\cite{JK09}, which   become first-class
citizens.  }
In this paper we  restrict our attention to 
{\it pair} patterns, which are expressive enough to illustrate 
the challenging notion of solvability/observability in 
the framework of pattern $\l$-calculi. 


In order to implement different {\it evaluation strategies}, the use
of {\it explicit pattern-matching} becomes  appropriate, 
giving rise to different languages with explicit
pattern-matching~\cite{CK04,CirsteaFK07,Bal2012}.  In all 
of them,  an application $(\l \p. \s)\uu$ reduces to $\s[\p/\uu]$,
where $[\p/\uu]$ is an explicit matching, defined by means of
suitable reduction rules, which are used to decide if the argument
$\uu$ matches the pattern $\p$.  If the matching is possible, the evaluation proceeds by
computing a substitution which is applied to the body $\s$. Otherwise, two cases arise: either a
successful matching is not possible at all,  
and then the  term $\s[\p/\uu]$
reduces to a {\it failure}, denoted by the constant $\fail$,
or it could become
possible after the application of some pertinent substitution to the
argument $\uu$, in which case the reduction is simply {\it blocked}.  An example of failure is
caused by the term $(\l \pair{\z_1}{\z_2}.\z_1)(\l \y.\y)$,
while a 
%since there 
%is no substitution transforming a function into a pair. A 
blocked reduction is caused by the term $(\l \pair{\z_1}{\z_2}.\z_1)\y$.


\ignore{
Explicit matching turns out to avoid certain blocking computations, so
that the reduction space becomes bigger then the one of the implicit
operational semantics sketched above, thus bringing the possibility of
implementing finer abstract machines.} \ignore{ in order to implement
  here is an example.  Let $\p=\pair{\z_1}{\z_2}$ and $\id = \l
  \y. \y$ and consider the term $\s_0= (\l \p. (\l \y. \y \id) \id)
  (\id \pair{\id}{\id})$. Then the pattern $\p$ and the term $\id
  \pair{\id}{\id}$ do not match, but they could match in the future if
  the term $\id \pair{\id}{\id}$ is further reduced.  So $\s_0$ has
  only one redex (the term $(\l \y. \y \id) \id$) in the implicit
  operational semantics sketched above.  However, by allowing the use
  of explicit matching, $\s_0$ can two redexes which are the term
  $\s_0$ and the subterm $(\l \y. \y \id) \id$, so that $\s_0$ can be
  reduced further to $\s'_0 = (\y \id)[\y/\id][\p/\id
    \pair{\id}{\id}]$, thus delaying the test fired by the operator
  $[\p/\x]$, which becomes a {\it blocking explicit matching}, after
  the reduction $\s'_0 \Rew{} $.}

\ignore{Another advantage of the use of explicit matching comes from the study
of solvability in call-by-value~\cite{AP12}.  The idea can be
explained in our framework by considering the term $\s_2=(\l
\pair{\z_1}{\z_2}.\Delta) \x \Delta$, where $\Delta = \l \z. \z\z$.  The term $\s_2$
is in normal form with respect to the implicit operational semantics, even if
it represents a meaningless computation because it is equivalent to
$(\Delta \Delta )[\pair{\z_1}{\z_2}/\x]$, which does not terminate since $\Delta
\Delta$ reduces to itself. Thus, the use of explicit
substitution/matching puts in evidence non-termination of some {\it
  meaningless} terms.}

Inspired by the notion of solvability in the $\l$-calculus, we define
a notion of {\it observability} for a pair pattern calculus with explicit matching. A term $\s$ is said to be
observable if there is a {\it head-context} $\ccontext$ such that $\ccontext[\s]$
reduces to a pair, which is the only data structure of the
language. This notion is conservative with respect to the notion of
solvability in the $\l$-calculus, \ie\ $\s$ is solvable in the
$\l$-calculus if and only if $\s$ is observable in our calculus.
 
%if $\s$ is solvable in the
%$\l$-calculus, then $\s$ is observable in our calculus. 


%The main result
%of this paper is a logical characterization of observability through a
%type assignment system based on intersection types. 
%The intersection
%connective of our system is partial and non-idempotent. Partiality comes from the
%fact that types can denote functions or products, which are the two
%kinds of values present in our language, and intersection can only
%collect types of one kind. Non-idempotency gives a quantitative flavour
%to the type assignment system.
%


Solvability in the $\l$-calculus is of course undecidable, but it has
been characterized at least in three different ways: syntactically by
the notion of head-normal form~\cite{barendregt84nh}, operationally by
the notion of head-reduction~\cite{barendregt84nh}, and logically by
an intersection type assignment
system~\cite{tipoA-BCD:JSL,krivine93book}.\ignore{ In the latter
characterization a term is solvable if and only if it can be assigned
a meaningful type, \ie\ a type different from the universal type
(inhabited by all terms).}  The problem becomes harder when changing
from the call-by-name to the call-by-value setting. Indeed, in the
call-by-value $\l$-calculus, there are normal forms that are
unsolvable, like the term $(\l \z.\Delta) (\x I) \Delta$, where $\Delta=\l \x.\x\x$.  The problem for
the  pair pattern calculus is similar to that for the call-by-value, but
even harder.  As in the call-by-value setting, an argument needs to be
partially evaluated before being consumed. Indeed, in order to
evaluate an application $(\l \p. \s)\uu$, it is necessary to verify
if $\uu$ matches the pattern $\p$, and thus the subterm $\uu$ can be
forced to be partially evaluated.  However, while only discrimination
between values and non-values are needed in the call-by-value setting,
the possible shapes of patterns are infinite here.

The difficulty of the problem depends on two facts. First, there is no simple
 {\it syntactical} characterization of observability:
indeed, we supply a notion
of {\it canonical form} such that reducing to some canonical form 
is a necessary condition for being observable. But this is not sufficient: 
canonical forms may contain blocking
explicit matchings, so that we need to know whether or not there
exists a substitution being able to unblock simultaneously all these blocked forms.  
%In fact the observability problem for a given term $t$ has two
%different levels of undecidability. The first one is to know whether
%or not $t$ can be
%reduced to some kind of canonical form, possibly containing blocking
%explicit matchings, the second one is to know whether or not there
%exists a substitution unblocking simultaneously all the blocking
%explicit matchings. 
This theoretical complexity is reflected in the logical
characterization we supply for observability: a term $\s$ turns out 
to be observable if and only if  it is typable, say with a type of the shape
$\A_1 \to \A_2 \to...\to \A_n \to \alpha$ (where $\alpha$ is a product
type), and all the types $\A_i\ (1\leq i \leq n)$ are  {\it
  inhabited}.  The inhabitation problem for idempotent intersection
types is known to be
undecidable~\cite{Urzyczyn99}, but 
it has recently been proved that
it is decidable in the non-idempotent
case~\cite{bkdlr14}. More precisely, there is a sound and complete
algorithm solving the inhabitation problem of non-idempotent
intersection types for the $\l$-calculus.  In this paper, we supply a
type assignment system, based on non-idempotent intersection, which
assigns types to terms of our pair pattern calculus. We then extend the
inhabitation algorithm given in~\cite{bkdlr14} to 
this framework, that is substantially more complicated,  due to the 
explicit pattern matching and the use of structural information of patterns
in the typing rules.
However, the paper does not only show decidability
of inhabitation for the pair pattern calculus, 
but it {\it uses} the decidability
result to derive a full characterization of observability, 
which is the main result of the paper. We thus combine
typability with inhabitation in order to obtain an interesting characterization of the set
of {\it meaningful} terms of the pair pattern calculus.

\ignore{
It is important to notice that also in the case of the $\l$-calculus the
logical characterization of solvability is related to inhabitation. In fact, the logical characterization of
solvability is constructive: all and only the 
terms having head
normal forms can be typed by meaningful types, and each  head
normal form has a canonical type of the form 
$\sigma_1 \to \sigma_2 \to...\to \sigma_n \to \alpha\to\alpha$ (for some $n$, $\alpha$ being a type constant). 
The inhabitants of the types $\sigma_i\ (1\leq i \leq n)$,
witness  the solvability of the head normal form we started from.
But this connection has not been explored further, since being typable in $\l$-calculus is enough for
being solvable, independently from any condition on types. 
In this paper we put in evidence
the relation between observability and inhabitation for a pattern matching
calculus naturally  extending the $\l$-calculus. 
}

The paper is organized as follows. Sec.~\ref{s:calculus} introduces the pattern
calculus. Sec.~\ref{l:type-system} presents the type system and proves a
characterization of terms having canonical forms by means of
typability. Sec.~\ref{sec:from} discusses the relationship between observability
and inhabitation and Sec.~\ref{s:inhabitation} presents a sound and complete algorithm
for the inhabitation problem associated to our typing system. Sec.~\ref{s:charact}
shows a complete
characterization of observability, and Sec.~\ref{s:conclusion} concludes by
discussing some future work.



