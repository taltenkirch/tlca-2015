\section{Some conservativity results}\label{sect:conservative}

The sequentialization result  (Theorem~\ref{thm:trifurcate}) has some interesting semantic consequences. 
It allows us to prove that (Corollary~\ref{cor:observational}) the $\lambda_v^\sigma$-calculus is sound with respect to the call-by-value observational equivalence introduced by Plotkin in \cite{Plotkin75} for $\lambda_v$. 
Moreover we can prove that some notions, like that of potential valuability and solvability, introduced in \cite{PaoliniRonchi99} for $\lambda_v$, 
coincide with the respective notions for $\lambda_v^\sigma$ (Theorem~\ref{thm:valsolv}). 
This justifies the idea that $\lambda_v^\sigma$ is a useful tool for studying the properties of $\lambda_v$.
%, which was inspired by definition in \cite{carraro14lncs}.
Our starting point is the following corollary. %\Red{(of Theorem~\ref{thm:trifurcate} and Lemma~\ref{lemma:commutation}.\ref{lemma:commutation.general}, respectively)}.

\begin{cor}
\begin{enumerate}
  \item\label{cor:value.one} If $M \to_{\vg}^* V \in \Lambda_v$ then there exists $V' \in \Lambda_v$ such that $M \ToHeadBetavReflTrans V' %\InterTrans 
  \ToIntTrans V$.
  \item\label{cor:value.two} For every $V \in \Lambda_v$, $M \ToHeadBetavReflTrans V$ if and only if $M \ToHeadReflTrans V$.
\end{enumerate}\label{cor:value}
\end{cor}

\begin{proof}
  The first point %can be 
  is proved by observing that, by Theorem~\ref{thm:trifurcate}, there are $N, L \in \Lambda$ such that $M \ToHeadBetavReflTrans L \ToHeadSigmaReflTrans N %\InterTrans 
  \ToIntTrans V$. 
  By Remark~\ref{rmk:preliminary}.\ref{rmk:preliminary.interinclusion}%~\ref{rmk:abs}.\ref{rmk:abs.anti}
  , $N \in \Lambda_v$ and thus $L = N$ according to Remark~\ref{rmk:abs}.\ref{rmk:abs.notovalue}.
  Concerning the second point, the right-to-left direction is a consequence of Lemma~\ref{lemma:commutation}.\ref{lemma:commutation.general} and Remark~\ref{rmk:abs}.\ref{rmk:abs.notovalue}; the left-to-right direction follows from $\ToHeadBetav \, \subseteq \, \ToHead$.
\end{proof}

% Incidentally, notice that, in spite of the non-confluence of the head reduction, Corollary~\ref{cor:value}.\ref{cor:value.two} implies that if a term $M$ has a head normal form $N \in \Lambda_v$ then $N$ is the unique head normal form of $M$, since the head $\beta_v$-reduction is deterministic.

Let us recall the notion of observational equivalence defined by Plotkin~\cite{Plotkin75} for $\lambda_v$.

\begin{defin}[Halting, observational equivalence]\label{def:halt}
  Let $M \in \Lambda$. We say that (\emph{the evaluation of}) \emph{$M$ halts} if there exists $V \in \Lambda_v$ such that $M \ToHeadBetavReflTrans V$.

  The (\emph{call-by-value}) \emph{observational equivalence} is an equivalence relation $\cong$ on $\Lambda$ defined by: $M \cong N$ if, for every context $\mathtt{C}$, one has that $\ctxC{M}$ halts iff $\ctxC{N}$ halts.%
  \footnote{Original Plotkin's definition of call-by-value observational equivalence (see \cite{Plotkin75}) also requires that $\ctxC{M}$ and $\ctxC{N}$ are closed terms, according to the tradition identifying programs with closed terms.}
\end{defin}

Clearly, similar notions can be defined for $\lambda_v^\sigma$ using %the $\mathsf{v}$-reduction instead of the $\beta_v$ one.
$\ToHead$ instead of $\ToHeadBetav$.
%Corollary~\ref{cor:value} proves that 
Head $\sigma$-reduction plays no role neither in deciding the halting problem for evaluation (Corollary~\ref{cor:value}.\ref{cor:value.one}), nor in reaching a particular value (Corollary~\ref{cor:value}.\ref{cor:value.two}). 
So, we can conclude that the notions of halting and observational equivalence in $\lambda_v^\sigma$ %is conservative with respect to that of $\lambda_v$.
coincide with the ones in $\lambda_v$, respectively.

Now we compare the equational theory of $\lambda_v^\sigma$ with Plotkin's observational equivalence.

%Namely:% we can prove the following results.
\begin{thm}[Adequacy of $\vg$-reduction]\label{thm:adequacy}
    If $M \to_{\vg}^* M'$ then: $M$ halts iff $M'$ halts.
\end{thm}

\begin{proof}
  If $M' \ToHeadBetavReflTrans V \in \Lambda_v$ then $M \to_{\vg}^* M' \to_{\vg}^* V$ since $\ToHeadBetav \, \subseteq \, \to_{\vg}$. By Corollary~\ref{cor:value}.\ref{cor:value.one}, there exists $V' \in \Lambda_v$ such that $M \ToHeadBetavReflTrans V'$. Thus $M$ halts.

  Conversely, if $M \ToHeadBetavReflTrans V \in \Lambda_v$ then $M \to_{\vg}^* V$ since $\ToHeadBetav \, \subseteq \, \to_{\vg}$. By confluence of $\to_\vg$ (Proposition~\ref{prop:general-properties}, since $M \!\to_{\vg}^*\! M'$) and Remark~\ref{rmk:fromvalue} (since $V \!\in\! \Lambda_v$), there is $V' \!\in\! \Lambda_v$ such that $V \!\to_{\vg}^*\! V'$ and $M' \!\to_{\vg}^*\! V'$. 
  By Corollary~\ref{cor:value}.\ref{cor:value.one}, there is $V'' \!\in\! \Lambda_v$ such that \mbox{$M' \!\ToHeadBetavReflTrans\! V''$. So $M'$ halts.}
\end{proof}

% A remarkable consequence of Theorem~\ref{thm:adequacy} is that all $\vg$-equivalent terms are observationally equivalent (Corollary~\ref{cor:observational}). 

% \begin{defin}[Observational equivalence]
%   The \emph{observational equivalence} is an equivalence relation $\cong$ on $\Lambda$ defined by: $M \cong N$ if, for every context $\mathtt{C}$, one has that $\ctxC{M}$ halts iff $\ctxC{N}$ halts.
% \end{defin}

\begin{cor}[Soundness]\label{cor:observational}
  If $M =_\vg N$ then $M \cong N$.
% \textcolor{red}{LUCA: non so perche vogliamo complicare la vita al lettore usando notazioni unusuali per l'inter-convertibilit\`a,i.e  $\simeq_\vg$ al posto del solito  $=_\vg$: vabb\`e!}
\end{cor}
\begin{proof}
  Let $\mathtt{C}$ be a context. 
  By confluence of $\to_\vg$ (Proposition~\ref{prop:general-properties}), $M =_\vg N$ implies that there exists $L\in \Lambda$ such that $M \to_\vg^* L$ and $N \to_\vg^* L$, hence $\ctxC{M} \to_\vg^* \ctxC{L}$ and $\ctxC{N} \to_\vg^* \ctxC{L}$. 
  By Theorem~\ref{thm:adequacy}, $\ctxC{M}$ halts iff $\ctxC{L}$ halts iff $\ctxC{N}$ halts. Therefore, $M \cong N$.
\end{proof}

Plotkin \cite[Theorem~5]{Plotkin75} has already proved that $M =_{\beta_v} N$ implies $M \cong N$, but our Corollary~\ref{cor:observational} is not obvious since our $\lambda_v^\sigma$-calculus equates more than Plotkin's $\lambda_v$-calculus 
($=_{\beta_v} \, \subseteq \, =_\vg$ since $\to_{\beta_v} \, \subseteq \, \to_\vg$, and Example~\ref{ex:reductions} shows that this inclusion is strict).

The converse of Corollary~\ref{cor:observational} does not hold since $\lambda x. x (\lambda y. xy) \cong \Delta$ but $\lambda x. x (\lambda y. xy)$ and $\Delta$ are different $\vg$-normal forms and hence $\lambda x. x (\lambda y. xy) \neq_\vg \Delta$ % hence $\lambda x. x (\lambda y. xy) \neq_\vg \Delta$ 
by confluence of $\to_\vg$ (Proposition~\ref{prop:general-properties}).%: in order to get the converse of Corollary~\ref{cor:observational} we need to add at least the $\eta_v$-reduction (see \cite{moggi88ecs,sabry92lisp}) to our $\lambda_v^\sigma$-calculus





%Then, we use the sequentialization theorem to prove some results of interest (Theorems~\ref{thm:valsolv} and \ref{thm:adequacy}, Corollary~\ref{cor:observational}).
%
%In Section~\ref{sect:sos} we will prove the most interesting consequence of the sequentialization theorem: a standardization theorem for the $\lambda_v^\sigma$-calculus (Theorem~\ref{thm:standardization}).
%The next corollary of Theorem~\ref{thm:trifurcate} says that in the $\lambda_v^{\sigma}$-calculus, as in Plotkin's $\lambda_v$-calculus \cite{Plotkin75}, if a term $M$ $\vg$-reduces to a value, then the (maximal) head $\beta_v$-reduction sequence starting from $M$ terminates in a value: $\sigma$-reduction plays no role in deciding the halting problem for evaluation.
%
%
A further remarkable consequence of Corollary~\ref{cor:value}.\ref{cor:value.one} is that the notions of potential valuability and solvability  for $\lambda_v^{\sigma}$-calculus (studied in \cite{carraro14lncs}) can be shown to coincide with the ones for Plotkin's $\lambda_v$-calculus (studied in \cite{PaoliniRonchi99,ronchi04book}), respectively. Let us recall their definition.

\begin{defin}[Potential valuability, solvability]\label{def:valsolv}
  Let $M$ be a term:
  \begin{itemize}
    \item \label{def:valsolv.potval}
    $M$ is \emph{$\vg$-potentially valuable} (resp.\ \emph{$\beta_v$-potentially valuable}) if there are $m \in \Nat$, pairwise distinct variables $x_1,\dots,x_m$ and $V, V_1, \dots, V_m \in \Lambda_v$ such that $M\msub{V_1}{x_1}{V_m}{x_m} \to_{\vg}^* V$ (resp.\ $M\msub{V_1}{x_1}{V_m}{x_m} \allowbreak\to_{\beta_v}^* V$);
    \item  \label{def:valsolv.solv}
    $M$ \!is \emph{$\vg$-solvable} (resp.~\!\emph{$\beta_v$-solvable}\!) if there are \mbox{$n, \!m \!\in\! \Nat$, \!variables $x_1,\dots,x_m$ \!and $N_1\!,\dots,\!N_n$} $ \allowbreak\in \Lambda$ such that
    $(\lambda x_1\ldots x_m.M)N_1\cdots N_n \to_{\vg}^* I$ \mbox{(resp.\ $(\lambda x_1\ldots x_m.M)N_1\cdots N_n \to_{\beta_v}^* I$).}
  \end{itemize}
\end{defin}

%The definitions of $\beta_v$-potentially valuable and $\beta_v$-solvable can be found in \cite{PaoliniRonchi99,ronchi04book}.
%The definitions of $\vg$-potentially valuable and $\vg$-solvable are given in  \cite{carraro14lncs}.
%\Giulio{Questo paragrafo mi sembra un po' ridondante confrontato con il paragrafo precedente alla Definizione~\ref{def:valsolv}.}

\begin{thm}\label{thm:valsolv}
  Let $M$ be a term:
  \begin{enumerate}
    \item \label{thm:valsolv.potval}
    $M$ is $\vg$-potentially valuable if and only if $M$ is $\beta_v$-potentially valuable;
    \item  \label{thm:valsolv.solv}
    $M$ is $\vg$-solvable if and only if $M$ is $\beta_v$-solvable.
  \end{enumerate}
\end{thm}

\begin{proof} In both points, %~\ref{thm:valsolv.potval}-\ref{thm:valsolv.solv}
 the implication from right to left is trivial since $\to_{\beta_v} \, \subseteq \, \to_{\vg}$. Let us prove the other direction.
  \begin{enumerate}
    \item Since $M$ is $\vg$-potentially valuable, there are variables $x_1,\dots,x_m$ and $V, V_1, \dots, V_m \in \Lambda_v$ (with $m \geq 0$) such that $M\msub{V_1}{x_1}{V_m}{x_m} \allowbreak\to_{\vg}^* V$; then, there exists $V' \in \Lambda_v$ such that $M\msub{V_1}{x_1}{V_m}{x_m} \allowbreak\to_{\beta_v}^* V'$ by Corollary~\ref{cor:value}.\ref{cor:value.one} and because $\ToHeadBetav \, \subseteq \, \to_{\beta_v}$, therefore $M$ is $\beta_v$-potentially valuable.

    \item Since $M$ is $\vg$-solvable, there exist variables $x_1,\dots,x_m$ and terms
    $N_1,\dots,N_n$ (for some $n,m \geq 0$) such that $(\lambda x_1\ldots x_m.M)N_1\cdots N_n \to_{\vg}^* I$; then, there exists $V \in \Lambda_v$ such that $(\lambda x_1\ldots x_m.M)N_1\cdots N_n \allowbreak\to_{\beta_v}^* V %\InterTrans 
    \ToIntTrans I$ by Corollary~\ref{cor:value}.\ref{cor:value.one} and because $\ToHeadBetav \, \subseteq \, \to_{\beta_v}$. 
    According to Remark~\ref{rmk:preliminary}.\ref{rmk:preliminary.interinclusion}%~\ref{rmk:abs}.\ref{rmk:abs.anti}
%     , $V = I$ and therefore $M$ is $\beta_v$-solvable.
    , $V = \lambda x.N$ for some $N \in \Lambda$ such that $N \to_\vg^* x$.
    By Corollary~\ref{cor:value}.\ref{cor:value.one}, there is $V' \in \Lambda_v$ such that $N \ToHeadBetavReflTrans V' \ToIntTrans x$, hence $V' = x$ by Remark~\ref{rmk:preliminary}.\ref{rmk:preliminary.interinclusion} again. Since $\ToHeadBetav \, \subseteq \, \to_{\beta_v}$, $N \to_{\beta_v}^* x$ and thus $V = \lambda x.N \to_{\beta_v}^* I$, therefore $M$ is $\beta_v$-solvable.
  \end{enumerate}

\vspace{-\baselineskip}
\end{proof}

So, due to Theorem~\ref{thm:valsolv}, the semantic (via a relational model) and operational (via two sub-reductions of $\to_\vg$) characterization of $\vg$-potential valuability and $\vg$-solvability given in \cite[Theorems~24-25]{carraro14lncs} is also a semantic and operational characterization of $\beta_v$-potential valuability and $\beta_v$-solvability. 
The difference is that in $\lambda^{\sigma}_{v}$ these notions can be studied operationally inside the calculus, while it has been proved in \cite{PaoliniRonchi99,ronchi04book} that the $\beta_{v}$-reduction is too weak to characterize them:
an operational characterization of $\beta_v$-potential valuability and $\beta_v$-solvability cannot be given inside $\lambda_v$. 
Hence, $\lambda^{\sigma}_{v}$ is a useful, conservative and ``complete'' tool for studying semantic properties of  $\lambda_{v}$.
%
%By the way, the operational characterization of $\beta_v$-potential valuability and $\beta_v$-solvability is \emph{inside} our $\lambda_v^{\sigma}$-calculus, not inside Plotkin's $\lambda_v$-calculus.
%In other words, adding $\sigma$-reduction (which is used in literature on call-by-value $\lambda$-calculi) to Plotkin's $\lambda_v$-calculus allows us to get rid of the mismatch in a call-by-value setting between reduction and some theoretical notions as potential valuability and solvability.
%For instance, the terms $M$ and $N$ in Example~\ref{ex:reductions} are $\beta_v$-normal but they are not $\beta_v$-potential valuable (nor $\beta_v$-solvable), so they should diverge in a good call-by-value $\lambda$-calculus and indeed they are not $\vg$-normalizable.
%
%From Corollary~\ref{cor:value} it follows other results of interest: the adequacy of $\vg$-reduction (Theorem~\ref{thm:adequacy}) and its immediate consequence (Corollary~\ref{cor:observational}): $\lambda_v^\sigma$-calculus is sound with respect to the call-by-value observational equivalence introduced in \cite{Plotkin75} (Definition~\ref{def:halt}).
%The observational equivalence is an approach to proving equivalence of programs (see \cite{moggi89lics,sabry92lisp}).
%
%\begin{defin}[Halting, observational equivalence]\label{def:halt}
%  Let $M \in \Lambda$. We say that (\emph{the evaluation of}) \emph{$M$ halts} if there exists $V \in \Lambda_v$ such that $M \ToHeadBetavReflTrans V$.
%
%  The \emph{call-by-value observational equivalence} is an equivalence relation $\cong$ on $\Lambda$ defined by: $M \cong N$ if, for every context $\mathtt{C}$, one has that $\ctxC{M}$ halts iff $\ctxC{N}$ halts.%
%  \footnote{Original Plotkin's definition of call-by-value observational equivalence (see \cite{Plotkin75}) also requires that $\ctxC{M}$ and $\ctxC{N}$ are closed terms, according to the tradition identifying programs with closed terms.}
%\end{defin}
%
%In passing, we point out that $M \ToHeadBetavReflTrans V \in \Lambda_v$ iff $M \ToHeadReflTrans V$, thanks to Lemma~\ref{lemma:commutation}.\ref{lemma:commutation.general} and Remark~\ref{rmk:abs}.\ref{rmk:abs.notovalue}.
%So, if we define that $M$ $\beta_v\sigma$-halts when $M \ToHeadReflTrans V$ for some $V \in \Lambda_v$, it ensues that $M \cong N$ is equivalent to say that, for any context $\mathtt{C}$, $\ctxC{M}$ $\beta_v\sigma$-halts iff $\ctxC{N}$ $\beta_v\sigma$-halts.
%
%\begin{thm}[Adequacy of $\vg$-reduction]\label{thm:adequacy}
%    If $M \to_{\vg}^* M'$, then $M$ halts iff $M'$ halts.
%\end{thm}
%
%\begin{proof}
%  Suppose $M' \ToHeadBetavReflTrans V \in \Lambda_v$: then $M \to_{\vg}^* M' \to_{\vg}^* V$ since $\ToHeadBetav \, \subseteq \, \to_{\vg}$. By Corollary~\ref{cor:value}, there exists $V' \in \Lambda_v$ such that $M \ToHeadBetavReflTrans V'$. Thus $M$ halts.
%
%  Conversely, suppose $M \ToHeadBetavReflTrans V \in \Lambda_v$: $M \to_{\vg}^* V$ since $\ToHeadBetav \, \subseteq \, \to_{\vg}$. By confluence of $\to_\vg$ (Proposition~\ref{prop:general-properties}, since $M \!\to_{\vg}^*\! M'$) and Remark~\ref{rmk:fromvalue} (since $V \!\in\! \Lambda_v$), there is $V' \!\in\! \Lambda_v$ such that $V \!\to_{\vg}^*\! V'$ and $M' \!\to_{\vg}^*\! V'$. 
%  By Corollary~\ref{cor:value}, there is $V'' \!\in\! \Lambda_v$ such that \mbox{$M' \!\ToHeadBetavReflTrans\! V''$. So $M'$ halts.}
%\end{proof}
%
%% A remarkable consequence of Theorem~\ref{thm:adequacy} is that all $\vg$-equivalent terms are observationally equivalent (Corollary~\ref{cor:observational}). 
%
%% \begin{defin}[Observational equivalence]
%%   The \emph{observational equivalence} is an equivalence relation $\cong$ on $\Lambda$ defined by: $M \cong N$ if, for every context $\mathtt{C}$, one has that $\ctxC{M}$ halts iff $\ctxC{N}$ halts.
%% \end{defin}
%
%\begin{cor}[Soundness]\label{cor:observational}
%  If $M =_\vg N$ then $M \cong N$.
%\end{cor}
%
%\begin{proof}
%  Let $\mathtt{C}$ be a context. By confluence of $\to_\vg$ (Proposition~\ref{prop:general-properties}), there exists $L$ such that $M \to_\vg^* L$ and $N \to_\vg^* L$, hence $\ctxC{M} \to_\vg^* \ctxC{L}$ and $\ctxC{N} \to_\vg^* \ctxC{L}$. By Theorem~\ref{thm:adequacy}, $\ctxC{M}$ halts iff $\ctxC{L}$ halts iff $\ctxC{N}$ halts. Therefore, $M \cong N$.
%\end{proof}
%
%Plotkin \cite{Plotkin75} has already proved that $M =_{\beta_v} N$ implies $M \cong N$, but our Corollary~\ref{cor:observational} is not obvious since our $\lambda_v^\sigma$-calculus equates more than Plotkin's $\lambda_v$-calculus (see Example~\ref{ex:reductions}).
%The converse of Corollary~\ref{cor:observational} does not hold since $\lambda x. x (\lambda y. xy) \cong \Delta$ but $\lambda x. x (\lambda y. xy)$ and $\Delta$ are different $\vg$-normal forms and hence $\lambda x. x (\lambda y. xy) \neq_\vg \Delta$ (by confluence of $\to_\vg$, Proposition~\ref{prop:general-properties})%: in order to get the converse of Corollary~\ref{cor:observational} we need to add at least the $\eta_v$-reduction (see \cite{moggi88ecs,sabry92lisp}) to our $\lambda_v^\sigma$-calculus
%.

