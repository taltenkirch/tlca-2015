\section{Background}\label{sec:background}

Outside Section \ref{sec:duality}, we just consider intuitionistic implicational logic. Formulas(=types) are given by: $A,B,C\,::=\,X\,|\,A\supset B$. In typing systems, contexts $\Gamma$ are sets of declarations $x:A$ with at most one declaration per variable. In term languages, meta-substitution is denoted with square brackets, as in $\sub NxM$.

%By \emph{formal substitution} we mean a primitive construction of some system standing for some meta-substitution operation. Formal substitution is usually %denoted by angle brackets, as in $\xsub NxM$. Whether formal substitution is \emph{explicit} or merely \emph{delayed/suspended} depends on the reduction system %surrounding the primitive construction. In the first case we mean that a set of reduction rules exist for step-by-step execution of the substitution; in the %latter case, a single reduction rule exists that triggers the meta-operation.


%-------------------------------------------------------
\subsection{Control operation}

Parigot's $\lmu$-calculus \cite{Parigot92} is our model for the management of formulas and (co-)variables in the r.h.s. of sequents, when the possibility of a distingushed/active/selected formula exists - a model we wish to ``dualize'' to the l.h.s. of sequents.

Still we diverge from the original. Let $Q:=[b]((\mu a.M)N_1\cdots N_m)$. In the original formulation of $\lmu$, the reduction of $Q$ proceeds by $m$ applications of ``structural reduction'', by which $\mu$-abstraction consumes the arguments, one after the other, capturing contexts $\hole N_i$ and triggering a \emph{``structural substitution''}. After $m$ such reduction steps, the resulting term $[b]\mu a.M'$ is reduced by a \emph{renaming} rule, producing $[b/a]M'$. The same effect is obtained with a single, long-step, reduction rule for the $\mu$-operator. Consider the context $\C=[b](\hole N_1\cdots N_m)$, hence $Q=\C\fh{\mu a.M}$. Now apply the reduction rule $\C\fh{\mu a.M}\to\sub{\C}aM$, where $\sub{\C}aM$ is \emph{context substitution}, in whose definition the critical case reads:\footnote{Structural substitution is the particular case $\sub{a(\hole N)}a{\_}$ of context substitution.}
%$$
\begin{equation}\label{eq:context-subst}
\sub{\C}a{(aP)}=\C\fh{P'} \textrm{  where $P'=\sub{\C}aP$}\enspace.
\end{equation}
%$$
\noindent In this formulation, the single reduction rule still says that the $\mu$-operator captures $\C$, while the definition of context substitution says that $aP$ is a \emph{fill instruction}: fill the hole of the $\C$ that lands here with $P$.

This style with a single reduction rule is - see \cite{jesAPAL13} - the exact reflection in natural deduction of the reduction rule for the $\mu$-operator in the $\lmmt$-calculus. We are calling the latter reduction rule $\mu$ - see Fig.~\ref{fig:lmmt} - and so it is natural to call $\mu$ the corresponding natural deduction rule \footnote{In \cite{jesAPAL13} both rules are called $\sigma_{\mu}$.}. Beware that, sometimes, in $\lmu$, $\mu$ names solely the ``structural reduction'' - which we may see as the control operation proper. In the style with a single rule, $\mu$ comprehends the whole control operation, and we will seek a single rule $\mut$ to comprehend the whole co-control operation - and find something different from the rule $\mut$ of $\lmmt$, despite the compelling symmetry of the latter.

%--------------------------------
\begin{figure}\caption{The $\lmmt$-calculus}\label{fig:lmmt}
\begin{center}
\begin{tabular}{|l|l|}
\hline
$
\begin{array}{rrcl}
\textrm{(Terms)} & t,u & ::= & x\,|\,\lambda x.t\,|\,\mu a.c\\
\textrm{(Co-terms)} & e & ::= & a\,|\,u::e\,|\,\mut x.c\\
\textrm{(Commands)} & c & ::= & \com{t}{e}
\end{array}
$
&
$
\begin{array}{rrcll}
(\beta) & \com{\lambda x.t}{u::e}& \rightarrow & \com u{\mut x.{\com
te}}\\
(\mut) & \com t{\mut x.c} & \rightarrow & [t/x]c\\
(\mu) & \com {\mu a.c}e & \rightarrow & [e/a]c
\end{array}
$\\
\hline
\end{tabular}
\end{center}
\end{figure}
%--------------------------------------------

%-------------------------------------------------------
\subsection{Vector notation}

The vector notation for the $\lb$-calculus is the following definition of the $\lb$-terms \cite{JoachimskiMatthes2003}:
$$
M,N,P,Q\,::=\,\lb x.M\,|\,x\vec N\,|\,(\lb x.M)N\vec N
$$
\noindent According to this definition, $\lb$-terms have three forms, which we call \emph{first, second and third forms}. The advantage of this notation is that the head variable/redex is visible, and the $\beta$-normal forms are obtained by omitting the third form of terms in the above grammar.

This notation is informal, since many details are left unspecified. For instance: Are vectors a separate syntactic class, or do the second and third forms correspond to families of rules? How is substitution defined? Notice that, in the second form, it does not make sense to replace $x$ by another term.

Let us introduce the \emph{relaxed} vector notation:
$$
M,N,P,Q\,::=\,\lb x.M\,|\,x\vec N\,|\,M\vec N
$$
\noindent Some redundancy is now allowed, as the same ordinary $\lb$-term can be represented in many ways. By analyzing the third form, we find four cases. One corresponds to the third form of the original vector notation, the other three may be simplified as follows:
$$
\begin{array}{rrrcl}
(\epsilon)&\quad&M\nil&=&M\\
(\pi_1)&\quad&(x\vec Q) N\vec N&=&x(\vec Q N\vec N)\\
(\pi_2)&\quad&(P\vec Q)N\vec N&=&P(\vec Q N\vec N)%\\
%&\quad&(\lb x.M)N\vec N&&
\end{array}
$$
\noindent Here $\nil$ represents the empty vector. The simplifications were given names that will link them with the reduction rules of sequent calculus.

The third form of the original vector notation is a $\beta$-redex and hence it means something like ``call the head function with the first argument of the vector''. The simplifications that have arisen by relaxing the vector notation can be read as rules for vector bookkeeping: garbage collect an empty vector ($\epsilon$), or append chained vector ($\pi_i$). Much more difficult is to recognize in the second form $x\vec N$ an instruction for action on the vector $\vec N$: this is visible in a more general system with co-control, like the sequent calculus we are about to introduce, where the base case for vectors is not just $\nil$.

%We will say that the vector notation is as important as combinators for the Curry-Howard isomorphism.






