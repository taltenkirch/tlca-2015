\section{Natural deduction}\label{sec:natural-deduction}

The interpretation developed before is an internal and literal one: vectors are vectors; and co-control is understood in a formal way, as dual to control. We now develop a natural deduction system $\unat$ that is isomorphic to $\uol$. The isomorphism gives a new, external interpretation, which recovers the view that (some) vectors are ``evaluation contexts''\cite{Herbelin95}; %it also gives an assignment of natural deduction terms to sequent calculus derivations - but, since %the assignment is an isomorphism, we escape the anathema;
it justifies the design of $\uol$, namely its notion of $\HH$ and reduction rule $\mut$; finally, it allows the transfer of properties among the two systems.

%-----------------------------------
\subsection{The $\unat$-calculus}

The proof-expressions of the calculus are given by:

$$
\begin{array}{lcrcl}
\textrm{(Terms)} &  & M,N,P & ::= & \lambda x.M\,|\,\app H\,|\,\lt HxP\\
\textrm{(Heads)} &  & H & ::= & x\,|\, \hd M\,|\,HN
\end{array}
$$

\noindent Notice that variables $x$ and applications $HN$ are not terms. A head $\hd M$ is called a \emph{head term}.
%If a head has the form $xN_1\cdots N_m$ or $\hd M N_1\cdots N_m$ we say it has \emph{length} $m$.

The typing rules are in Fig.~\ref{fig:typing-nat-ded}. They handle two kinds of sequents: $\Gamma\vdash M:A$ and $\Gamma\rhd H:A$. Four rules are standard, with the appropriate kind of sequent determined by the kind of expression being typed. The remaining two rules switch the kind of sequent, and are called \emph{coercions}. However, despite the superficial impression, the two coercions are quite different, one being called weak and the other strong. Normalization will tell them apart radically.

%--------------------------------------
\begin{figure}[t]\caption{Typing rules of $\unat$}\label{fig:typing-nat-ded}
%$$
%\begin{array}{c}
%\infer[Hyp]{\Gamma,x:A\rhd x:A}{}\quad\quad\infer[Let]{\Gamma\vdash \lt HxP:B}{\Gamma\rhd H:A &
%\Gamma,x:A\vdash P:B}\\ \\
%\infer[Intro]{\Gamma\vdash\lambda x.M:A\supset B}{\Gamma,x:A\vdash
%M:B}\quad\quad \infer[Elim]{\Gamma\rhd
%HN:B}{\Gamma\rhd H:A\supset B & \Gamma\vdash N:A}\\
%\\
%\infer[WCoercion]{\Gamma\vdash\app H:A}{\Gamma\rhd H:A}
%\quad\quad \infer[SCoercion]{\Gamma\rhd \hd M:A}{\Gamma\vdash M:A}\\
%\\
%\end{array}
%$$
$$
\begin{array}{c}
\infer[Hyp]{\Gamma,x:A\rhd x:A}{}\quad\quad\infer[Let]{\Gamma\vdash \lt HxP:B}{\Gamma\rhd H:A &
\Gamma,x:A\vdash P:B}\quad\quad\infer[WCoercion]{\Gamma\vdash\app H:A}{\Gamma\rhd H:A}\\ \\
\infer[Intro]{\Gamma\vdash\lambda x.M:A\supset B}{\Gamma,x:A\vdash
M:B}\quad\quad \infer[Elim]{\Gamma\rhd
HN:B}{\Gamma\rhd H:A\supset B & \Gamma\vdash N:A}\quad\quad \infer[SCoercion]{\Gamma\rhd \hd M:A}{\Gamma\vdash M:A}
\end{array}
$$
\end{figure}
%-------------------------------------

A \emph{normal} derivation is one without occurrences of $Let$ and $SCoercion$.
%------------------------------
\begin{theorem}[Subformula property]\label{thm:subformula} Every formula (resp. every formula, including $A$) occurring in a normal derivation of $\Gamma\vdash M:A$ (resp. $\Gamma\rhd H:A$) is subformula of $A$ or of some formula in $\Gamma$ (resp. is a subformula of some formula in $\Gamma$).
\end{theorem}
%------------------------------
\noindent So, the weak coercion loses information regarding the subformula property, while the strong coercion potentially violates that property.
%---------------------------------------
\begin{figure}[t]\caption{Reduction rules of $\unat$}\label{fig:reduction-rules-nat-ded}
$$
\begin{array}{rcrcl}
%(\beta) && \K\fh{\hd{\lb x.M}N}& \to & \lt{\hd N}x{\K\fh{\hd M}}\\
(\BETA) && \hd{\lb x.M}N& \to & \hd{\lt{\hd N}xM}\\
(\LET) && \lt HxP & \to & \sub HxP\\
(\TRIV) && \app{\hd M }& \to & M\\
(\HD_1) && \hd{\app{H}} & \to & H\\
(\HD_2) && \K\fh{\hd{\lt HxP}} & \to & \lt Hx{\K\fh{\hd P}}
%(\assoc_1) && \K\fh{\hd{\K'\fh x}}& \to & (\K\circ\K')\fh x\\
%(\assoc_2) && \K\fh{\hd{\K'\fh{\hd M}}}& \to & (\K\circ\K')\fh{\hd M}\\
\end{array}
$$
\end{figure}
%---------------------------------------

The reduction rules of $\unat$ are in Fig.~\ref{fig:reduction-rules-nat-ded}. Rule $\LET$ triggers ordinary substitution $[H/x]P$, while rule $\HD_2$ employs certain contexts that we call \emph{continuations}:
$$
\K\,::=\,\app{\hole}\,|\,\lt{\hole}xP\,|\,\K\fh{\hole N}
$$
\noindent We single out two particular cases of $\LET$: $\REN$, when $H=x$; and $\SUB$, when $H=\hd M$. We put $\T:=\LET\backslash(\REN\cup\SUB)$. Let $\HD:=\HD_1\cup\HD_2$. Notice that rules $\BETA$ and $\HD_1$ are relations on heads.
%Rule $\HD_1$ is a relation on heads, while rule $\HD_2$ is relation on terms. As with all other reduction rules, $\HD_1$ generates by compatible closure a %relation $\to_{\HD_1}$ on heads and another on terms. The relation $\to_{\HD_1}$ on terms would have been the same, had we taken the rule $\HD_1$ as the relation %on terms $\K\fh{\hd{\app{H}}}\to \K\fh H$.
%(By taking both rules as axioms of the compatible closure, one generates two relations $\to_{\HD}$, one on heads, the other on terms.)
The normal forms w.r.t. all reduction rules are given by:
$$
\begin{array}{rclcrcl}
M & ::= & \lambda x.M\,|\,\app H & \qquad & H & ::= & x\,|\,HN
\end{array}
$$
\noindent That is, these normal forms are characterized by the absence of occurrences of lets and $\hd{}$. Lets are eliminated by $\LET$ whereas all the other rules concur to eliminate the coercion $\hd{}$. So, $\BETA,\TRIV,\LET,\HD$-reduction is \emph{normalization}, that is, the reduction to a form corresponding to normal derivations.\\

%------------------------------------------------------------------------------
\subsection{Isomorphism}\label{subsec:isomorphism}
%\textbf{Isomorphism.}
See Fig.~\ref{fig:Theta} for the map $\Theta:\uol\longrightarrow\unat$. There is actually a function $\Theta:\uol-Terms\longrightarrow\unat-Terms$, together with an auxiliary function $\Theta:\unat-Heads\times\uol-Vectors\longrightarrow\unat-Terms$. Let $\Theta(t)=M$,
$\Theta(u_i)=N_i$ and $\Theta(v)=P$. The idea is to map, say,
$t(u_1::u_2::\mut x.v)$ to $\lt{hd(M)N_1N_2}xP$, and $\der x{(u_1::u_2::\nil)}$ to $\app{xN_1N_2}$: left-introductions are replaced by applications, inverting the associativity of non-abstractions.


%------------------------------
\begin{figure}[t]\caption{Map $\Theta:\uol\longrightarrow\unat$}\label{fig:Theta}
$$
\begin{array}{rclcrcl}
\Theta(\lambda x.t)  & = & \lambda x.\Theta t &\quad& \Theta(H,\nil)&=&\app{H}\\
\Theta(\der xk)& = & \Theta(x,k) &\quad& \Theta(H,\mut x.t) & = & \lt Hx{\Theta t}\\
\Theta(tk) & = & \Theta(hd(\Theta t),k)&\quad& \Theta(H,u::k) & = & \Theta(H\Theta u,k)
\end{array}
$$
\end{figure}
%-----------------------------


%------------------------------
\begin{figure}[t]\caption{Map $\Psi:\unat\longrightarrow\uol$}\label{fig:Psi}
$$
\begin{array}{rclcrcl}
\Psi(\lambda x.M)  & = & \lambda x.\Psi M &\quad& \Psi(x,k)&=&\der xk\\
\Psi(\app H)& = & \Psi(H,\nil) &\quad& \Psi(\hd M,k) & = & (\Psi M)k\\
\Psi(\lt HxP) & = & \Psi(H,\mut x.\Psi P)&\quad& \Psi(HN,k) & = & \Psi(H,(\Psi N)::k)
\end{array}
$$

\end{figure}
%-----------------------------
%-------------------theorem-------------------------------------
\begin{theorem}[Isomorphism]\label{thm:isomorphism} Map $\Theta$
is a sound bijection between the set of $\uol$-terms and the set of $\unat$-terms (whose inverse $\Psi$ is shown in Fig.~\ref{fig:Psi}).
%Moreover, for each reduction rule
%$R$ of $\uol$, let $R'$ be the corresponding rule of $\unat$, as given in Fig.~\ref{fig:correspondence-red-rules}.
Moreover, let $R$ be rule $\beta$ (resp. $\mut$, $\epsilon$, $\pi$) of $\uol$, and let $R'$ be rule $\BETA$ (resp. $\LET$, $\TRIV$, $\HD$) of $\unat$. Then, $t\rightarrow_{R}t'$ in $\uol$ iff $\Theta
t\rightarrow_{R}\Theta t'$ in $\unat$.
\end{theorem}
%-------------------theorem-------------------------------------


The real action of the isomorphism happens in the translation of non-abstractions. Every non-abstraction $\unat$-term has the form $\Theta(H,k)$, and $\Psi\Theta(H,k)=\Psi(H,k)$. Every non-abstraction $\uol$-term has the form $\Psi(H,k)$, and $\Theta\Psi(H,k)=\Theta(H,k)$. So non-abstractions have the form $\Theta(H,k)$ (natural deduction) or $\Psi(H,k)$ (sequent calculus), and the isomorphism action between them is just to interchange $\Theta$ and $\Psi$ in these expressions.

$\Psi$ can be extended to continuations, establishing a bijection with vectors: $\Psi(\app{\hole})=\nil$, $\Psi(\lt{\hole}xP)=\mut x.\Psi P$ and $\Psi(\K\fh{\hole N})=\Psi(N)::\Psi(\K)$. As we knew, continuations are typed ``on the left'': the sequent calculus rules for typing vectors are derived typing rules in natural deduction for typing continuations.

Similarly, $\Theta$ can be extended to co-continuations, establishing a bijection with heads: $\Theta(\der x{\hole})=x$, $\Theta(t(\hole)=\hd{\Theta t}$ and $\Theta(\HH\fh{u::\hole})=\Theta(\HH)\Theta u$. This tells us how to type co-continuations \cite{jesAPAL13}: the natural deduction rules for typing heads are derived typing rules in sequent calculus for typing co-continuations, and so co-continuations are typed ``on the right''. %Let us call $\Psi$ the inverse of this bijection.

Let us call $\Theta$ the inverse of the bijection between continuations and vectors. Then it is easy to prove that $\Theta(H,k)=\Theta(k)\fh{H}$. This tells us that non-abstractions in $\uol$ are fill instructions, and that $\Theta$ executes these instructions. For instance, $\Theta(tk)=\Theta(\hd{\Theta t},k)=\Theta(k)\fh{\hd{\Theta t}}$; so $tk$ means ``fill $\hd M$ in the hole of continuation $\K$'', with $M=\Theta t$ and $\K=\Theta(k)$; and $\Theta(tk)$ is the result of such filling. Similarly, $\der xk$ means ``fill $x$ in the hole of $\K$''. So $\Theta$ realizes again the idea, going back to Prawitz \cite{Prawitz1965}, that sequent calculus derivations are instructions for building natural deduction proofs.




%------------------------------------------------------------------------------------
\subsection{Forgetfulness}\label{subsec:forgetfulness}
%\textbf{Forgetfulness.}
The \emph{forgetful map} $|\cdot|$ translates $\unat$-expressions to $\lb$-terms by erasing occurrences of the coercion $\hd{\cdot}$, forgetting the distinction between terms and heads, de-sugaring let-expressions (\emph{i.e.} translating them as $\beta$-redexes), and mapping $|\app{H}|=I|H|$, where $I=\lb x.x$. The following results about $\uol$ are proved through the analysis of this simple translation of the isomorphic calculus $\unat$.


%-----------------------------
\begin{theorem}[Strong normalization]\label{thm:SN} Every typable term of $\uol$ (resp. of $\unat$) is $\beta\epsilon\mut\pi$-SN
(resp. $\BETA\,\TRIV\,\LET\,\HD$-SN).
%$R$-SN, with $R=\beta\cup\epsilon\cup\mut\cup\pi$ (resp. $R=\beta\cup\TRIV\cup\LET\cup\HD$).
\end{theorem}
%-----------------------------


%-----------------------------
\begin{theorem}[Focalization]\label{thm:focalization} Every term of $\uol$ has a unique $\rho\tau$-normal form, which is a $\ol$-term (representing a $LJT$-proof).
\end{theorem}
%-----------------------------


%----------------------------
%\begin{proposition}
%$\to_{\LET}$ in $\unat$ terminating.
%\end{proposition}
%\textbf{Proof.} Map $\unat$-expressions to $\lb$-terms by erasing coercions, forgetting the distinction between terms and heads, and by translating %let-expressions as marked $\beta$-redexes. Then, every $\LET$-reduction step is mapped to a $\beta$-reduction step of a marked redex. Termination follows from %finiteness of developments in the $\lb$-calculus. $\square$\\
%----------------------------


%-----------------------------------
%\subsection{Natural deduction isomorphic to $\ol$}
