\section{Duality}\label{sec:duality}

Let us check that co-control, as formulated in $\uol$, is dual to control. We will expand $\uol$ to a self-dual system where control and co-control are each other's dual. This is achieved with three steps.

First step: to unify $\uol$ and $\unat$. Some hints are at the end of Section \ref{subsec:isomorphism}, the idea comes from \cite{jesTOCS09}. Every non-abstraction term of $\uol$ has the form $\Psi(H,k)$. So we unify $\der xk$ and $tk$ as $\opsi(H,k)$ and allow in $\uol$ a new syntactic class $H\,::=\,x\,|\,\hd t$. Every non-abstraction term of $\unat$ has the form $\Theta(H,k)$. Se we unify $\app H$ and $\lt HxP$ as $\utheta(H,k)$ and allow in $\unat$ a new syntactic class $k\,::=\,\nil\,|\,\mut x.P$. Next let us unify $\opsi(H,K)$ and $\utheta(H,k)$ as $\ucom Hk$. After this we realize that $\uol$ and $\unat$ are partial views of the same system (the former lacks $HN$, the latter lacks $u::k$). So let $t$ and $M$ range over the same set of proof terms. Continuations are internalized and coincide with vectors, co-continuations are internalized and coincide with heads, context-substitution is internalized as ordinary substitution. We work modulo $\ucom{HN}k=\ucom H{N::k}$, which abstracts the single difference between $\uol$ and $\unat$. %This much is inspired in \cite{jesTOCS09}.

Second step: to add control. We introduce the class of ``commands'' $c\,::=\,\ucom Hk$, and a non-abstraction term is now $\mu a.c$. Continuations are now given by $k\,::=\,a\,|\,\mut x.c\,|\,u::k$. Sequents have full r.h.s's: for instance, $\Gamma|k:A\vdash\Delta$. Logically, we moved to classical logic.

Third step: to complete the duality. We add the dual implication $A-B$, and the class of co-terms $r\,::=\,\lbt a.r\,|\,\mut x.c$. The place left vacant in the grammar of continuations by the move of $\mut x.c$ is occupied by the new construction $\hdt r$. The full suite of sequents is:
$$
\Gamma\vdash t:A|\Delta \qquad\Gamma|r:A\vdash\Delta \qquad \Gamma\rhd H:A|\Delta \qquad \Gamma|k:A\rhd\Delta \qquad c:(\Gamma\vdash\Delta)
$$
\noindent We now easily write the constructors for the inference rules relative to $A-B$, just by dualizing those of implication: the already seen $\lbt a.r$ (left introduction), the co-continuation $\const Hr$ (right introduction), and the continuation $\appt rk$ (elimination, on the left!). The full system is given in Fig.~\ref{fig:unified}. The typing rules are omitted due to space limitations, but writing them down is now just routine.

%--------------------------------
\begin{figure}\caption{The unified calculus}\label{fig:unified}
\begin{center}
\begin{tabular}{|c|}
\hline
$
\begin{array}{rrcl}
\textrm{(Terms)} & t,u,M,N & ::= & \lambda x.t\,|\,\mu a.c\\
\textrm{(Co-terms)} & r,s & ::= & \lbt a.r\,|\,\mut x.c\\
\textrm{(Co-continuations)} & H & ::= & x\,|\,\hd M\,|\,HN\,|\,\const Hr\\
\textrm{(Continuations)} & k & ::= & a\,|\,\hdt{r}\,|\,\appt rk\,|\,u::k\\
\textrm{(Commands)} & c & ::= & \ucom{H}{k}
\end{array}
$
\\
\hline\\[-.25cm]
$
\begin{array}{rrcl}
(\beta) & \ucom{\hd{\lambda x.t}}{u::k}& \rightarrow & \ucom{\hd u}{\hdt{\mut x.{\ucom{\hd t}k}}}\\
(\tld\beta) & \ucom{\const Hs}{\hdt{\lbt a.r}}& \rightarrow & \ucom{\hd{\mu a.\ucom H{\hdt{r}}}}{\hdt s}\\
%\end{array}
%$
%\quad
%$
%\begin{array}{rrcl}
(\mu) & \ucom {\hd{\mu a.c}}k & \rightarrow & [k/a]c\\
(\mut) & \ucom H{\hdt{\mut x.c}} & \rightarrow & [H/x]c
\end{array}
$
\\ \\[-.25cm]
\hline
$\begin{array}{rrcl}
(\cong)&\ucom{HN}k&=&\ucom H{N::k}\\
(\cong)&\ucom{\const Hr}k&=&\ucom H{\appt rk}
\end{array}$\\
\hline
\end{tabular}
\end{center}
\end{figure}
%--------------------------------------------

The classical, de Morgan/Gentzen duality is the duality between hypotheses and conclusions, l.h.s. and r.h.s. of sequents, conjunction and disjunction (if these were present), $A\supset B$ and $B-A$. Gentzen praised $LK$ for its exhibiting of this duality \cite{Gentzen1969}. Let us denote it by $\tilde{(\cdot)}$, \emph{justement}. At the level of types $\tilde{A\supset B}=\tilde B - \tilde A$ and vice-versa. Co-terms are dual of terms, and vice-versa. The same for co-continuations and continuations. The notation of constructions and the naming of reduction rules self-explains how $\tilde{(\cdot)}$ operates. Commands are self-dual: $\tilde{\ucom Hk}=\ucom{\tilde k}{\tilde H}$. The unified system is self-dual, at the level of typing and reduction. For instance, $\Gamma\vdash t:A|\Delta$ iff $\tilde{\Delta}|\tilde t:\tilde A\vdash\tilde\Gamma$, etc.

Given the process of construction of the unified system, it is clear the latter has a fragment that is a sequent calculus: forbid $HN$ and $\const Hr$ and get rid of class $H\,::=\,x\,|\,\hd t$ by expanding the two cases of commands: $\der xk:=\ucom xk$ and $tk:=\ucom{\hd t}k$. The result $SC$ is not a self-dual system: its dual is the natural-deduction fragment $ND$ of the unified system, obtained thus: forbid $u::k$ and $\appt rk$ and get rid of $k$'s by expanding the two cases of commands: $aH:=\ucom Ha$ and the new $\ucom H{\hdt r}$. In other words: de Morgan/Gentzen duality transforms sequent calculus $SC$ into natural deduction $ND$, and vice-versa; and this is just a partial view of the self-duality of the unified system. Notice that the duality between $SC$ and $ND$ links $HN$ with $\appt rk$ (and $\const Hr$ with $u::k$), whereas the isomorphism between the two systems (internalized as equations in the unified system) links $HN$ with $N::k$ (and $\const Hr$ with $\appt rk$).

So $SC$ is not self-dual, but $\lmmt$ seemingly is \cite{CurienHerbelin2000}. This is a symptom of something. Look again at Fig.~\ref{fig:lmmt}. Despite its compelling symmetry, $\lmmt$ (like $SC$) has a huge distortion towards control. Why? Commands in $\lmmt$, we may say, have the form $\com He$ with $H\,::=\,\hd t$. %(comparing with $SC$, the production $H\,::=\,x$ is missing because $x$ is a term in $\lmmt$).
Hence, the constructors for $e$'s have no dual in the class of $H$'s: the class of $e$'s is fully there, the class of $H$'s is residually there. We seem to see a duality between terms $t$ and ``co-terms'' $e$, but here the word ``co-terms'' is a misnomer. ``Co-terms'' $e$'s are continuations, rightly captured by the $\mu$-operator; then, either we see terms as the ``dual'' of continuations, and let them be captured by the $\mut$-operator, but then the latter, although ``dual'' to the $\mu$-operator, is not a co-control operator; or the $\mut$-operator, if it is to be a co-control operator, should capture, not terms, but co-continuations, a missing kind of expression, which is also typed ``on the right''; and the true co-terms are another missing kind of expressions typed ``on the left''. Notice that the distortion in $\lmmt$ has nothing to do with the fact that the dual of implication is not included in Fig.~\ref{fig:lmmt}; if it were, one would add one constructor to  the grammar of terms and its ``dual'' to the grammar of ``co-terms'', preserving the original ``duality'' \cite{CurienHerbelin2000}, but still failing to achieve true duality, for the same reasons.

What did we learn? There is nothing wrong with $SC$ or $\lmmt$, in their not being self-dual. What happens is that the classical sequent calculus, despite its symmetry, is unable to capture the duality between control and co-control, because the latter requires the full extent of the de Morgan/Gentzen duality, which also involves natural deduction, and is captured only in the unified system.


