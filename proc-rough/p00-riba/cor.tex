%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Correctness \wrt\@ Language Operations}
\label{sec:cor}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\noindent
This Section gathers several properties stating the correctness
of our constructions \wrt\@ operations on recognized languages.
We begin in Sect.~\ref{sec:cor:smc}
by properties on the symmetric monoidal structure,
the most important one being that the
synchronous arrow is \emp{correct},
in the sense that $\Sigma \thesis \At A \synchto \At B$
implies $\Lang(\At A) \sle \Lang(\At B)$.
Then, in Sect.~\ref{sec:cor:neg},
we discuss complementation of automata, and its relation with
the synchronous arrow.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Correctness of the Symmetric Monoidal Structure}
\label{sec:cor:smc}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


We begin by a formal correspondence between
acceptance games and synchronous games of a specific form.
This allows to show 
that the synchronous arrow is \emp{correct},
in the sense that $\Sigma \thesis \At A \synchto \At B$
implies $\Lang(\At A) \sle \Lang(\At B)$.
We then briefly discuss the correctness of the synchronous product
\wrt\@ language intersection.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}
\label{prop:cor:acc}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given $\Sigma \thesis \At A$ and $t \in \Tree[\Sigma]$,
there is a bijection:
\[
\{\sigma \tq \one \thesis \sigma \real \G(\At A,\const t) \}
\qquad\iso\qquad
\{ \theta \tq
\one \thesis \theta \real \G(\AU,\Id_\one) \synchto \G(\At A, \const t) \}
\]
\end{proposition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{remark}
\label{rem:cor:acc}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The above correspondence is only possible
for acceptance games over $\one$:
\begin{itemize}
\item In $\Sigma \thesis \sigma \real \G(\At A,M)$,
$\sigma$ is a positive $\Prop$-strategy, hence chooses the input characters in $\Sigma$.

\item In $\Sigma \thesis \theta \real \G(\Univ\Sigma,\Id_\Sigma) \synchto \G(\At A,M)$,
the strategy $\theta$ is a negative. It plays positively in
$\Sigma \thesis \G(\At A,M)$, but must follow the input characters
chosen by $\Opp$ in $\Sigma\thesis \G(\Univ\Sigma,\Id_\Sigma)$.
\end{itemize}
\end{remark}

\noindent
We now check that the arrow
$\G(\At A, M) \synchto \G(\At B, N)$
is correct \wrt\ language inclusion:
%%if
%\[
%\text{if}\quad
%\Sigma \real \G(\At A, M) \synchto \G(\At B, N)
%\quad\text{then}\quad
%%\]
%%then
%%\[
%\forall t \in \Tree[\Sigma],\quad
%M(t) \in \Lang(\At A) \quad\imp\quad
%N(t) \in \Lang(\At B)
%\]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}[Correctness of the Arrow]
\label{prop:cor:lgge}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Assume given
$\Sigma \thesis \sigma \real \G(\At A,  M) \synchto \G(\At B,  N)$.

\begin{enumerate}[(i)]
\item
\label{prop:cor:lgge:subst}
For all $t \in \Tree[\Sigma]$,
we have
$\subst{\const t}(\sigma) \real \G(\At A, M \comp \const t) 
  \synchto \G(\At B, N \comp \const t)$.

\item
\label{prop:cor:lgge:acc}
If
$\one \real \G(\At A, M \comp \const t)$ then
$\one \real \G(\At B, N \comp \const t)$.

\item
\label{prop:cor:lgge:lgge}
For all tree $t \in \Tree[\Sigma]$,
if $ M(t) \in \Lang(\At A)$
then $ N(t) \in \Lang(\At B)$.
\end{enumerate}
\end{proposition}

\noindent
The converse property will be discussed in 
Sect.~\ref{sec:proj}.
%Sect.~\ref{sec:nd}.
%
We finally check that the synchronous product is correct.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}
\label{prop:cor:synchprod}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
$\Lang(\clos{\At A} \synchprod \clos{\At B}) = \Lang(\At A) \cap \Lang(\At B)$.
\end{proposition}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Complementation and Falsity}
\label{sec:cor:neg}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subparagraph{Complementation.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given an automaton
\(
\At A = (Q,\init q,\trans,\Omega)
\),
following~\cite{walukiewicz02tcs}, we 
let its complement be
\(
\aneg \At A
\deq
(Q,\init q,\trans_{\aneg \At A},\Omega_{\aneg \At A})
\),
where
$\Omega_{\aneg \At A} \deq Q^\omega \setminus \Omega$
and
\[
\trans_{\aneg \At A}(q,a)
%\quad\deq\quad \trans(q,a)^\Perp
\quad\deq\quad
\{ \conj_{\aneg} \in \Po(Q \times \Dir) \tq
\forall \conj \in \trans(q,a),~ \conj_{\aneg} \cap \conj \neq \emptyset\}
\]

\noindent
The idea is that $\Prop$ on $\aneg\At A$
simulates $\Opp$ on $\At A$, so that the correctness
of $\aneg \At A$ relies on determinacy of acceptance games.
In particular, thanks to Borel determinacy~\cite{martin75am}, we have:

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}[\cite{walukiewicz02tcs}]
\label{prop:cor:neg:cor}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given $\At A$ with $\Omega_{\At A}$ a Borel set, we have
$\Lang(\aneg \At A) = \Tree[\Sigma] \setminus \Lang(\At A)$.
\end{proposition}


\noindent
Note that if $\At A$ is complete, then $\aneg \At A$
is not necessarily complete,
but $\delta_{\aneg \At A}$ is always not empty
and so are the $\conj$'s in its image.
%(given $q \in Q$ and $a \in \Sigma$,
%$\delta(q,a)$ is not empty, and moreover if $\conj \in \delta(q,a)$,
%then $\conj$ is not empty as well).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subparagraph{The Falsity Automaton $\AF$.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
We let
%We define $\Sigma \thesis \AF$ as
\(
\AF \deq (Q_\AF, q_\AF,\trans_\AF,\Omega_\AF)
\)
where $Q_\AF \deq \one$, $q_\AF \deq \bullet$,
$\Omega_\AF = \emptyset$
and
\(
\trans_\AF(q_\AF,a) \deq
  \{\{(q_\AF,d)\} \tq d \in \Dir \}
\).
Note that $\AU = \aneg \AF$.
In particular, it is actually $\Prop$ who guides the evaluation
of $\AF$, by choosing
the tree directions.
%This is in accordance with the semantics of complementation,
%which relies on determinacy.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}
\label{prop:cor:dialogue:lgges}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Let $\At A$ and $\At B$ be complete. %automata on $\Sigma$.
Then
$\Sigma \real \At A \synchprod \At B \synchto \clos\AF$
iff
$\Sigma \real \At A \synchto \clos{\aneg \At B}$.
\end{proposition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{corollary}
%\label{cor:cor:dialogue:lgges}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Let $\At A$ be a complete automaton on $\Sigma$.
Then $\one \real \clos{\aneg \At A}$
iff $\one \real \At A \synchto \clos\AF$.
\end{corollary}

