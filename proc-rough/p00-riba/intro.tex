%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\label{sec:intro}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This paper proposes a notion of morphism between tree automata based
on game semantics.
We follow the Curry-Howard-like slogan:
\emp{Automata as objects, Executions as morphisms}.

We consider general alternating automata on infinite ranked trees.
These automata encompass Monadic Second-Order Logic (MSO)
and thus most of the logics used in verification~\cite{gtw02alig}.
Tree automata are traditionally viewed as positive objects:
one is primarily 
interested in satisfaction or satisfiability,
and the primitive notion of quantification is 
existential.
%
In contrast, Curry-Howard approaches tend to favor proof-theoretic oriented
and negative approaches, \ie\@ approaches in which the predominant
logical connective is the implication, and where the predominant
form of quantification is universal.

We consider full infinite ranked trees, built from a non-empty finite
set of directions $\Dir$ and labeled in non-empty finite
alphabets $\Sigma$.
The base category $\Tree$ has alphabets as objects
and morphisms from $\Sigma$ to $\Gamma$ are $(\Sigma \to \Gamma)$-labeled
$\Dir$-ary trees.

The fibre categories are based on a generalization of the usual acceptance games,
where for an automaton $\At A$ on alphabet $\Gamma$
(denoted $\Gamma \thesis \At A$),
input characters can be precomposed with a tree morphism
$M \in \Tree[\Sigma,\Gamma]$,
leading to substituted acceptance games of type
$\Sigma \thesis \G(\At A,M)$.
Usual acceptance games,
which correspond to the evaluation of %an automaton
$\Sigma \thesis \At A$ on a $\Sigma$-labeled input tree,
are substituted acceptance games $\one \thesis \G(\At A,t)$
with $t \in \Tree[\one,\Sigma]$.
Games of the form $\Sigma \thesis \G(\At A,M)$
are the objects of the fibre category over $\Sigma$.

For morphisms, we introduce a notion of ``synchronous'' simple
game between acceptance games.
We rely on Hyland \& Schalk's functor (denoted $\HS$)
from simple games to $\Rel$~\cite{hs99ctcs}.
A synchronous strategy $\Sigma \thesis \sigma : \G(\At A,M) \synchto \G(\At B,N)$
is a strategy in the simple game $\G(\At A,M) \lin \G(\At B,N)$
required to satisfy (in $\Set$) a diagram of the form of~(\ref{eq:synch})
below,
expressing that $\At A$ and $\At B$ are evaluated 
along the same path of the tree and
read the same input characters:
\begin{equation}
\label{eq:synch}
\xymatrix@R10pt{
  \HS(\sigma)
  \ar[r]
  \ar[d]
& \G(\At B,N)
  \ar[d]
\\
  \G(\At A,M)
  \ar[r]
& (\Dir + \Sigma)^*
}
\end{equation}

This gives a split fibration $\game$ of tree automata and 
acceptance games.
When restricting the base to \emp{alphabet} morphisms
(\ie\ functions $\Sigma \to \Gamma$), substitution can be internalized
in automata.
By change-of-base of fibrations, this leads to a split fibration $\aut$.
In the fibers of $\aut$, the substituted acceptance games have finite-state winning
strategies, whose existence can be checked by trivial adaptation of usual algorithms.

Each of these fibrations is monoidal in the sense of~\cite{shulman08tac},
by using a natural synchronous product of tree automata.
%When restricted to \emp{non-deterministic} automata,
%the monoidal synchronous product becomes Cartesian.
We also investigate a linear negation, as well as
existential quantifications, 
obtained by adapting the usual projection operation on non-deterministic
automata to make it a left-adjoint to weakening, the adjunction
satisfying the usual Beck-Chevalley condition.

Our linear implication of acceptance games seems to provide a natural
notion of prenex universal quantification on automata not investigated before.
As expected, if there is a synchronous winning strategy
$\sigma\real \At A \synchto \At B$,
then $\Lang(\At A) \sle \Lang(\At B)$
(\ie\ each input tree accepted by $\At A$ is also accepted by $\At B$).
Under some assumptions on $\At A$ and $\At B$
the converse holds:
$\Lang(\At A) \sle \Lang(\At B)$
implies $\sigma \real \At A \synchto \At B$
for some $\sigma$.


At the categorical level, 
thanks to~(\ref{eq:synch}),
the constructions mimic relations in slices categories
$\Set/(\Dir + \Sigma)^*$
of the co-domain fibration:
substitution is given by a (well chosen) pullback,
and the monoidal product of automata is issued from the Cartesian product
of plays in $\Set/(\Dir + \Sigma)^*$
(\ie\ also by a well chosen pullback).


The paper is organized as follows.
Section~\ref{sec:base} presents notations for trees and tree automata.
Our notions of substituted acceptance games and synchronous arrow
games are then discussed in Sect.~\ref{sec:sag}.
Substitution functors and the corresponding fibrations
are presented in Sect.~\ref{sec:fib}, and Section~\ref{sec:mon}
overviews the monoidal structure.
We then state our main correctness results in Sect.~\ref{sec:cor}.
Section~\ref{sec:proj} presents existential quantifications and %Sect.~\ref{sec:nd}
quickly discusses non-deterministic automata.





