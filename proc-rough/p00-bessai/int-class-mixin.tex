%!TEX root = submission.tex

\newcommand{\ClassCmb}{{\cal C}}
\newcommand{\MixinCmb}{{\cal M}}

\subsection{Class and Mixin combinators}\label{subsec:Mixin}

The following definition of classes and mixins is inspired by \cite{CookHC90} and \cite{BrachaC90} respectively, 
though with some departures to be discussed below. To make the description more concrete, in the examples we add constants to $\lambdaR$.

Recall that a {\em combinator} is a term in $\lambdaR^0$, namely a closed term. Let $\Y$ be  
Curry's fixed point combinator: $\lambda f.(\lambda x.f(xx))(\lambda x.f(xx))$ (the actual definition of $\Y$ is immaterial, since all fixed point combinators have the same types in {\bf BCD}).

\begin{definition}
Let $\myClass, \State$ and $\argClass$ be (term) variables and $\Y$ be a fixed point combinator; then we define the following sets of combinators:
\[
\begin{array}{llll}
%\text{Definition:} & D & ::= & \lambda\,\self. \, \record{m_i = e_i \mid i \in I}\\ [1mm]
%\text{Generator:} & G & ::= & \lambda \,\myClass \ \lambda \, \State. \, D(\myClass \ \State)\\ [1mm]
\text{Class:}  & C & ::= & \Y (\lambda \,\myClass \ \lambda \, \State. \,  \record{l_i = N_i \mid i \in I}) \\ [1mm]
%\text{Wrapper:} & W & ::= & \lambda \, \super\, \lambda \, \self. \, \super \oplus \record{m_i = e_i \mid i \in I} \\ [1mm]
\text{Mixin:} & M & ::= & \lambda \, \argClass. \, \Y (\lambda \,\myClass \, \lambda \, \State. \, (\argClass \; \State) \Override \record{l_i = N_i \mid i \in I})\\ [1mm]
\end{array}
\]
We define $\ClassCmb$ and $\MixinCmb$ as the sets of classes and mixins respectively.
\end{definition}

To illustrate this definition let us use the abbreviation $\letin{x = N} M \equiv M\Subst{N}{x}$. 
Then a  class combinator $C\in \ClassCmb$ can be written
in a more perspicuous way as follows:
\begin{equation}\label{eq:class-def}
C \equiv \Y(\lambda \,\myClass \ \lambda \, \State. \ \letin{\self = ( \myClass \; \State)}  \record{l_i = N_i \mid i \in I}).
\end{equation}


%Let us say that a {\em state} $S \equiv \record{l_j:V_j \mid j\in J}$ is a record  of {\em values} $V_j$ (that can be represented by combinators, or simply by constants). Then 

A  {\em class} is the fixed point of a function,  the {\em class definition}, mapping a recursive definition of the class itself and a state $S$, that is the value or a record of values in general, for the instance variables of the class, into a record $\record{l_i = N_i \mid i \in I}$ of {\em methods}.  A class $C$ is instantiated to an {\em object} $O \equiv C\,S$ by applying the class $C$ to
a state $S$. Hence we have:
\[
O \equiv C\,S \reduces^* \letin{\self = (C\,S)}  \record{l_i = N_i \mid i \in I},
\]
where the variable $\self$ is used in the method bodies $N_i$ to call other methods from the same object. Note that
the recursive parameter $\myClass$ might occur in the $N_i$ in subterms other than $( \myClass \; \State)$, and in particular
$N_i\Subst{C}{\myClass}$ might contain a subterm $C\,S'$, where $S'$ is a state possibly different than $S$; even $C$ itself might be returned as the value
of a method. 
Classes are the same as in \cite{CookHC90} \S 4, but for the explicit identification of $\self$ with $(\myClass \; \State)$.

We come now to typing of classes.
Let $R =  \record{l_i = N_i \mid i \in I}$, and suppose that $C \equiv  \Y (\lambda \,\myClass \ \lambda \, \State. \,  R) \in \ClassCmb$. To type $C$ we must find
a type $\sigma$ (a type of its state) and a sequence of types $\rho_1,\ldots , \rho_n \in \TTR$ such that for all $i< n$:
\[ 
\myClass: \sigma \to \rho_i, \State: \sigma \der R : \rho_{i+1}.
\]
Note that this is always possible for any $n$: in the worst case, we can take $\rho_i = \record{l_i:\omega \mid i\in I}$ for all $0 < i \leq n$. In general one has more expressive types, depending on the typings of the $N_i$ in $R$ (see example \ref{ex:class-type} below). It follows that:
\[
\der \lambda \,\myClass \ \lambda \, \State. \,  R: (\omega\to\rho_1) \inter \bigcap_{1 \leq i < n} (\sigma \to \rho_i) \to (\sigma \to \rho_{i+1}),
\] 
and therefore, by using the fact that $\der \Y : (\omega\to \tau_1) \inter \cdots \inter (\tau_{n-1} \to \tau_n) \to \tau_n$ for
arbitrary types $\tau_1,\ldots,\tau_n$, we conclude that the typing of classes has the following shape (where $\rho =\rho_n$):
\begin{equation}\label{eq:class-typing}
\der  C \equiv \Y (\lambda \,\myClass \ \lambda \, \State. \,  \record{l_i = N_i \mid i \in I}): \sigma \to \rho
\end{equation}
In conclusion the type of a class $C$ is the arrow from the type of the state $\sigma$ to a type $\rho$ of its instances.

\begin{example}\label{ex:class-type}
%\input{example_class}

The class $\Point$ has an integer state and contains the method $\get$ to retrieve the state, $\set$ to update the current state and $\shift$ to add a value to the current state. 
\begin{align*}
\Point = \Y(\lambda & \myClass. \lambda \state. \letin{\self = \myClass \ \state}\\
& \record{\get = \state, \set = \lambda \state'. \state', \shift = \lambda d. \self.set(\self.get + d)})
\end{align*}

Note that in a setting without references, we rely on purely functional state updates. Therefore, every function returns a new state that can be used to construct the new object. An example for this is set, which just returns the new state.

To type $\Point$ we have $\der \Point : \Int \to \rho_{\Point}$ where $\rho_{\Point} = \rho_2$ and
\begin{align*}
\rho_1 &= \record{\get : \Int, \set : \Int \to \Int, \shift : \omega}\\
\rho_2 &= \record{\get : \Int, \set : \Int \to \Int, \shift : \Int \to \Int}\\
\text{using }\Y &: (\omega \to \Int \to \rho_1) \cap ((\Int \to \rho_1) \to \Int \to \rho_2) \to \Int \to \rho_2
\end{align*}

%Let us recall te abbreviation $M.l:=N \equiv M\Override\record{l=M}$, and let us write $M.l(N) \equiv (M.l)\,N$.
%The class $\Point$ of one dimensional points with integer coordinate $x$, has states of the form $\record{x = n}$ and contains the method $\get$ to retrieve the coordinate, $\set$ to update the current state and $\eqm(O)$ to compare the state of $\self$ with that one of $O$ (of same subclass of $\Point$).
%\begin{align*}
%\Point = \Y(\lambda & \myClass. \lambda \state. \\
%& \letin{\self = \myClass \ \state}\\
%& \record{  \get = \state.x, \set = \lambda m. \,  \myClass(\state.x := m),   \eqm = \lambda o. o.\get = \self.\get }
%\end{align*}
%Observe that our classes are purely functional, therefore the update of the instance variable $x$ in $\set$ is modeled by returning a new instance of the class $\Point$, applied to the new state. Let us abbreviate 
%$R_{\Point} \equiv \record{  \get = \state.x, \set = \lambda m. \,  \myClass(\state.x := m),   \eqm = \lambda o. o.\get = \self.\get }$, and
%take $\sigma_{\Point} = \record{x:\Int}$. Then we have: 
%\[\myClass: \ \omega, \state: \sigma_{\Point} \der \myClass:\omega ~~~ \mbox{and}~~~
%\myClass: \ \omega, \state: \sigma_{\Point} \der \self:\omega,
%\] 
%and consequently the best typing we can deduce for $ R_{\Point}$ in the basis $\Gamma_0 = \Set{\myClass: \ \omega, \state: \sigma_{\Point}}$ is just:
%\[
%\Gamma_0 \der R_{\Point}: \record{\get:\Int, \set:\omega,\eqm:\omega}
%\]
%However, taking $\rho_1 = \record{\get:\Int, \set:\omega,\eqm:\omega}$ and 
%$\Gamma_1 = \Set{\myClass: \sigma_{\Point} \to \rho_1, \state: \sigma_{\Point} }$, we have $\Gamma_1\der\self : \rho_1$ and
%\[
%\Gamma_1, m:\Int \der \myClass(\state.x := m):\rho_1 ~~~ \mbox{and}~~~
%\Gamma_1, o:\rho_1: o.\get = \self.\get: \Bool
%\]
%from which we obtain the informative typing:
%\[
%\Gamma_1 \der R_{\Point}: \record{\get:\Int, \set:\Int\to\rho_1,\eqm:\rho_1\to \Bool} \equiv \rho_2.
%\]
%Putting all together we have 
%\begin{align*}
%\der (\lambda \myClass. \lambda \state.\,R_{\Point}: & (\omega\to (\sigma_{\Point}\to\omega))\inter \\
%&	( (\sigma_{\Point}\to\omega)\to  (\sigma_{\Point}\to\rho_1)) \inter ((\sigma_{\Point}\to\rho_1) \to (\sigma_{\Point}\to\rho_2)),
%\end{align*}
%and hence $\der \Point: (\sigma_{\Point}\to\rho_{\Point})$, where $\rho_{\Point} = \rho_2$.
\end{example}

\medskip A {\em mixin} $M \in \MixinCmb$ is a combinator such that, if $C\in \ClassCmb$ then $M\,C$ reduces to a new class $C' \in \ClassCmb$, inheriting from $C$.
Writing $M$ in a more explicit way we obtain:
%\begin{multline}\label{eq:mixin-def}
%M \equiv  \lambda \, \argClass. \, \Y ( \\
%	\hspace{1.5cm} \lambda \,\myClass \, \lambda \, \State. \, \\
%	\hspace{2cm} 	\letin{ \super = (\argClass\;\State)} \\
%	\hspace{2.5cm}	\letin{\self = ( \myClass \; \State)} 
%		\super \Override \record{l_i = N_i \mid i \in I}) \hfill
%\end{multline}
\begin{align*}\label{eq:mixin-def}
M \equiv  \lambda \, \argClass. \, \Y ( \lambda \,\myClass \, \lambda \, \State. \,&\letin{ \super = (\argClass\;\State)} \\
	&\letin{\self = ( \myClass \; \State)}\\ 
	&\super \Override \record{l_i = N_i \mid i \in I})
\end{align*}
In words, a mixin merges an instance $C\,S$ of the input class $C$ with a new state $S$ together with a {\em difference} record 
$R \equiv \record{l_i = N_i \mid i \in I}$, that would be written $\Delta(C\,S)$ in terms of \cite{BrachaC90}. Note that our mixins are not the same as class modificators (also called wrappers e.g. in \cite{BrachaThesis}) because the latter do not take the instantiation of a class as the value of $\super$, but the
class definition, namely the function defining the class {\em before} taking its fixed point. 
%The effect is that we have a static (or early) binding 
%instead of dynamic (or late) binding of $\self$. \marginpar{explain why we made this choice}

Let $M \equiv  \lambda \, \argClass. \, \Y (\lambda \,\myClass \, \lambda \, \State. \, (\argClass \; \State) \Override R) \in \MixinCmb$; 
to type $M$ we have to find types $\sigma^1, \sigma^2, \rho^1$ and a sequence $\rho^2_1, \ldots, \rho^2_n \in \TTR$ of record types such that for all $1 \leq i < n$ it is true that
$\lbl(R) = \lbl(\rho^2_{i})$ and such that, setting
$\Gamma_0 = \Set{\argClass: \sigma^1 \to \rho^1, \myClass: \omega, \State: \sigma^1\inter\sigma^2}$ and $\Gamma_{i} = \Set{\argClass: \sigma^1 \to \rho^1, \myClass: (\sigma^1\inter\sigma^2) \to \rho^1 +  \rho_{i}^2, \State: \sigma^1\inter\sigma^2}$ for all $1 \leq i < n$, we may deduce for all $0 \leq i < n$:
\[
\prooftree 
	\prooftree
		\prooftree
			\Gamma_i \der \State: \sigma^1\inter\sigma^2
		\justifies
			\Gamma_i \der \State: \sigma^1
		\using (\leq)
		\endprooftree
	\justifies
		\Gamma_i \der \argClass \; \State:\rho^1
		\using (\ArrE)
	\endprooftree
	\qquad
	\Gamma_i  \der R: \rho_{i+1}^2 \qquad \lbl(R) = \lbl(\rho^2_{i+1})
\justifies
	\Gamma_i \der (\argClass \; \State) \Override R: \rho^1+\rho_{i+1}^2
\using (+)
\endprooftree
\]
Hence for all $0\leq i<n$ we can derive the typing judgment:
\begin{eqnarray*}
\lefteqn{\argClass: \sigma^1 \to \rho^1 \der \lambda \,\myClass \, \lambda \, \State. \, (\argClass \; \State) \Override R:} \\ [2mm]
& & \hspace{4.5cm}((\sigma^1\inter\sigma^2) \to (\rho^1 +  \rho_i^2)) \to (\sigma^1\inter\sigma^2) \to ( \rho^1+\rho_{i+1}^2)
\end{eqnarray*}
and therefore, by reasoning as for classes, we get (setting $\rho^2 = \rho_{n}^2$):
\begin{multline}\label{eq:mixin-typing}
\der  M \equiv \lambda \, \argClass. \, \Y (\lambda \,\myClass \, \lambda \, \State. \, (\argClass \; \State) \Override R): \\
(\sigma^1 \to \rho^1) \to (\sigma^1\inter\sigma^2) \to ( \rho^1+\rho^2)
\end{multline}
Spelling out this type, we can say that $\sigma^1$ is a type of the state of the argument-class of $M$; $\sigma^1\inter\sigma^2$ is the type
of the state of the resulting class, that refines $\sigma^1$. $\rho^1$ expresses the requirements of $M$ about the methods of the argument-class, i.e. what is assumed to hold for the usages of $\super$ and $\argClass$ in $R$ to be properly typed; $\rho^1+\rho^2$ is a type of the record of methods of the refined class, resulting from the merge of the methods of the argument-class with those of the difference $R$; since in general there will be overridden methods, whose types might be incompatible, the $+$ type constructor cannot be replaced by intersection. % This is the interpretation, in our setting, of the fact that ``inheritance is not subtyping'' \cite{CookHC90}. 

\begin{example}\label{ex:mixin-type}
%\input{example_mixin}

The mixin $\Movable$, provided an argument class that contains a $\set$ and a $\shift$ method, creates a new class with (potentially overwritten) methods $\set$ and $\move$ along with delegated methods. The method $\set$ is fixed to set the underlying state to $1$ and the method $\move$ shifts the state by $1$.
%\begin{align*}
%\Movable = \lambda \argClass. \Y (\lambda & \myClass. \lambda \state. \\
%& \letin{\super = \argClass \ \state} \\
%& \letin{\self = \myClass \ \state} \\
%& \super \oplus \record{\set = \super.set(1), \move = \self.\shift(1)})
%\end{align*}
\begin{align*}
\Movable = \lambda \argClass. \Y (\lambda \myClass. \lambda \state.
& \letin{\super = \argClass \ \state} \\
& \letin{\self = \myClass \ \state} \\
& \super \oplus \record{\set = \super.set(1), \move = \self.\shift(1)})
\end{align*}

%We have $\der \Movable \colon (\Int \to \rho_{\Point}) \to (\Int \to \rho_{\Movable} )$ where $\rho_{\Movable} = \rho_{\Point}+\rho^2_2$ and
%\begin{align*}
%\rho^2_1 &= \record{\set : \Int, \move : \omega}\\
%\rho^2_2 &= \record{\set : \Int, \move : \Int}\\
%\text{using }\Y &: (\omega \to \Int \to\rho^2_1) \cap ((\Int \to \rho^2_1) \to \Int \to \rho^2_2) \to \Int \to \rho^2_2.
%\end{align*}

Note that to update $\self$ and $\super$ one can use $\myClass$ and $\argClass$.
Generalizing for all $\rho \in \TTR$ following the argumentation above we can choose:
\begin{align*}
\rho^1 &= \rho \cap \record{\set : \Int \to \Int, \shift : \Int \to \Int } \qquad
&\rho^2 &= \record{\set : \Int, \move: \Int }\\
\sigma^1 &= \Int\qquad &\sigma^2 &= \omega\\
\rho^2_1 & = \record{\set : \Int, \move: \omega }\qquad
&\rho^2_2 &= \record{\set : \Int, \move: \Int }
\end{align*}
Using these choices we can type $\Y$ by
$$ \vdash \Y : (\omega \to (\Int \to \rho^1 + \rho^2_1)) \cap ((\Int \to \rho^1 + \rho^2_1) \to (\Int \to \rho^1 + \rho^2_2)) \to (\Int \to \rho^1 + \rho^2_2)$$
and obtain the typings of Movable for all $\rho$:
\[
\prooftree
  \prooftree
    \dots
  \justifies
    \vdash \Movable : (\Int \to \rho \cap \record{\set : \Int \to \Int, \textit{shift} : \Int \to \Int }) \to (\Int \to \rho^1 + \rho^2_2)
  \endprooftree
\justifies
  \vdash \Movable : (\Int \to \rho \cap \record{\set : \Int \to \Int , \textit{shift} : \Int \to \Int}) \to (\Int \to \rho + \record{\set : \Int, \move : \Int })
  \using (\leq)
\endprooftree   
\]


%Continuing example \ref{ex:class-type}, let us consider the mixin:
%\begin{align*}
%\Movable = \lambda \argClass. \Y (\lambda & \myClass. \lambda \state. \\
%& \letin{\super = \argClass \ \state} \\
%& \letin{\self = \myClass \ \state} \\
%& \super \oplus \record{\set = \super.\set(0), \move = \lambda d.\,\super.\set(\self.x + d)}).
%\end{align*}
%By deriving a type for $\Movable$, we check that it is applicable to the class $\Point$, and get a type of the subclass $(\Movable\,\Point)$ inheriting from $\Point$.
%
%In the typing (\ref{eq:mixin-typing}) we take $\sigma^1 = \sigma^2 =  \sigma_{\Point}$; then we set
%$\Gamma_0 = \Set{ \argClass: \sigma_{\Point}\to\rho_{\Point},  \myClass: \omega, \state: \sigma_{\Point}}$, and we derive:
%\[
%\Gamma_0 \der \super = (\argClass \ \state) :  \rho_{\Point}, ~~~\mbox{so that}~~~\Gamma_0 \der \super.\set(0):\rho_1 
%~~~(\mbox{see example \ref{ex:class-type}}),
%\]
%but we have only $\Gamma_0 \der \self = (\myClass \ \state) :\omega$, so that $\Gamma_0\der :  \lambda d.\,\super.\set(\self.x + d):\omega$.
%Let $R_{\Movable} \equiv \record{\set = \super.\set(0), \move = \lambda d.\,\super.\set(\self.x + d)}$. Then we conclude that
%$\Gamma_0\der R_{\Movable}: \record{\set: \rho_1,\move:\omega}$, from which we derive by $(*)$:
%\[
%\Gamma_0 \der \super \Override R_{\Movable}:  \rho_{\Point} + \rho_1 \equiv \rho^1 + \rho^2_1.
%\]
%Iterating the method, we take $\Gamma_1 = \Gamma_0\setminus{\myClass: \omega}, \myClass:  \sigma_{\Point} \to \rho^1 + \rho^2_1$, getting:
%\[
%\Gamma_1 \der \self = (\myClass \ \state) :  \rho^1 + \rho^2_1~~\mbox{and hence}~~
%\Gamma_1, d:\Int \der \super.\set(\self.x + d): \rho_1,
%\]
%therefore obtaining $\Gamma_1 \der \super \Override R_{\Movable}:  \rho_{\Point} + \record{\set: \rho_1, \move: \Int\to\rho_1}$. Following the same pattern
%of the derivation of the typing (\ref{eq:mixin-typing}), and setting $\rho_{\Movable} \equiv \record{\set: \rho_1, \move: \Int\to\rho_1}$, we conclude that
%$\der \Movable: (\sigma_{\Point}\to\rho_{\Point}) \to (\sigma_{\Point}\to\rho_{\Point}+\rho_{\Movable})$.
\end{example}