Our main goal is to combine type information given by intersection types for $\lambdaR$ and the capabilities of the logical programming language given by BCL inhabitation to synthesize meaningful mixin compositions as terms of a combinatory logic. Such \emph{combinatory terms} are formed by application of combinators from a \emph{repository} (combinatory logic context) $\Delta$.
\begin{definition}[Combinatory Term]
$E, E' ::= C\; |\; (E\; E'),\; C \in \dom(\Delta)$
\end{definition}
We create repositories of typed combinators that can be considered logic programs for the existing BCL synthesis framework (CL)S~\cite{BDDMR14} to reason about semantics of such compositions. The underlying type system of (CL)S is an extension of the intersection type system {\bf BCD} \cite{bcd} by covariant constructors. The extended type system $\TTC$, while suited for synthesis, is flexible enough to encode record types and features of $+$. While (CL)S implements covariant constructors of arbitrary arity, we only use unary constructors, which are sufficient for our encoding.

\begin{definition}[Intersection Types with Constructors $\TTC$]
The set $\TTC$ is given by:
\[
\TTC \ni \sigma, \tau, \tau_1, \tau_2 \ ::= a \mid \alpha \mid \omega \mid \tau_1 \rightarrow \tau_2 \mid \tau_1 \cap \tau_2 \mid c(\tau)
\]
where $a$ ranges over constants, $\alpha$ over type variables and $c$ over unary constructors $\CC$.
\end{definition}

$\TTC$ adds the following two subtyping axioms to the {\bf BCD} system
$$\tau_1 \leq \tau_2 \Rightarrow
c(\tau_1) \leq c(\tau_2) \qquad \quad
c(\tau_1) \cap c(\tau_2) \leq 
c(\tau_1 \cap \tau_2)$$

The additional axioms ensure \emph{constructor distributivity}, i.e., $c(\tau_1) \cap c(\tau_2) = c(\tau_1 \cap \tau_2)$. 

\begin{definition}[Type Assignment in $\TTC$]
\[\small
\begin{array}{ccc }
\prooftree
	C:\tau \in \Delta \qquad S \text{ Substitution}
	\justifies
	\Delta \vdashbcl C:S(\tau)
	\using (\text{Var})
\endprooftree 
&
\prooftree
	\Delta \vdashbcl E: \sigma\to\tau \qquad \Delta \vdashbcl E':\sigma
	\justifies
	\Delta \vdashbcl E E':\tau
	\using (\ArrE)
\endprooftree 

\\ [6mm]

\prooftree
	\Delta \vdashbcl E: \sigma \quad 
	\Delta \vdashbcl E: \tau
	\justifies
	\Delta \vdashbcl E: \sigma\inter\tau
	\using (\inter)
\endprooftree 
&
\prooftree
	\Delta \vdashbcl E: \sigma \qquad \sigma\leq\tau
	\justifies
	\Delta \vdashbcl E : \tau
	\using (\leq)
\endprooftree 
\end{array}\]
\end{definition}

We extend the necessary property of \emph{beta-soundness} and a notion of \emph{paths} and \emph{organized types} from \cite{RehofEtAlCSL12} to constructors in the following way.

\begin{lemma}[Extended Beta-Soundness]\label{lem:beta}\ 
If $\bigcap\limits_{i\in I}(\sigma_i\to\tau_i) \cap \bigcap\limits_{j\in J}c_j(\tau_j) \cap \bigcap\limits_{k\in K}\alpha_k \cap \bigcap\limits_{k'\in K'}a_{k'} \leq c(\tau)$,\\
then 
$\{j \in J \mid c_j = c\} \neq \emptyset$ and 
$\bigcap \{\tau_j \mid j \in J, c_j = c\} \leq \tau$.
\end{lemma}

\begin{definition}[Path]
A \emph{path} $\pi$ is a type of the form:
$ \pi ::= a \mid \alpha \mid \tau \to \pi \mid c(\omega) \mid c(\pi) $,
where $\alpha$ is a variable, $\tau$ is a type, $c$ is a constructor and $a$ is a constant.
\end{definition}

\begin{definition}[Organized Type]
A type $\tau$ is called organized, if it is an intersection of paths $\tau \equiv \bigcap_{i \in I} \tau_i$, where $\tau_i$ for $i \in I$ are paths.
\end{definition}

Similarly, we obtain the following property of subtyping w.r.t. organized types.

\begin{lemma}\label{lem:pathsubtyping}
Given two organized types $\tau \equiv \bigcap_{i \in I} \tau_i$ and $\sigma \equiv \bigcap_{j \in J} \sigma_j$,
we have $\tau \leq \sigma$ iff for all $j \in J$ there exists an $i \in I$ with $\tau_i \leq \sigma_j$.
\end{lemma}

Note that for all intersection types there exists an equivalent organized intersection type coinciding with the notion of strict intersection types~\cite{Bakel11}.

For a set of typed combinators $\Delta$ and a type $\tau \in \TTC$ we say $\tau$ is inhabitable in $\Delta$, if there exists a combinatory term $E$ such that $\Delta \vdashbcl E : \tau$. 
In the following we fix a finite set of labels $\mathcal{L} \subseteq \Label$ that are used in the particular domain of interest for mixin composition synthesis.

\paragraph*{Records as Unary Covariant Distributing Constructors}
We define constructors $\rrecord{\cdot}$ and $l(\cdot)$ for $l \in \mathcal{L}$ to represent record types using the following partial translation function 

$\interpret{\cdot} \colon \TT \to \TTC$,
$\interpret{\tau} = \begin{cases}
\tau & \text{if } \tau \equiv \omega \text{ or } \tau \equiv a\\
\interpret{\tau_1} \to \interpret{\tau_2} & \text{if } \tau \equiv \tau_1 \to \tau_2\\
\interpret{\tau_1} \cap \interpret{\tau_2} & \text{if } \tau \equiv \tau_1 \cap \tau_2\\
\rrecord{l(\tau)} & \text{if } \tau \equiv \record{l : \tau}\\
\rrecord{\omega} & \text{if } \tau \equiv \record{}\\
\text{undefined} & \text{else}
\end{cases}$

Since atomic records are covariant and distribute over $\cap$, the presented translation preserves subtyping. We have $\interpret{\record{l_i : \tau_i \mid i \in I}} = \interpret{\bigcap_{i \in I} \record{l_i : \tau_i}} = \bigcap_{i \in I} \rrecord{l_i(\interpret{\tau_i})}$ if $I \neq \emptyset$.

Note that the translation function $\interpret{\cdot}$ is not defined for types containing $+$ in $\TT$. Additionally, $+$ has non-monotonic properties and therefore cannot be immediately represented by a covariant type constructor. Simply applying Lemma \ref{lem:normal-recordTypes}(\ref{lem:normal-recordTypes-c}) is impossible, if the left-hand side of $+$ is all-quantified. There are two possibilities to deal with this situation. 
The first option is extending the type-system used for inhabitation. Here, the main difficulty is that existing versions of the inhabitation algorithm crucially rely on the separation of intersections into paths \cite{RehofEtAlCSL12}. As demonstrated in the remark accompanying Lemma \ref{lem:typePlus}, it becomes unclear how to perform such a separation in the presence of the non-monotonic $+$ operation.
The second option, pursued in the rest of this section, is to use the expressiveness of the logical programming language given by $\bclc$ inhabitation. Specifically, encoding $\TT$ types containing $+$ as $\TTC$ types accompanied by following repositories $\Delta_{\mathcal{L}}$ and $\Delta_{\wp(\mathcal{L})}$ suited for $\bclc$ inhabitation. We introduce $|\mathcal{L}|$ distinct variables $\alpha_{l'}$ indexed by $l' \in \mathcal{L}$ and $2^{|\mathcal{L}|}$ distinct constructors $w_L(\cdot)$ indexed by $L \subseteq \mathcal{L}$.
\begin{align*}
\Delta_{\mathcal{L}} = & \{ W_{\{l\}} : w_{\{l\}}(\bigcap\limits_{l' \in \mathcal{L} \setminus \{l\}} \rrecord{l'(\alpha_{l'})}) \mid l \in \mathcal{L} \}\\
\Delta_{\wp(\mathcal{L})} = & \{ W_L : w_{\{l_{1}\}}(\alpha) \to w_{\{l_{2}\}}(\alpha) \to \ldots \to w_{\{l_k\}}(\alpha) \to w_L(\alpha) \mid k \geq 2, \{l_1, \ldots, l_k\} = L \subseteq \mathcal{L}\}
\end{align*}

These repositories are purely logical in a sense that they do not represent terms in $\lambdaR$ but encode necessary side conditions in the logic program. In particular, we formalize the conditions for the absence of a label in a record type by the following Lemma \ref{lem:label_predicate}. This encoding of negative information is crucial to encode non-monotonic properties of $+$.

%We define repositories $\Delta_{\mathcal{L}}$ and $\Delta_{\wp(\mathcal{L})}$ of typed combinators introducing for each non-empty set of labels $L \subseteq \mathcal{L}$ a constructor $w_L(\cdot)$.


%We extend the function $\lbl$ to $\TTR$ using the normal form from Lemma \ref{lem:normal-recordTypes} (\ref{lem:normal-recordTypes-c}) by $\lbl(\bigcap\limits_{l \in L} \record{l : \tau_l}) = L$. 


\begin{lemma}
\label{lem:label_predicate}
Let $l \in \mathcal{L}$ be a label and let $\tau \in \TT$ be a type such that $\interpret{\tau}$ is defined.
$w_{\{l\}}(\interpret{\tau})$ is inhabitable in $\Delta_{\mathcal{L}} \cup \Delta_{\wp(\mathcal{L})}$ iff $\tau \in \TTR \cup \{\omega \}$ with $l \not\in \lbl(\tau)$.
\end{lemma}

\begin{proof} (Sketch)
Let $l \in \mathcal{L}$ be a label.
\begin{itemize}
\item[$(\Rightarrow)$] Let wlog. $\interpret{\tau} \equiv \bigcap\limits_{i \in I} \tau_i$ be an organized intersection type such that $w_{\{l\}}(\interpret{\tau})$ is inhabitable in $\Delta_{\mathcal{L}} \cup \Delta_{\wp(\mathcal{L})}$. The only combinator with a matching target to inhabit $w_{\{l\}}(\interpret{\tau})$ is $W_{\{l\}}$. Due to distributivity of type constructors there exists a substitution $S$ such that $S(w_{\{l\}}(\bigcap\limits_{l' \in \mathcal{L} \setminus \{l\}} \rrecord{l'(\alpha_{l'})})) \leq w_{\{l\}}(\interpret{\tau})$. By Lemma \ref{lem:pathsubtyping} followed by Lemma \ref{lem:beta} we obtain for each $i \in I$ that there exists an $l' \in \mathcal{L} \setminus \{l\}$ such that $\tau_i = \rrecord{l'(\interpret{\sigma_i})}$ for some type $\sigma_i \in \TT$. By definition of $\interpret{\cdot}$ and distributivity we obtain $\tau \in \TTR \cup \{\omega\}$ with $l \not\in \lbl(\tau)$.
\item[$(\Leftarrow)$] Let $\tau = \bigcap\limits_{l' \in L} \record{l' : \tau_{l'}}$ (resp. $\record{}$) for some $L \subseteq \mathcal{L} \setminus \{l\}$ and types $\tau_{l'} \in \TT$ for $l' \in L$. We have \\
$W_{\{l\}} : S(w_{\{l\}}(\bigcap\limits_{l' \in \mathcal{L} \setminus \{l\}} \rrecord{l'(\alpha_{l'})})) \leq w_{\{l\}}(\interpret{\tau})$ for a substitution $S(\alpha_{l'}) = 
\begin{cases}
\interpret{\tau_{l'}} & \text{if } l' \in L\\
\omega & \text{else}
\end{cases}$
\end{itemize}
\end{proof}

\begin{lemma}
\label{lem:record_predicate}
Let $L \subseteq \mathcal{L}$ be a non-empty set of labels and let $\tau \in \TT$ be a type such that $\interpret{\tau}$ is defined.
$w_L(\interpret{\tau})$ is inhabitable in $\Delta_{\mathcal{L}} \cup \Delta_{\wp(\mathcal{L})}$ iff $\tau \in \TTR \cup \{\omega\}$ with $L \cap \lbl(\tau) = \emptyset$.
\end{lemma}

\begin{proof} (Sketch)
Using $W_L$ and Lemma \ref{lem:label_predicate} for each argument of $W_L$. 
\end{proof}

Our intermediate goal is to use inhabitation in $\Delta_\mathcal{L} \cup \Delta_{\wp(\mathcal{L})}$ to translate types of the shape $\rho + \bigcap\limits_{l \in L} \record{l : \tau_l}$ into $\rho \cap \bigcap\limits_{l \in L} \record{l : \tau_l}$, which in general is incorrect. The following Lemma \ref{lem:enc_soundness} describes sufficient circumstances where this translation holds.

\begin{lemma}[$\Delta_{\mathcal{L}} \cup \Delta_{\wp(\mathcal{L})}$ Translation Soundness]
\label{lem:enc_soundness}
Let $L \subseteq \mathcal{L}$ be a non-empty set of labels, let $\rho \in \TTR$ be a type such that $\interpret{\rho}$ is defined and let $\tau_l \in \TT$ for $l \in L$ be types.
If $w_L(\interpret{\rho})$ is inhabitable in $\Delta_{\mathcal{L}} \cup \Delta_{\wp(\mathcal{L})}$ then $\rho + \bigcap\limits_{l \in L} \record{l : \tau_l} = \rho \cap \bigcap\limits_{l \in L} \record{l : \tau_l}$.
\end{lemma}

\begin{proof} (Sketch)
Since $w_L(\interpret{\rho})$ is inhabitable in $\Delta_{\mathcal{L}} \cup \Delta_{\wp(\mathcal{L})}$ we have $\lbl(\rho) \cap L = \emptyset$. The result follows from Lemma \ref{lem:label} (\ref{lem:label-b}).
\end{proof}

Next, we show by the following Lemma \ref{lem:enc_completeness} that inhabitation in $\Delta_{\mathcal{L}}$ is not too restrictive.

\begin{lemma}[$\Delta_{\mathcal{L}} \cup \Delta_{\wp(\mathcal{L})}$ Translation Completeness]
\label{lem:enc_completeness}
Let $L \subseteq \mathcal{L}$ be a non-empty set of labels, let $\tau_l \in \TT$ for $l \in L$ be types, let $\rho \in \TTR$ be a type such that $\interpret{\rho}$ is defined. There exists a type $\rho' \in \TTR$ such that $w_L(\interpret{\rho'})$ is inhabitable in $\Delta_{\mathcal{L}} \cup \Delta_{\wp(\mathcal{L})}$ and $\rho' \cap \bigcap\limits_{l \in L} \record{l : \tau_l} \leq \rho + \bigcap\limits_{l \in L} \record{l : \tau_l}$.
\end{lemma}

\begin{proof} (Sketch)
Given $\rho = \bigcap\limits_{l' \in L'} \record{l' : \tau_{l'}'}$ choose $\rho' = \bigcap\limits_{l' \in L' \setminus L} \record{l' : \tau_{l'}'}$ (resp. $\rho' = \record{}$ if $L' \setminus L = \emptyset$). By Lemma \ref{lem:record_predicate} $w_L(\interpret{\rho'})$ is inhabitable in $\Delta_{\mathcal{L}} \cup \Delta_{\wp(\mathcal{L})}$ and $\rho' \cap \bigcap\limits_{l \in L} \record{l : \tau_l} \leq \rho + \bigcap\limits_{l \in L} \record{l : \tau_l}$.
\end{proof}

Note that in Lemma \ref{lem:enc_completeness} the type $\rho'$ can be chosen greater than $\rho$ only due to the non-monotonic properties of $+$.
