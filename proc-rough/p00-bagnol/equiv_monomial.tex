The first attempt in the direction of a notion of proofnet for \mallm is due to J.-Y.Girard~\cite{Girard1996},
followed by a version with a full \cut-elimination procedure by O.~Laurent and R.~Maielli~\cite{Laurent2008a}.

While proofnets for multiplicative linear logic without units were introduced along linear 
logic itself~\cite{Girard1987}, extending the notion to the multiplicative-additive without units fragment
proved to be a true challenge, mainly
because of the \emph{superposition} at work in the $\twith$ rule.

Girard's idea was to represent the superposed \enquote{versions} of the proofnet by attributing a Boolean formula
(called a \emph{weight}) to each link, with one Boolean variable for each $\twith$ connective in the conclusion $\Gamma$.
To retrieve the version of the proofnet corresponding to some selection of the left/right branches
of each $\twith$, one then just needs to evaluate the Boolean formulas with the corresponding valuation of their
variables.

This is the occasion to introduce the vocabulary to speak about Boolean formulas.

\begin{definition}[Boolean formula]
	Given a finite set of variables $V=\extset{{x_1}}{x_n}$, a \emph{Boolean formula} over $V$ is inductively defined
	from the elements of $V$; the constants $\zero$ (\emph{\enquote{false}}) and $\unit$(\emph{\enquote{true}});
	the unary symbol $\neg{\cdot}$ (\emph{\enquote{negation}});
	the binary symbols $+$ and $\p$ (\emph{\enquote{sum/or/disjuction}} and 
	\emph{\enquote{product/and/conjunction}} respectively).
%	
%	\TODO{vérifier: égalité modulo commutativité?}
%	Boolean formulas are considered \emph{equal} up to commutativity of $+$ and $\p$,
%	%; absorption and
%	%neutrality of $\zero$ (\ie $\zero\p\phi=\zero$ and $\zero+\phi=\phi$) 
%	but \emph{not} up to other usual boolean equations (distributivity of $\p$ over $+$, for instance).
\end{definition}

%\TODOEXEMPLE

We consider a syntactic
notion of \emph{equality} of Boolean formulas: for instance $\zero\p x\neq\zero$.
The real important notion, that we therefore state separately, is \emph{equivalence}: the fact that if we
replace the variables with actual values, we gets the same output.

\begin{definition}[equivalence]\label{def_booleq}
	A \emph{valuation} $v$ of $V$ is a choice of $\zero$ or $\unit$ for any element of $V$. A valuation induces a
	an \emph{evaluation} function $v(\cdot)$ from Boolean formulas over $V$ to $\{\zero,\unit\}$ in the obvious way.
	%
	Two Boolean formulas $\phi$ and $\psi$ over $V$ are \emph{equivalent} (notation $\phi\booleq\psi$) when
	for any valuation $v$ of $V$, we have $v(\phi)=v(\psi)$.
\end{definition}


\begin{definition}[monomial]
	We write $\neg V=\extset{\neg {x_1}}{\neg x_n}$.
	A \emph{monomial} over $V$ is a Boolean formula
	of the form $y_1\p\,\dots\,\p y_k$ with $\extset{y_1}{y_k}\subseteq V \cup \neg V$.
	%, such that the $y_i$ are
	%parwise distinct and there is no $j$ such that $x_j,\neg x_j\in\extset{y_1}{y_k}$. 
%	By convention
%	the empty monomial is defined to be the Boolean formula $\unit$.
	
	Two monomials $\monof m$ and $\monof m'$ are in \emph{conflict} if $\monof m\p\monof m'\booleq \zero$.
%	If they are not in conflict, they are called \emph{compatible}.
%	A \emph{disjunctive normal form (DNF)} Boolean formula over $V$ is a sum of monomials over~$V$.
\end{definition}

\begin{remark}\label{rem_compat}
	Two monomials are in conflict if and only if there is a variable $x$ such that $x$ appears in one of them and $\neg x$
	appears in the other.
\end{remark}

While monomials are a specific type of Boolean formula, the \emph{binary decision diagrams}
we are about to introduce are not defined directly as Boolean formulas. Of course, they relate to each other in an 
obvious way, but having a specific syntax for binary decision diagrams will prove more convenient to solve the problems we will be facing.

\begin{definition}[\bindd]
	A \emph{binary decision diagram} (\bindd) is defined inductively as:
	\begin{itemize}
		\item The constants $\zero$ and $\one$ are \bindd
%		\item If $\phi$ is a \obdd V, then $\dummy\phi$ is a \bdd.
%		\item If $\phi$, $\psi$ are \bindd then $\itef \zero\phi\psi$ and $\itef \one\phi\psi$ are \bdd.
		\item If $\phi$, $\psi$ are \bindd and $X$ is either a variable, $\zero$ or $\one$, $\itef X\phi\psi$ is a \bindd
		\item If $\phi$ is a \bindd and $X$ is either a variable, $\zero$ or $\one$, $\dcare X\phi$ is a \bindd
	\end{itemize}
	Moreover, suppose we have an ordered set of variables $V=\extset{{x_1}}{x_n}$ with the convention
	that variables are listed in the reverse order: $x_n$ is first, then $x_{n-1}$, \etc
	We define a subclass of
	\bindd which we call \emph{ordered binary decision diagrams over $V$} (\obdd V) by
	restricting to the following inductive cases (we let $V'=\extset{{x_1}}{x_{n-1}}$)
	\begin{itemize}
		\item The constants $\zero$ and $\one$ are \obdd \void
		\item If $\phi$ and $\psi$ are \obdd {V'}, $\itef {x_n}\phi\psi$ is a \obdd V
		\item If $\phi$ is a \obdd{V'}, $\dcare {x_n}\phi$ is a \obdd V
	\end{itemize}
	The notions of valuation and equivalence are extended to \bindd and \obdd{} the obvious way so that
	$\dcare X\phi\,\booleq\,\phi$ and $\itef X\phi\psi\,\booleq\, X\p\phi+\neg X\p\psi$.
\end{definition}

\begin{remark}
	Any time we will look at \bdd and \obdd{} from a complexity perspective, we will consider they are represented as
	labeled trees. The cases of \texttt{If}\:$\zero$, \texttt{DontCare}\:$\one,$\dots{} will be useful to obtain
	\aco reductions in \cref{sec_bool} and \cref{sec_logspace}, since erasing a whole subpart of a graph of which we do
	not know the address in advance
	is not something that is doable in this complexity class. The absence of sharing implied by the
	tree representation is also crucial to get low-complexity reductions.
\end{remark}

\begin{example}
	The Boolean formula $x\p y\p\neg z$ is a monomial, while $x\p y +z$ and $x\p\one$ are not.
	
	The \bdd $\itef {x_2}{(\itef {x_1}\zero\one)}{\one}$ (which is \emph{not} a \obdd{\{{x_1},{x_2}\}} by the way)
	is equivalent to the Boolean formula
	${x_2}\p\neg{x_1} +\neg {x_2}$: both evaluate to $\one$ only for the valuations $\{{x_1}\mapsto \one,{x_2}\mapsto \zero\}$,
	$\{{x_1}\mapsto \zero,{x_2}\mapsto \zero\}$ and $\{{x_1}\mapsto \zero,{x_2}\mapsto \one\}$. There exist an equivalent
	\obdd{\{{x_1},{x_2}\}}: $\itef {x_2}{(\itef {x_1}\zero\one)}{(\dcare {x_1}\one)}$.
\end{example}

%These two notions come with associated equivalence problems.
\newcommand{\bddequ}{\bdd\problem{equ}\xspace}
\newcommand{\obddequ}{\protectobdd\problem{equ}\xspace}
%
\begin{definition}\label{def_bddequ}
	\bddequ is the following decision problem: 
	\begin{center}
	{\it\enquote{given two \bdd $\phi$ and $\psi$, do we have
	$\phi\booleq\psi$?}}
	\end{center}
	%
	\obddequ is the following decision problem: 
	\begin{center}
	{\it\enquote{given two \obdd V $\phi$ and $\psi$, do we have
	$\phi\booleq\psi$?}}
	\end{center}
\end{definition}

Girard's proofnets are called monomial because the only Boolean formulas that are allowed are monomials.
This is, as of the state of the art, the only known way to have a notion of proofnet that enjoys a satisfying 
correctness criterion.

For our purposes, we do not need to get into the technical details of monomial proofnets. Still, let
us end this section with an example of proofnet from the article by Laurent and Maielli,
where the monomial weight of a link is pictured just above it:
%~\cite{Laurent2008a},
%so that we have 
%at least 
%an idea of how they look anyway:

\begin{center}
	\includegraphics[width=9cm]{laurent_mallpn}
\end{center}

